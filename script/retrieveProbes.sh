#!/bin/sh

basedir=$(dirname "$0")
basedir="$basedir"
cat <<- 'END_OF_FILE'
{
   "probes": [
        {
           "name": "cpu",
           "path": "org.ow2.clif.probe.cpu.Insert",
           "help": "polling_period_in_milliseconds execution_time_is_seconds"
        },
        {
           "name": "disk",
           "path": "org.ow2.clif.probe.disk.Insert",
           "help": "polling_period_in_milliseconds execution_time_is_seconds partition_path"
        },
        {
           "name": "jmx_jvm",
           "path": "org.ow2.clif.probe.jmx_jvm.Insert",
           "help": "polling_period_in_milliseconds execution_time_is_seconds configuration_file"
        },
        {
           "name": "memory",
           "path": "org.ow2.clif.probe.memory.Insert",
           "help": "polling_period_in_milliseconds execution_time_is_seconds"
        },
        {
           "name": "network",
           "path": "org.ow2.clif.probe.network.Insert",
           "help": "polling_period_in_milliseconds execution_time_is_seconds network_interface_name_or_IP_address_or_description"
        },
        {
           "name": "rtp",
           "path": "org.ow2.clif.probe.rtp.Insert",
           "help": "polling_period_in_milliseconds execution_time_is_seconds port_number_or_range"
        }
    ]
}
END_OF_FILE
