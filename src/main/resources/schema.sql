CREATE TABLE IF NOT EXISTS users (
    username varchar(50) not null primary key,
    password varchar(120) not null,
    email varchar(50),
    enabled boolean not null
);

CREATE TABLE IF NOT EXISTS authorities (
    username varchar(50) not null,
    authority varchar(50) not null,
    foreign key (username) references users (username)
);

CREATE TABLE IF NOT EXISTS userProject (
    username varchar(50) not null,
    projectName varchar(50) not null,
    permission ENUM('admin', 'editor', 'tester', 'viewer') not null,
    foreign key (username) references users (username),
    CONSTRAINT OnePermissionPerUser PRIMARY KEY (username, projectName)
);