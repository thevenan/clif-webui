/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 *  Adds the clicked file path to the localcache
 */
if (document.addEventListener) {
	document.addEventListener('contextmenu', function(e) {
		//obtain the path to the clicked file
		var tempElement = e.target;
		var path = "";
		while (tempElement.parentNode != null && tempElement.id != "ulFileTreeContainer") {
			tempElement = tempElement.parentNode;
			if (tempElement.tagName == "LI") {
				var childs = tempElement.childNodes;
				for (var i = 0; i < childs.length; i++) {
					if (childs[i].tagName == "DIV") {
						if (childs[i].innerText[0] == " ") {
							path = childs[i].innerText.substring(1) + "/" + path;
						}
						else {
							path = childs[i].innerText + "/" + path;
						}
					}
				}
			}
		}
		localCache.set("clickedFilePath", path);
		folderIncludeClassname(filesList, directories, localCache.get("clickedFilePath"));
	});
}

/**
 * This function check if the element the user has clicked on is a foldler. 
 * If so, it checks if one of its sub-files contains is names xxx.classname.
 * This checking is only done at level n-1 and n-2
 * @param {Array} fileList - A list containing all the urls of the files inside the project folder
 * @param {Array} directoryList - A list containing all the urls of the folders inside the project folder
 * @param {String} clickedElement - The file/project on which the user has clicked
 */
function folderIncludeClassname(fileList, directoryList, clickedElement) {
	var sendDataMenu = document.getElementById("thisSendData");
	disableMenuWithoutSecurity(sendDataMenu);
	var filePathToSend = [];
	//remove the "/" at the end of clickedElement
	//we check if the element is a folder
	if (directoryList.includes(clickedElement.substring(0, clickedElement.length - 1))) {
		//This sublist will include only the elements that contain clickedElement Url
		var fileSubList = [];
		for (var i = 0; i < fileList.length; i++) {
			if (fileList[i].includes(clickedElement)) {
				fileSubList.push(fileList[i]);
			}
		}
		//split each element of the sublist to check if they have a .classname element inside clickedElement folder
		var splitDirectoryLength = clickedElement.substring(0, clickedElement.length - 1).split("/").length;
		var firstLevel = false;
		for (var i = 0; i < fileSubList.length; i++) {
			fileSubList[i] = fileSubList[i].split("/");
			if (fileSubList[i].length > splitDirectoryLength) {
				if (fileSubList[i][splitDirectoryLength].includes(".classname")) {
					firstLevel = true;
					enableMenu(sendDataMenu);
					//We remove the .classname to obtain the classpath
					var filePath = clickedElement + fileSubList[i][splitDirectoryLength].substring(0, fileSubList[i][splitDirectoryLength].length - 10);
					filePathToSend.push(filePath);
				}
			}
		}
		//if no element has been found inside clickedElement folder, we check if the next folder level contains a .classname file
		if (firstLevel == false) {
			for (var i = 0; i < fileSubList.length; i++) {
				if (fileSubList[i].length > splitDirectoryLength + 1) {
					if (fileSubList[i][splitDirectoryLength + 1].includes(".classname")) {
						//activate send data to ES in the context menu
						enableMenu(sendDataMenu);
						//add the file path to the list
						var fileName = fileSubList[i][splitDirectoryLength + 1].substring(0, fileSubList[i][splitDirectoryLength + 1].length - 10);
						var filePath = clickedElement + fileSubList[i][splitDirectoryLength] + "/" + fileName;
						filePathToSend.push(filePath);
					}
				}
			}
		}
		localCache.set("fileSentToElastic", filePathToSend);
	}
}

/**
 *This function retrieves the url of the files that will be sent to ElasticSearch and 
 *send it via an ajax call.
 */
function checkIndexElasticSearch() {
	var filePathToSend = localCache.get("fileSentToElastic");
	for (var i = 0; i < filePathToSend.length; i++) {
		console.log(filePathToSend[i]);
	}
	filePathToSend = JSON.stringify(filePathToSend);
	console.log(filePathToSend);

	$.ajax({
		url: "/checkIndexElasticSearch",
		type: 'POST',
		dataType: "text",
		data: filePathToSend,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			//clear the modal box
			clearModalElasticSearch();
			console.log(response);
			var parsedResponse = JSON.parse(response);
			console.log(parsedResponse);
			//complete the modal ElasticSearch window
			checkboxIds = confirmModalElasticSearch(parsedResponse);
			displayModalElastiSearch(checkboxIds, parsedResponse);

		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
}
/**
 * Adds elements to a modalElasticSearch window to request user confirmation before sending data to ElasticSearch.
 * @param {Array} jsonResponse - a Json object containing the index, the url and the file size in octet
 * @returns {Array} The ids of the checkbox. Used later to retrieve the url of the sent file
 */
function confirmModalElasticSearch(jsonResponse) {
	var choiceElement = document.getElementById("contentElasticSearchChoice");
	var title = document.getElementById("modalElasticSearchTitle");
	title.innerText = getLocalizedText("elasticSearch.titleConfirm");
	var checkboxIds = [];
	//info about checkbox
	var divInfo = document.createElement("div");
	divInfo.innerHTML = getLocalizedText("elasticSearch.info");
	choiceElement.append(divInfo);
	//Add the checkboxs and the informations about the indexes
	for (var i = 0; i < jsonResponse["response"].length; i++) {
		//add a checkbox and its label
		var indexName = jsonResponse["response"][i]["index"];
		var filePath = jsonResponse["response"][i]["filepath"];
		var fileSize = jsonResponse["response"][i]["fileSize"];
		var formattedSize = displayFileSize(fileSize);

		//checkbox. Contain the filePath and the index name
		var divCheckbox = document.createElement("div");
		var checkbox = document.createElement("INPUT")
		checkbox.type = "checkbox";
		checkbox.name = filePath;
		checkbox.id = indexName;
		checkboxIds.push(indexName);
		//checkbox label
		var labelCheckbox = document.createElement("label");
		labelCheckbox.htmlFor = indexName;
		divCheckbox.append(checkbox);
		divCheckbox.append(labelCheckbox);
		choiceElement.append(divCheckbox);

		if (jsonResponse["response"][i]["indexExist"] == "true") {
			//checkbox
			labelCheckbox.innerHTML = "* " + filePath + " (" + formattedSize + ")";
		}
		else {
			//checkbox
			labelCheckbox.innerHTML = filePath + " (" + formattedSize + ")";
			checkbox.checked = true;
		}

	}

	return checkboxIds;
}

/**
 * This function is used to format the received fileSize
 * @param {String} fileSize - The file size in octet/byte
 * @returns {String} The formatted fileSize and its unit 
 */
function displayFileSize(fileSize) {
	var fileInfo = "";
	var temp = "";
	if (fileSize.length <= 3) { //octet
		fileInfo = fileSize + " " + getLocalizedText("unit.octet");
	}
	else if (fileSize.length <= 6) { //kO
		temp = fileSize.substring(0, fileSize.length - 2);
		fileInfo = temp.substring(0, temp.length - 1) + getLocalizedText("decimalSeparator.separator") + temp.substring(temp.length - 1) + " k" + getLocalizedText("unit.octet.short");
	}
	else if (fileSize.length <= 9) { //M0
		temp = fileSize.substring(0, fileSize.length - 5);
		fileInfo = temp.substring(0, temp.length - 1) + getLocalizedText("decimalSeparator.separator") + temp.substring(temp.length - 1) + " M" + getLocalizedText("unit.octet.short");
	}
	else if (fileSize.length <= 12) { //G0
		temp = fileSize.substring(0, fileSize.length - 8);
		fileInfo = temp.substring(0, temp.length - 1) + getLocalizedText("decimalSeparator.separator") + temp.substring(temp.length - 1) + " G" + getLocalizedText("unit.octet.short");
	}
	else { //T0
		temp = fileSize.substring(0, fileSize.length - 11);
		fileInfo = temp.substring(0, temp.length - 1) + getLocalizedText("decimalSeparator.separator") + temp.substring(temp.length - 1) + " T" + getLocalizedText("unit.octet.short");
	}
	return fileInfo;
}

/**
 *Displays a modal window offering the user to overwrite the data on ElasticSearch
 *@param {Array} checkboxIds - An array containing the ids of the checkbox. Used to retrieve the path to the file we send to Elastic
 *@param {String} parsedJson - A String containing te datas returned by the server. Used to knox if an index already exist
 */
function displayModalElastiSearch(checkboxIds, parsedJson) {
	$('#modalElasticSearch').modal('show');
	$('#modalElasticSearch').removeAttr('hidden');
	document.getElementById("elasticSearchConfirm").onclick = function() { replaceTestData(checkboxIds, parsedJson) };
}


/**
 *This function retrieves the path of the files that will be sent to ElasticSearch and 
 *send it via an ajax call. The name of each checkbox contains the path of the file to send.
 *@param {Array} checkboxIds - The ids of the checkbox created in @see {@link confirmModalElasticSearch}.
 */
function replaceTestData(checkboxIds) {
	var filesToSend = [];
	for (var i = 0; i < checkboxIds.length; i++) {
		var element = document.getElementById(checkboxIds[i]);
		if (element.checked == true) {
			filesToSend.push(element.name)
		}
	}
	if (filesToSend.length > 0) {

		filePathToSend = JSON.stringify(filesToSend);

		$.ajax({
			url: "/replaceElasticData",
			type: 'POST',
			dataType: "text",
			data: filePathToSend,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				//clear the modal box
				clearModalElasticSearch();
				console.log(response);
				var parsedResponse = JSON.parse(response);
				console.log(parsedResponse);
				//complete the modal ElasticSearch window
				successModalElasticSearch(parsedResponse);
				//set a one second timeout
				setTimeout(function() { displayModalElastiSearchSummary() }, 1000);


			},
			error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText)
			}
		});
	}
	else {
		showMessage(getLocalizedText("elasticSearch.cancel"));
	}
}


/**
 * Adds elements to the modalElasticSearch window to dislay a summary about the sent data
 *@param {Array} jsonResponse - a Json object returned by the server
 */
function successModalElasticSearch(jsonResponse) {
	var title = document.getElementById("modalElasticSearchTitle");
	title.innerText = getLocalizedText("elasticSearch.titleSummary");
	var divChoice = document.getElementById("contentElasticSearchChoice");
	//new index :
	var divNewIndexes = document.createElement("DIV");
	divNewIndexes.innerHTML = getLocalizedText("elasticSearch.createdIndex");
	divChoice.append(divNewIndexes);
	//summary of the indexes
	for (var i = 0; i < jsonResponse["response"].length; i++) {
		//add informations about the index
		var indexName = jsonResponse["response"][i]["index"];
		var divAbstract = document.createElement("div");
		divAbstract.innerHTML = indexName;
		divChoice.append(divAbstract);
	}
}

/**
 *Displays a modal window displaying informations about the data sent to ElasticSearch
 *Replace the confirm button in order to remove the associated function
 */
function displayModalElastiSearchSummary() {
	var button = document.getElementById("elasticSearchConfirm");
	button.parentNode.removeChild(button);
	var newButton = document.createElement("button");
	var parent = document.getElementById("modalFooterElasticSearch");
	newButton.id = "elasticSearchConfirm";
	newButton.className += "btn btn-success";
	newButton.innerHTML = getLocalizedText("elasticSearch.confirm");
	newButton.setAttribute("data-dismiss", "modal");
	parent.append(newButton);

	$('#modalElasticSearch').modal('show');
	$('#modalElasticSearch').removeAttr('hidden');
}

/**
 * Clear the different field for the ElasticSearch modal window
 */
function clearModalElasticSearch() {
	var modalBodyChoice = document.getElementById("contentElasticSearchChoice");
	while (modalBodyChoice.firstChild) {
		modalBodyChoice.removeChild(modalBodyChoice.firstChild);
	}

}

