/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 *Create an event listener to catch when the user's mouse is on the folding arrow
 */
if (document.addEventListener) {
	document.addEventListener('mouseover', function(event) {
		if (isDescendant(document.getElementById("scenarioYAML"), event.target)) {
			var editor = ace.edit(getActiveEditor());
			var closedFolds = document.getElementsByClassName("ace_closed");
			var target = event.target;
			var booleanClosedFold = false;
			var targetedFolds;

			for (var i = 0; i < closedFolds.length && booleanClosedFold == false; i++) {
				if (closedFolds[i] == target) {
					//obtain the target line number
					var targetInnerHtml = target.parentNode.innerHTML;
					var temp = targetInnerHtml.split("<");	//temp is formed by a line number followed by an yaml tag
					var targetFirstLine = parseInt(temp[0], 10);
					targetedFolds = closedFolds[i];

					//obtain the parents line numbers
					var completeText = editor.getValue();
					var arraySplitText = completeText.split("\n");

					var endLine = retrieveLineNumbersYaml(target, targetFirstLine, arraySplitText.length);

					//retrieve the folded text
					var retrievedText = "";
					for (var j = targetFirstLine - 1; j < endLine; j++) {
						retrievedText += arraySplitText[j] + "\n";
					}


					var popupTxt = document.getElementById("popupFoldingText");
					var popupBox = document.getElementById("popupFolding");

					var resumedText = switchRetrievedTextYaml(retrievedText);

					var comment = handleCommentYaml(retrievedText);

					resumedText = comment + resumedText;

					//display the popup
					if (resumedText != null && resumedText != undefined && resumedText != "") {
						popupFoldingStyle(popupTxt, popupBox, resumedText, target);
						popupTxt.classList.toggle("show");
					}
					booleanClosedFold = true;
				}
			}
			//close the popup
			if (target != targetedFolds && target != document.getElementById("popupFoldingText") && target != document.getElementById("popupFolding")) {
				var popupTxt = document.getElementById("popupFoldingText");
				popupTxt.classList.toggle("show", false);
			}
		}
	});
	//close the popup when the user clicks on the closing arrow
	document.addEventListener('click', function(event) {
		if (isDescendant(document.getElementById("scenarioYAML"), event.target)) {
			var target = event.target;
			var targetClassList = target.classList;
			if (targetClassList.contains("ace_closed")) {
				var popupTxt = document.getElementById("popupFoldingText");
				popupTxt.classList.toggle("show", false);
			}
		}
	}, true);
}


/**
 * This function determines the last line of a folded text portion on the ace yaml editor
 * @param {Object} target - the object on which the event occurs
 * @param {int} targetFirstLine - the first line of the folded text
 * @param {int} textLineNumber - the number of line on that the file contains
 * @return {int} The last line that is folded
 */
function retrieveLineNumbersYaml(target, targetFirstLine, textLineNumber) {
	//the usual class of the line numbers is "ace_gutter-cell". Whene a line is selected, 
	//it is changed to ace_gutter-cell.ace_gutter-active-line
	var targetParentClassName = target.parentNode.className;
	if (targetParentClassName.includes(" ")) {
		var targetParentClassArray = targetParentClassName.split(" ");
		for (var i = 0; i < targetParentClassArray.length; i++) {
			if (targetParentClassArray[i].includes("cell")) {
				targetParentClassName = targetParentClassArray[i];
			}
		}
	}
	var xmlEditor = document.getElementById("scenarioYAML");
	var parentsArray = xmlEditor.getElementsByClassName(targetParentClassName);

	var parentsLineNumber = []
	for (var j = 0; j < parentsArray.length; j++) {
		//the obtained innerHtml has the following format : number<span ...>
		//only the  line number matters for us
		var temp = parentsArray[j].innerHTML.split("<");
		parentsLineNumber.push(parseInt(temp[0], 10));
	}

	//obtain the final line to retrieve
	//Exemple: If the text id folded between line 5 and 10, the elements of the parent class include lines 5 and 11
	var counter = 0;
	while (parentsLineNumber[counter] <= targetFirstLine) {
		counter++;
	}
	var endLine = parentsLineNumber[counter] - 1;
	if (isNaN(endLine)) {
		endLine = textLineNumber;
	}
	return endLine;
}


/**
 * This function allows to know which function will be called to create the text summarizing the folded yaml code 
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function switchRetrievedTextYaml(retrievedText) {
	var retrievedTextArray = retrievedText.split("\n");
	var resumedText = "";
	for (var i = 0; i < retrievedTextArray.length - 1; i++) {
		retrievedTextArray[i] += ">";
	}
	if (retrievedTextArray[0].includes("scenario:")) {
		resumedText = foldedScenarioYaml(retrievedText);
	}
	else if (retrievedTextArray[0].includes("behaviors:")) {
		resumedText = foldedBehaviorsYaml(retrievedText);
	}
	if (retrievedTextArray[0].includes("use:")) {
		resumedText = foldedUseYaml(retrievedText);
	}
	if (retrievedTextArray[0].includes("plugins:")) {
		resumedText = foldedPluginsYaml(retrievedText);
	}
	else if (retrievedTextArray[0].includes("loadprofile:")) {
		resumedText = foldedLoadprofilesYaml(retrievedText);
	}
	else if (retrievedTextArray[0].includes("control:")) {
		resumedText = foldedPrimitiveYaml(retrievedText, "Control");
	}
	else if (retrievedTextArray[0].includes("sample:")) {
		resumedText = foldedPrimitiveYaml(retrievedText, "Sample");
	}
	else if (retrievedTextArray[0].includes("timer:")) {
		resumedText = foldedPrimitiveYaml(retrievedText, "Timer");
	}
	else if (retrievedTextArray[0].includes("condition:")) {
		resumedText = foldedConditionYaml(retrievedText);
	}
	else if (retrievedTextArray[0].includes("params:")) {
		resumedText = foldedParamsYaml(retrievedText);
	}
	else if (retrievedTextArray[0].includes("nchoice")) {
		resumedText = foldedNchoiceYaml(retrievedText);
	}
	else if (retrievedTextArray[0].includes("if:")) {
		resumedText = foldedStatementYaml(retrievedText, "If");
	}
	else if (retrievedTextArray[0].includes("while:")) {
		resumedText = foldedStatementYaml(retrievedText, "While");
	}
	else if (retrievedTextArray[0].includes("preemptive:")) {
		resumedText = foldedStatementYaml(retrievedText, "Preemptive");
	}
	return resumedText;
}


/**
 * This function summarizes the retrieved text if the first yaml line includes "scenario:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedScenarioYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	var textPlugins = textAsObject["scenario"]["behaviors"]["plugins"]["use"];
	var textBehavior = textAsObject["scenario"]["behaviors"]["behavior"];

	var resumedText = getLocalizedText("folding.scenario.behaviors");
	if (textBehavior.length > 1) {
		for (var j = 0; j < textBehavior.length; j++) {
			resumedText += "\t" + textBehavior[j]["id"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textBehavior["id"] + "\n";
	}

	resumedText += getLocalizedText("folding.scenario.plugins");
	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["name"] + "\n";
	}

	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "behaviors:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedBehaviorsYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	var textPlugins = textAsObject["behaviors"]["plugins"]["use"];
	var textBehavior = textAsObject["behaviors"]["behavior"];

	var resumedText = getLocalizedText("folding.scenario.behaviors");
	if (textBehavior.length > 1) {
		for (var j = 0; j < textBehavior.length; j++) {
			resumedText += "\t" + textBehavior[j]["id"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textBehavior["id"] + "\n";
	}

	resumedText += getLocalizedText("folding.scenario.plugins");
	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["name"] + "\n";
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "plugins:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedPluginsYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	var textPlugins = textAsObject["plugins"]["use"];

	var resumedText = getLocalizedText("folding.plugins");

	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["id"] + " : " + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["id"] + " : " + textPlugins["name"] + "\n";
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "use:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedUseYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	var textPlugins = textAsObject["use"];

	var resumedText = getLocalizedText("folding.plugins");

	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["id"] + " : " + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["id"] + " : " + textPlugins["name"] + "\n";
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "primitive_type:"
 * @param {String} retrievedText - The text that will be summarized
 * @param {String} primitive - The type of primitive (control, sample, timer)
 * @return {String} The summarized text
 */
function foldedPrimitiveYaml(retrievedText, primitive) {
	var textAsObject = jsyaml.load(retrievedText);
	var textPrimitive = textAsObject[0][primitive.toLowerCase()];
	var resumedText = primitive + " " + textPrimitive["use"] + "." + textPrimitive["name"] + "\n";
	if (textPrimitive.hasOwnProperty("params")) {
		var textControl = textPrimitive["params"];

		for (var k = 0; k < textControl.length; k++) {
			resumedText += "\t" + textControl[k]["name"] + " : " + textControl[k]["value"] + "\n";
		}
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first is yaml line includes "params:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedParamsYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	if (textAsObject["params"].length > 0) {

		var resumedText = "Params" + "\n" + getLocalizedText("folding.control");

		if (textAsObject["params"].length > 1) {
			for (var k = 0; k < textAsObject["params"].length; k++) {
				resumedText += "\t" + textAsObject["params"][k]["name"] + " : " + textAsObject["params"][k]["value"] + "\n";
			}
		}
		else {
			resumedText += "\t" + textAsObject["params"]["name"] + " : " + textAsObject["params"]["value"] + "\n";
		}
		return resumedText;
	}
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "nchoice:
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedNchoiceYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	if (textAsObject[0].hasOwnProperty("nchoice")) {
		var numberOfChoices = textAsObject[0]["nchoice"].length;
		var totalWeight = 0;
		if (numberOfChoices >= 1) {
			for (var i = 0; i < numberOfChoices; i++) {
				totalWeight += parseInt(textAsObject[0]["nchoice"][i]["proba"], 10);
			}
		}
		var resumedText = getLocalizedText("folding.nchoice").replace("%1", numberOfChoices).replace("%2", totalWeight);

		return resumedText;
	}
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "statement_type:"
 * @param {String} retrievedText - The text that will be summarized
 * @param {String} statement - The type of statement (if, while, preemptive)
 * @return {String} The summarized text
 */
function foldedStatementYaml(retrievedText, statement) {
	var textAsObject = jsyaml.load(retrievedText);
	var textStatement = textAsObject[0][statement.toLowerCase()];
	if (textStatement.hasOwnProperty("condition")) {
		var resumedText = statement + " " + textStatement["condition"]["use"] + "." + textStatement["condition"]["name"] + "\n";
		if (textStatement["condition"].hasOwnProperty("params")) {
			var params = textStatement["condition"]["params"];
			if (params.length >= 1) {
				for (var k = 0; k < params.length; k++) {
					resumedText += "\t" + params[k]["name"] + " : " + params[k]["value"] + "\n";
				}
			}
		}
	}
	else {
		var resumedText = "";
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "condition:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedConditionYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	var textPrimitive = textAsObject["condition"];
	var resumedText = "Condition" + " " + textPrimitive["use"] + "." + textPrimitive["name"] + "\n";
	if (textPrimitive.hasOwnProperty("params")) {
		var textControl = textPrimitive["params"];

		for (var k = 0; k < textControl.length; k++) {
			resumedText += "\t" + textControl[k]["name"] + " : " + textControl[k]["value"] + "\n";
		}
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first yaml line includes "loaprofile:"
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedLoadprofilesYaml(retrievedText) {
	var textAsObject = jsyaml.load(retrievedText);
	var resumedText = getLocalizedText("folding.loadprofile");

	if (textAsObject["loadprofile"].hasOwnProperty("group")) {
		if (textAsObject["loadprofile"]["group"].length > 1) {
			for (var i = 0; i < textAsObject["loadprofile"]["group"].length; i++) {
				resumedText += "\t" + textAsObject["loadprofile"]["group"][i]["behavior"];
			}
		}
		else {
			resumedText += "\t" + textAsObject["loadprofile"]["group"]["behavior"] + "\n";
		}
	}

	return resumedText;
}

/**
 * This function is used to retrieve comments inside the text that we have to summarize
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The comments that must be kept 
 */
function handleCommentYaml(retrievedText) {
	var arrayRetrievedText = retrievedText.split("\n");
	var comment = "";
	var spaceNumber = 0

	//count how many space there is on the first line of the text that is folded
	var firstRow = arrayRetrievedText[0];
	for (var i = 0; firstRow[i] == " "; i++) {
		spaceNumber++;
	}

	var extraSpaceNumber
	//valid extra space number
	if (firstRow.includes("plugins:") || firstRow.includes("control:") || firstRow.includes("sample:") ||
		firstRow.includes("timer:") || firstRow.includes("if:") || firstRow.includes("while:") ||
		firstRow.includes("preemptive:") || firstRow.includes("nchoice")) {
		extraSpaceNumber = 4;
	}
	else if(firstRow.includes("scenario:") || firstRow.includes("behaviors:") || firstRow.includes("loadprofiles:")
		|| firstRow.includes("condition") || firstRow.includes("use")){
		extraSpaceNumber = 2;
	}


	//browse the retrieve text to find the comment.
	for (var j = 1; j < arrayRetrievedText.length; j++) {
		if (arrayRetrievedText[j].includes("#")) {
			//retrieve the first row with no comment
			var rowNumberNoComment = getNextRowWithoutComment(j, arrayRetrievedText, spaceNumber, extraSpaceNumber);
			var rowNoComment = arrayRetrievedText[rowNumberNoComment];
			//check the "spaceNumber + extraSpaceNumber" character of the first line with no comment
			var k = 2;
			var commentAdded = false;
			while (k <= extraSpaceNumber && commentAdded != true) {
				if (rowNoComment[spaceNumber + k] != " ") {
					comment += arrayRetrievedText[j] + "\n";
					commentAdded = true;
				}
				else{
					k += 2;
				}
			}

		}
	}
	//format comment
	var commentArray = comment.split("\n");
	var newComment = "";
	for (var i = 0; i < commentArray.length; i++) {
		commentArray[i] = commentArray[i].substring(commentArray[i].indexOf("#"));
		newComment += commentArray[i] + "\n";
	}

	return newComment.substring(0, newComment.length - 1);
}

function getNextRowWithoutComment(currentLine, arrayRetrievedText, spaceNumber, extraSpaceNumber) {
	var lineNoCommentFound = false;
	var observedLineNumber = currentLine + 1;
	while (lineNoCommentFound == false) {
		//observed line doesn't have a comment
		if (arrayRetrievedText[observedLineNumber].includes("#") == false) {
			lineNoCommentFound = true;
		}
		else {
			var observedLine = arrayRetrievedText[observedLineNumber];
			//current line has a comment after the position whose value must be different from " "
			if (spaceNumber + extraSpaceNumber < observedLine.indexOf("#")) {
				lineNoCommentFound = true;
			}
			//the comment is located before the said position
			else {
				observedLineNumber++;
			}
		}
	}
	return observedLineNumber;

}

