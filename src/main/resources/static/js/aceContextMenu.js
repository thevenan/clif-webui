/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 *This function access the local cache in order to add a list of plugins and primitives to the
 *Ace context menu
 */

/*------------------------------------------------------------------	
---------------------Ace Context menu handler-----------------------
-------------------------------------------------------------------*/
/**
 * This function resets and enable the different items of the editor's context menu
 */
function aceContextMenuHandler() {
	removeAceContextMenu();
	clearContextMenu();

	var booleanDisablePrimitive = disableMenuItems("behavior");
	var booleanDisablePlugins = disableMenuItems("plugins");
	var booleanDisableLoadprofile = disableMenuItems("loadprofile");
	var booleanDisableBehavior = disableMenuItems("behaviors");

	if (booleanDisablePrimitive == false) {
		enableContextualSubmenu("NewStatementMenu");
		enableContextualSubmenu("parentControlSubmenu");
		enableContextualSubmenu("parentTimerSubmenu");
		enableContextualSubmenu("parentSampleSubmenu");
	}
	if (booleanDisablePlugins == false) {
		enableContextualSubmenu("parentPluginSubmenu");
	}
	if (booleanDisableLoadprofile == false) {
		enableContextualSubmenu("parentLoadprofileSubmenu");
	}
	if (booleanDisableBehavior == false) {
		enableContextualSubmenu("parentBehaviorSubmenu");
	}

}

/**
 * This function removes a css class that prevent the user to click on a contextmenu item
 */
function enableContextualSubmenu(menuId) {
	var selectedSubmenu = document.getElementById(menuId);
	if (selectedSubmenu.classList.contains("noHover") == true) {
		selectedSubmenu.classList.remove("noHover");
	}
}

/**
 *This function browses the editor to determine where the user right-cliked.
 *If the user didn't'right clicked between the ">" and "<", the returned value 
 *will disable some menu items
 *@param {String} tagType - Depending of tagType value, the checks will be done for different menu items
 *@return {boolean} If disable == true, some menu items will be disabled
 */
function disableMenuItems(tagType) {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.selection.getCursor();
	var completeText = editor.getValue();
	var cursorAbsolutePosition = 0;
	var disable = true;

	//obtain the cursor absolute position
	var arrayCompleteText = completeText.split("\n");
	for (var i = 0; i < arrayCompleteText.length - 1; i++) {
		arrayCompleteText[i] += "\n";
	}
	for (var counterRowLine = 0; counterRowLine < cursorPosition["row"]; counterRowLine++) {
		cursorAbsolutePosition += arrayCompleteText[counterRowLine].length;
	}
	cursorAbsolutePosition += cursorPosition["column"];

	//depanding on the tag type, we check if we are between two specific tags
	disable = disableBetweenSpecificTag(tagType, completeText, cursorAbsolutePosition);

	//We are inside the wanted tags, check if the cursor is between > and <
	if (disable == false) {
		var openChevronPos = completeText.indexOf("<", cursorAbsolutePosition);
		var closeChevronPos = completeText.lastIndexOf(">", cursorAbsolutePosition);
		var tempCloseChevron = completeText.indexOf(">", cursorAbsolutePosition)
		if (openChevronPos > closeChevronPos && cursorAbsolutePosition > closeChevronPos && tempCloseChevron > openChevronPos) {
			disable = false;
			//check for comments
			var temp = openChevronPos;
			var indexComment = completeText.indexOf("<!--", cursorAbsolutePosition);
			while (temp == indexComment) {
				indexComment = completeText.indexOf("<!--", indexComment + 1);
				temp = completeText.indexOf("<", temp + 1);
				openChevronPos = temp;
			}
		}
		else {
			disable = true;
		}
	}
	//We are between > and <. Check if the next closing tag allows to insert content
	if (disable == false) {
		disable = disableUncorrectNextTag(tagType, completeText, openChevronPos);
	}
	return disable;
}

/**
 * This function checks if the user's cursor is between two specific major tags
 * @param {String} tagType - Depending of tagType value, the checks will be done for different tag 
 * @param {String} completeText - The text contained on the editor
 * @param {number} cursorAbsolutePosition - a number indicating at which character the cursor is located
 * @returns {boolean} If disable = true, the menu items corresponding to tagType must be disaabled
 */
function disableBetweenSpecificTag(tagType, completeText, cursorAbsolutePosition) {
	var disable = true;
	if (tagType == "behavior") {
		//add to an array the <behavior and </behavior> positions
		//the first value of behaviorBeginTagArray is shifted as the <behaviors> tag is taken into account

		var behaviorBeginTagArray = getMatchIndexes(completeText, "<behavior");
		var behaviorEndTagArray = getMatchIndexes(completeText, "</behavior>");

		behaviorBeginTagArray.shift();

		for (var i = 0; i < behaviorBeginTagArray.length; i++) {
			if (cursorAbsolutePosition >= behaviorBeginTagArray[i] && cursorAbsolutePosition <= behaviorEndTagArray[i] + "</behavior>".length) {
				disable = false;
			}
		}
	}
	else if (tagType == "plugins") {
		var pluginsBeginTagArray = getMatchIndexes(completeText, "<plugins");
		var pluginsEndTagArray = getMatchIndexes(completeText, "</plugins>");

		for (var i = 0; i < pluginsBeginTagArray.length; i++) {
			if (cursorAbsolutePosition >= pluginsBeginTagArray[i] && cursorAbsolutePosition <= pluginsEndTagArray[i] + "</plugins>".length) {
				disable = false;
			}
		}
	}
	else if (tagType == "loadprofile") {
		var loadprofileBeginTagArray = getMatchIndexes(completeText, "<loadprofile");
		var loadprofileEndTagArray = getMatchIndexes(completeText, "</loadprofile>");

		for (var i = 0; i < loadprofileBeginTagArray.length; i++) {
			if (cursorAbsolutePosition >= loadprofileBeginTagArray[i] && cursorAbsolutePosition <= loadprofileEndTagArray[i] + "</loadprofile>".length) {
				disable = false;
			}
		}
	}

	else if (tagType == "behaviors") {
		var loadprofileBeginTagArray = getMatchIndexes(completeText, "<behaviors");
		var loadprofileEndTagArray = getMatchIndexes(completeText, "</behaviors>");

		for (var i = 0; i < loadprofileBeginTagArray.length; i++) {
			if (cursorAbsolutePosition >= loadprofileBeginTagArray[i] && cursorAbsolutePosition <= loadprofileEndTagArray[i]) {
				disable = false;
			}
		}
	}
	return disable;
}

/**
 * This function checks if the next allows to insert content before it
 * @param {String} tagType - Depending of tagType value, the checks will be done for different tag 
 * @param {String} completeText - The text contained on the editor
 * @param {number} openChevronPos - a number indicating at which character the next "<" is located
 * @returns {boolean} If disable = true, the menu items corresponding to tagType must be disaabled
 */
function disableUncorrectNextTag(tagType, completeText, openChevronPos) {
	//retrieve next tag name
	var disable = false;
	var openTagSpace = completeText.indexOf(" ", openChevronPos); //next tag = <tagName ...>
	var openTagClose = completeText.indexOf(">", openChevronPos); //next tag = <tagName>
	var openTagClose2 = completeText.indexOf("/>", openChevronPos); //next tag = </tagName>
	var tagAfter = "";
	if (openTagSpace != -1 && (openTagSpace < openTagClose || openTagClose == -1) && (openTagSpace < openTagClose2 || openTagClose2 == -1)) {
		tagAfter = completeText.substring(openChevronPos + 1, openTagSpace);
	}
	else if (openTagClose != -1 && (openTagClose < openTagSpace || openTagSpace == -1) && (openTagClose < openTagClose2 || openTagClose2 == -1)) {
		tagAfter = completeText.substring(openChevronPos + 1, openTagClose);
	}
	else {
		tagAfter = completeText.substring(openChevronPos + 1, openTagClose2);
	}
	//the supported following next tags are not the same depending on the tag type
	if (tagType == "behavior") {
		if (tagAfter == "control" || tagAfter == "sample" || tagAfter == "timer"
			|| tagAfter == "if" || tagAfter == "while" || tagAfter == "preemption" || tagAfter == "nchoice"
			|| tagAfter == "/while" || tagAfter == "/preemption" || tagAfter == "/nchoice"
			|| tagAfter == "/then" || tagAfter == "/else" || tagAfter == "/behavior" || tagAfter == "/choice") {
			disable = false;
		}
		else {
			disable = true;
		}
	}
	else if (tagType == "plugins") {
		if (tagAfter == "use" || tagAfter == "/plugins") {
			disable = false;
		}
		else {
			disable = true;
		}
	}

	else if (tagType == "loadprofile") {
		if (tagAfter == "group" || tagAfter == "/loadprofile") {
			disable = false;
		}
		else {
			disable = true;
		}
	}
	else if (tagType == "behaviors") {
		if (tagAfter == "behavior" || tagAfter == "/behaviors") {
			disable = false;
		}
		else {
			disable = true;
		}
	}
	return disable;
}

/**
 *This function is used to obtain the indexes of substring inside a string
 *@param {String} str - The input String
 *@param {String} toMatch - The substring that we are looking for
 *@return {array} indexMatches - An array with the position of the different indexMatches
 */
function getMatchIndexes(str, toMatch) {
	var toMatchLength = toMatch.length,
		indexMatches = [], match,
		i = 0;

	while ((match = str.indexOf(toMatch, i)) > -1) {
		indexMatches.push(match);
		i = match + toMatchLength;
	}

	return indexMatches;
}

function getUniqueVal(value, index, self) {
	return self.indexOf(value) === index;
}


/*------------------------------------------------------------------	
------------------------------Clear---------------------------------
-------------------------------------------------------------------*/

/**
 * This function adds the style class noHover to the statement submenu. This style class prevents the submenu to be clickable
 */
function clearContextMenu() {
	var controlSubmenu = document.getElementById("parentControlSubmenu");
	var timerSubmenu = document.getElementById("parentTimerSubmenu");
	var sampleSubmenu = document.getElementById("parentSampleSubmenu");
	var statementSubmenu = document.getElementById("NewStatementMenu");
	var pluginsSubmenu = document.getElementById("parentPluginSubmenu");
	var loadprofileSubmenu = document.getElementById("parentLoadprofileSubmenu");
	var behaviorSubmenu = document.getElementById("parentBehaviorSubmenu");

	if (controlSubmenu.classList.contains("noHover") == false) {
		controlSubmenu.classList.add("noHover");
	}
	if (timerSubmenu.classList.contains("noHover") == false) {
		timerSubmenu.classList.add("noHover");
	}
	if (sampleSubmenu.classList.contains("noHover") == false) {
		sampleSubmenu.classList.add("noHover");
	}
	if (statementSubmenu.classList.contains("noHover") == false) {
		statementSubmenu.classList.add("noHover");
	}
	if (pluginsSubmenu.classList.contains("noHover") == false) {
		pluginsSubmenu.classList.add("noHover");
	}
	if (loadprofileSubmenu.classList.contains("noHover") == false) {
		loadprofileSubmenu.classList.add("noHover");
	}
	if (behaviorSubmenu.classList.contains("noHover") == false) {
		behaviorSubmenu.classList.add("noHover");
	}



}


/*------------------------------------------------------------------	
-------------------------Scan Ace Editor----------------------------
-------------------------------------------------------------------*/

/**
 *This function scans the xml ace editor to retrieve the plugins used for the current xis file
@returns {Array} An array that contains the used plugins
 */
function scanPlugins() {
	var editor = ace.edit(getActiveEditor());
	var textToAnalyse = editor.getValue();
	var arrayActivePlugins = [];
	var useTagContent;
	var tmpPluginName;

	if (textToAnalyse.indexOf("<plugins>") != -1 && textToAnalyse.indexOf("</plugins>") != -1) {
		textToAnalyse = textToAnalyse.substring(textToAnalyse.indexOf("<plugins>"), textToAnalyse.indexOf("</plugins>"));

		var indexUseTag = textToAnalyse.indexOf("<use");
		var indexUseEndTag = textToAnalyse.indexOf("</use>");

		while (indexUseTag != -1) {
			useTagContent = textToAnalyse.substring(indexUseTag, indexUseEndTag);
			tmpPluginName = useTagContent.substring(useTagContent.indexOf("name"), useTagContent.indexOf(">"));
			tmpPluginName = tmpPluginName.replaceAll('\"', '');
			tmpPluginName = tmpPluginName.replaceAll('name', '');
			tmpPluginName = tmpPluginName.replaceAll(' ', '');
			tmpPluginName = tmpPluginName.replaceAll('=', '');
			arrayActivePlugins.push(tmpPluginName);
			indexUseTag = textToAnalyse.indexOf("<use", indexUseTag + 1);
			indexUseEndTag = textToAnalyse.indexOf("</use>", indexUseEndTag + 1);
		}
	}
	return arrayActivePlugins;
}

/**
 * This function scans the ace editor content and retrieve the ids of a specific plugin
 * @param {String} pluginName 
 * @retrun {Array} An array that contains the ids associated to pluginName
 */
function scanPluginId(pluginName) {
	var arrayPluginId = [];
	var textAsObject = stringToObj();
	if (textAsObject.hasOwnProperty("behaviors")) {
		if (textAsObject["behaviors"]["plugins"].hasOwnProperty("use")) {
			if (textAsObject["behaviors"]["plugins"]["use"].length == undefined) {
				if (textAsObject["behaviors"]["plugins"]["use"]["name"] == pluginName) {
					arrayPluginId.push(textAsObject["behaviors"]["plugins"]["use"]["id"]);
				}
			}
			else {
				var useCount = textAsObject["behaviors"]["plugins"]["use"].length;
				for (var i = 0; i < useCount; i++) {
					if (textAsObject["behaviors"]["plugins"]["use"][i]["name"] == pluginName) {
						arrayPluginId.push(textAsObject["behaviors"]["plugins"]["use"][i]["id"]);
					}
				}
			}

		}
	}

	return arrayPluginId;
}

/**
 * This function scans the ace editor content and defines the default ID that will be assigned to the tag <use>
 * @returns {String} the name that will be given to the inserted plugin 
 */
function choosePluginId(pluginName) {

	var arrayPluginId = scanPluginId(pluginName);
	var extension = 0;
	var i = 0;
	var nameToTest = pluginName + "_" + extension;
	while (i < arrayPluginId.length) {
		nameToTest = pluginName + "_" + extension;
		if (nameToTest == arrayPluginId[i]) {
			i = 0;
			extension++;
		}
		else {
			i++;
		}
	}
	return nameToTest;
}

/**
 * This function scans the ace editor content and retrieve the ids of each behavior
 * @retrun {Array} An array that contains the ids associated to the behaviors
 */
function scanBehaviorId() {
	var arrayBehaviorId = [];
	var textAsObject = stringToObj();
	if (textAsObject["behaviors"].hasOwnProperty("behavior")) {
		var useCount = textAsObject["behaviors"]["behavior"].length;
		if (useCount != undefined) {
			for (var i = 0; i < useCount; i++) {
				arrayBehaviorId.push(textAsObject["behaviors"]["behavior"][i]["id"]);
			}
		}
		else {
			arrayBehaviorId.push(textAsObject["behaviors"]["behavior"]["id"])
		}
	}

	return arrayBehaviorId;
}

/**
 * This function scans the ace editor content and defines the default ID that will be assigned to the tag <behavior>
 * @returns {String} the name that will be given to the inserted behavior 
 */
function chooseBehaviorId() {

	var arrayBehaviorId = scanBehaviorId();
	var extension = 0;
	var i = 0;
	var behaviorId = "B" + extension;
	while (i < arrayBehaviorId.length) {
		behaviorId = "B" + extension;
		if (behaviorId == arrayBehaviorId[i]) {
			i = 0;
			extension++;
		}
		else {
			i++;
		}
	}
	return behaviorId;
}
