/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
var timeFading_ms = 2000;
var projectPermission;

var deployedTest = [];

var modalTestResetVal;
var deployed = false;

var testTime = 0;

getLocalizedTexts();
function getLocalizedTexts() {
	$.ajax({
		url: "/getLocalizedTexts",
		type: 'POST',
		success: function(response) {
			listLocalisedText = response;
		}
	});
}
function getLocalizedText(key) {
	return listLocalisedText[key];
}

/*------------------------------------------------------------------	
-----------------Develop and reduce file tree-----------------------
-------------------------------------------------------------------*/


function developDirectory(fileObject) {
	filesList.sort();
	var fileName = fileObject.parentElement.id;
	var div = document.getElementById(fileName);
	if (div.lastChild && div.lastChild.tagName == "UL") {	// delete child to reduce
		div.removeChild(div.lastChild);
	} else {		//develop child of element	
		if (fileName.includes("/")) {
			setCurrentProject(fileName.substring(0, fileName.indexOf('/')));
		}
		else {
			setCurrentProject(fileName);
		}
		var ul = document.createElement('ul');
		div.appendChild(ul);
		for (var index in filesList) {
			var file = $.trim(filesList[index]);
			if (file[0] = ' ') {
				file[0] = '';
			}
			if (file.includes(fileName) && file != fileName && isDirectSon(fileName, file)) {
				createNode(file, ul);
			}
		}
	}
}

function createNode(file, ul) {

	var li = document.createElement('li');
	li.setAttribute("draggable", true);
	li.ondragstart = function() { drag(event) };
	var subdiv = document.createElement('div');
	if (file.includes('/files/')) {
		li.id = file.substring(file.indexOf('/files/') + 7);
	} else {
		li.id = file;
	}
	if (isDirectory(file)) {
		li.ondrop = function() { drop(event) };
		li.ondragover = function() { allowDrop(event) };
		subdiv.setAttribute("isDir", "true");
		subdiv.onclick = function onclick(event) { developDirectory(this); };
		subdiv.innerHTML = '<img data-type="dir" src="/images/directory.png" height="20" width="20" />	' + file.substring(file.lastIndexOf('/') + 1);

		let i = 0;
		while (i < ul.childNodes.length && isDirectory(ul.childNodes[i].id)) { // insert dir in the right place
			if (ul.childNodes[i].id > file) {
				ul.insertBefore(li, ul.childNodes[i]);
				i = ul.childNodes.length;
			}
			i++
		}
		if (i == 0) {
			ul.insertBefore(li, ul.firstChild);
		}
		else {
			ul.appendChild(li);
		}

	} else {		// create a file
		subdiv.onclick = function onclick(event) { display(this); };
		subdiv.setAttribute("isDir", "false");
		subdiv.innerHTML = '<img src="' + getFileImage(file) + '" height="20" width="20" />	' + file.substring(file.lastIndexOf('/') + 1);
		ul.appendChild(li);
	}
	li.appendChild(subdiv);
}

function display(file) {
	if (document.getElementById("deleteme")) document.getElementById("deleteme").remove();
	var path = file.parentNode.id;
	setCurrentProject(path.substring(0, path.indexOf('/')));
	$.ajax({
		url: "/getFileContent",
		type: 'POST',
		dataType: "text",
		data: path,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			response = response.replace(/%0A/g, '\n');
			LoadFileAce(response, path);
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
	var div = document.getElementById("editorContent");
	div.setAttribute("file", path);

}

function getFileImage(fileName) {
	if (fileName.lastIndexOf(".") < 1) return "/images/file.png";
	var extension = fileName.substring(fileName.lastIndexOf("."));
	if (extension == ".xis") {
		return "/images/xis.png";
	} else if (extension == ".ctp") {
		return "/images/ctp.gif"
	} else {
		return "/images/file.png";
	}

}

function isDirectory(file) {
	for (var directory in directories) {
		if (directories[directory] === file || ' ' + directories[directory] === file || directories[directory] === ' ' + file) {
			return true;
		}
	}
	return false;
}

function isDirectSon(file1, file2) {
	var nameFile = file2.substring(file2.lastIndexOf('/') + 1);
	return file1 != file2 && file1 + "/" + nameFile == file2;
}

function isDescendant(parent, child) {
	var node = child.parentNode;
	while (node != null) {
		if (node == parent) {
			return true;
		}
		node = node.parentNode;
	}
	return false;
}



/*------------------------------------------------------------------	
--------------------Functions for CRUD File-------------------------
-------------------------------------------------------------------*/



// Import functions

function thisFileUpload() {
	removeMenu();
	document.getElementById("importFileButton").click();
};
function doUploadFile() {
	var file = document.getElementById("importFileButton").files[0];
	var path = document.getElementById("thisNewFile").getAttribute("value");
	var fileContent;
	if (file) {
		var reader = new FileReader();
		reader.readAsText(file, "UTF-8");
		reader.onload = function(evt) {
			fileContent = evt.target.result;
			$.ajax({
				url: "/importFile",
				type: 'POST',
				dataType: "text",
				data: path + '/' + file.name + '\n' + fileContent,
				beforeSend: function(xhr) {
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				success: function(response) {
					document.getElementById("editorContent").setAttribute("file", path + '/' + file.name);
					LoadFileAce(fileContent, path + '/' + file.name);
					showMessage(response);
					var ul = document.getElementById(document.getElementById("thisNewFile").getAttribute("value")).getElementsByTagName('ul')[0];
					addToFilesList(document.getElementById("thisNewFile").getAttribute("value") + "/" + file.name);
					if (ul) { // div is developped
						var li = document.createElement('li');
						var subdiv = document.createElement('div');
						var ul = document.getElementById(document.getElementById("thisNewFile").getAttribute("value")).getElementsByTagName('ul')[0];
						li.id = document.getElementById("thisNewFile").getAttribute("value") + "/" + file.name;
						subdiv.onclick = function onclick(event) { display(this); };
						subdiv.setAttribute("isDir", "false");
						subdiv.innerHTML = '<img src="' + getFileImage(file.name) + '" height="20" width="20" />	' + li.id.substring(li.id.lastIndexOf('/') + 1);
						li.appendChild(subdiv)
						ul.appendChild(li);
					}
				},
				error: function(jqXhr, textStatus, errorThrown) {
					showMessage(jqXhr.responseText)
				}
			});
		}
		reader.onerror = function(evt) {
			fileContent = "error reading file";
		}
	}

};


function thisProjectUpload() {
	document.getElementById('projectImportButton').click();
}


function initModalImportProject() {
	document.getElementById("modalFormProject").removeAttribute("hidden");
	document.getElementById("uploadDroppedFile").setAttribute("disabled", true);
	document.getElementById("dropContainer").innerHTML = getLocalizedText("project.import.drop");
	document.getElementById("modalBodyImportProjectError").innerHTML = "";
}

function doUploadProject() {
	if (fileToUpload) {
		var reader = new FileReader();
		reader.readAsArrayBuffer(fileToUpload);
		reader.onload = function(evt) {
			document.getElementById("uploadDroppedFile").innerHTML = getLocalizedText("action.import") + '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"/>';
			$.ajax({
				url: "/importProject",
				type: 'POST',
				data: evt.target.result,
				processData: false,
				contentType: false,
				async: false,
				success: function(response) {
					for (i in response) {
						response[i] = response[i].replace(/\//g, "");
						createNewProject(response[i]);
					}
					document.getElementById("uploadDroppedFile").innerHTML = getLocalizedText("action.import")
					showMessage(getLocalizedText("action.projectimport"));
					$('#modalFormProject').modal('hide');
				},
				error: function(jqXhr, textStatus, errorThrown) {
					document.getElementById("uploadDroppedFile").innerHTML = getLocalizedText("action.import");
					document.getElementById("modalBodyImportProjectError").innerHTML =
						jqXhr.responseText.replace(/\[/g, "").replace(/\]/g, "").replace(/\"/g, "");
				}
			});
		}
		reader.onerror = function(evt) {
			console.log(evt)
		}
	}
}


function exportProject() {
	if (currentProject) {
		var url = 'exportProject/' + currentProject;
		var filename = currentProject + '.zip';
		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.responseType = 'blob';
		request.onload = function() {
			var link = document.createElement('a');
			document.body.appendChild(link);
			link.href = window.URL.createObjectURL(request.response);
			link.download = filename;
			link.click();
		};
		request.send();

		/*	$.ajax({
				url: "/exportProject/"+currentProject,
				type: 'GET',
				async: false,
				beforeSend: function(xhr) {  
					xhr.setRequestHeader("Content-type","application/json");
					xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
					xhr.responseType = "arraybuffer";
				},
				success:function(response){
					var bytes = new Uint8Array(response); // pass your byte response to this constructor
					var blob=new Blob([bytes], {type: "application/zip"});// change resultByte to bytes
					var link=document.createElement('a');
					link.href=window.URL.createObjectURL(blob);
					link.download=currentProject+".zip";
					link.click();
					link.remove();
				},error:function(jqXhr, textStatus, errorThrown){
					showMessage(jqXhr.responseText)
				}
			});*/
	}
}



/**
*Initializes the creation of new files
*The extension determines which kind of find will be created
*
* @param {string} extension - The file extension
*/
function initModalNewFile(extension, fromXis) {
	removeMenu();
	document.getElementById("modalFormCreate").removeAttribute("hidden");

	if (extension == "ctp" && fromXis == true) {
		document.getElementById("modalFormCreateTitle").innerText = getLocalizedText("submenu.generatedctp");
		document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.newfile");
		var generatedXisFileName = localCache.get("clickedXisFile");
		generatedXisFileName = generatedXisFileName.substring(0, generatedXisFileName.length - 4) + ".ctp";
		document.getElementById("elementCreateValue").value = generatedXisFileName;
		document.getElementById("modalFormCreateValidate").onclick = function() { thisNewFileWithContent(extension, fromXis) };
	}
	else if (extension == "xis") {
		document.getElementById("modalFormCreateTitle").innerText = getLocalizedText("submenu.newxis");
		document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.newfile");
		document.getElementById("elementCreateValue").value = ".xis";
		document.getElementById("elementCreateValue").setSelectionRange(0, 0);
		document.getElementById("modalFormCreateValidate").onclick = function() { thisNewFileWithContent(extension) };
	}
	else if (extension == "ctp") {
		document.getElementById("modalFormCreateTitle").innerText = getLocalizedText("submenu.newctp");
		document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.newfile");
		document.getElementById("elementCreateValue").value = ".ctp";
		document.getElementById("elementCreateValue").setSelectionRange(0, 0);
		document.getElementById("modalFormCreateValidate").onclick = function() { thisNewFileWithContent(extension) };

	}
	else {
		document.getElementById("modalFormCreateTitle").innerText = getLocalizedText("menu.newfile");
		document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.newfile");
		document.getElementById("elementCreateValue").value = "";
		document.getElementById("modalFormCreateValidate").onclick = function() { thisNewFile() };
	}
}

/**
* Sends a request to create a new file with no extension
 */
function thisNewFile() {
	removeMenu();
	var txt;
	var file = document.getElementById("thisNewFile").getAttribute("value");
	var filePromptName = document.getElementById("elementCreateValue").value;
	if (filePromptName == null || filePromptName == "") {
		showMessage("File need a name.");
	} else {
		filePromptName.replace(' ', '');
		$.ajax({
			url: "/createFile",
			type: 'POST',
			dataType: "text",
			data: file + "/" + filePromptName,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				addToFilesList(file + "/" + filePromptName);
				reloadGlobalFiles();
				showMessage(response);
				LoadFileAce("", file + "/" + filePromptName);
				var ul = document.getElementById(file).getElementsByTagName('ul')[0];
				document.getElementById("editorContent").setAttribute("file", file + "/" + filePromptName);
				if (ul) { // div is developped
					var li = document.createElement('li');
					var subdiv = document.createElement('div');
					li.id = file + "/" + filePromptName
					subdiv.onclick = function onclick(event) { display(this); };
					subdiv.setAttribute("isDir", "false");
					subdiv.innerHTML = '<img src="' + getFileImage(filePromptName) + '" height="20" width="20" />	' + li.id.substring(li.id.lastIndexOf('/') + 1);
					li.appendChild(subdiv)
					ul.appendChild(li);
				}
			},
			error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText)
			}
		});
	}
}


/**
* Sends a request to create a new file
* The extension determines if a xis or a ctp file will be created
* @param {string} extension - The extension of the file. The extension modifies the filename in the request
*/
function thisNewFileWithContent(extension, fromXis) {
	removeMenu();
	var txt;
	var file = document.getElementById("thisNewFile").getAttribute("value");
	var filePromptName = document.getElementById("elementCreateValue").value;
	if (filePromptName == null || filePromptName == "") {
		showMessage("File need a name.");
	}
	else {
		filePromptName.replace(' ', '');
		filePromptName = checkFilePromptName(extension, filePromptName);
		$.ajax({
			url: "/createFileWithContent",
			type: 'POST',
			dataType: "text",
			data: file + "/" + filePromptName,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				addToFilesList(file + "/" + filePromptName);

				response = response.replace(/%0A/g, '\n');
				LoadFileAce(response, file + "/" + filePromptName);

				var ul = document.getElementById(file).getElementsByTagName('ul')[0];
				document.getElementById("editorContent").setAttribute("file", file + "/" + filePromptName);
				if (ul) { // div is developped
					var li = document.createElement('li');
					var subdiv = document.createElement('div');
					li.id = file + "/" + filePromptName
					subdiv.onclick = function onclick(event) { display(this); };
					subdiv.setAttribute("isDir", "false");
					subdiv.innerHTML = '<img src="' + getFileImage(filePromptName) + '" height="20" width="20" />	' + li.id.substring(li.id.lastIndexOf('/') + 1);
					li.appendChild(subdiv)
					ul.appendChild(li);
				}
				reloadGlobalFiles();
				if (fromXis == true) {
					addFileNameTestPlan();
					saveAceText();
				}
			},
			error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText);
			}
		});
	}
}


/**
*Initializes the creation of new files
*The extension determines which kind of find will be executed
*
* @param {string} extension - The file extension
*/
function initDuplicateFile() {
	removeMenu();
	document.getElementById("modalFormCreate").removeAttribute("hidden");
	document.getElementById("modalFormCreateTitle").innerText = document.getElementById("thisDuplicate").innerHTML;
	document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.duplicate");
	var originalFileName = localCache.get("clickedFile");
	var duplicateFileName = "copy of " + originalFileName;
	document.getElementById("elementCreateValue").value = duplicateFileName;
	document.getElementById("modalFormCreateValidate").onclick = function() { thisDuplicate(originalFileName, document.getElementById("elementCreateValue").value) };

}

/**
* Sends a request to create a new file
* The extension determines if a xis or a ctp file will be created
* @param {string} extension - The extension of the file. The extension modifies the filename in the request
*/
function thisDuplicate(originalFileName, duplicatedFileName) {
	removeMenu();
	var file = document.getElementById("thisNewFile").getAttribute("value");

	if (duplicatedFileName == null || duplicatedFileName == "") {
		showMessage("File need a name.");
	}
	else {
		duplicatedFileName.replace(' ', '');
		//filePromptName = checkFilePromptName(extension, filePromptName);
		var data = file + "/" + duplicatedFileName + "\n" + file + "/" + originalFileName;
		$.ajax({
			url: "/duplicateFile",
			type: 'POST',
			dataType: "text",
			data: data,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				addToFilesList(file + "/" + duplicatedFileName);
				reloadGlobalFiles();
				response = response.replace(/%0A/g, '\n');
				LoadFileAce(response, file + "/" + duplicatedFileName);

				var ul = document.getElementById(file).getElementsByTagName('ul')[0];
				document.getElementById("editorContent").setAttribute("file", file + "/" + duplicatedFileName);
				if (ul) { // div is developped
					var li = document.createElement('li');
					var subdiv = document.createElement('div');
					li.id = file + "/" + duplicatedFileName;
					subdiv.onclick = function onclick(event) { display(this); };
					subdiv.setAttribute("isDir", "false");
					subdiv.innerHTML = '<img src="' + getFileImage(duplicatedFileName) + '" height="20" width="20" />	' + li.id.substring(li.id.lastIndexOf('/') + 1);
					li.appendChild(subdiv)
					ul.appendChild(li);
				}
			},
			error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText);
			}
		});
	}
}


function initModalNewDirectory() {
	removeMenu();
	document.getElementById("modalFormCreate").removeAttribute("hidden");
	document.getElementById("modalFormCreateTitle").innerText = document.getElementById("thisNewDirectory").innerHTML;
	document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.newdirectory");
	document.getElementById("modalFormCreateValidate").onclick = function() { thisNewDirectory() };
	document.getElementById("elementCreateValue").value = "";

}


$(document).ready(function() {
	$('#modalFormCreate').on('shown.bs.modal', function() {
		$('#elementCreateValue').trigger('focus');
	});
});

$("#elementCreateValue").on("keydown", function search(e) {
	if (e.keyCode == 13) {
		$('#modalFormCreateValidate').click();
	}
});

function thisNewDirectory() {
	var path = document.getElementById("thisNewDirectory").getAttribute("value");
	var directoryPromptName = document.getElementById("elementCreateValue").value;
	if (directoryPromptName == null || directoryPromptName == "") {
		showMessage("Directory need a name.");
	} else if (directoryPromptName.includes("/")) {
		showMessage("Forbidden character");
	} else {
		$.ajax({
			url: "/createDirectory",
			type: 'POST',
			dataType: "text",
			data: path + "/" + directoryPromptName,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				showMessage(directoryPromptName + " created")
				var ul = document.getElementById(path).getElementsByTagName('ul')[0];
				addToFilesList(path + "/" + directoryPromptName);
				addToDirsList(path + "/" + directoryPromptName)
				if (ul) { // div is developped
					var li = document.createElement('li');
					var subdiv = document.createElement('div');
					li.id = path + "/" + directoryPromptName;
					subdiv.onclick = function onclick(event) { developDirectory(this); };
					subdiv.setAttribute("isDir", "true");
					subdiv.innerHTML = '<img src="/images/directory.png" height="20" width="20" />	' + li.id.substring(li.id.lastIndexOf('/') + 1);
					li.appendChild(subdiv)
					ul.appendChild(li);
				}
			},
			error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText)
			}
		});
	}
}

function initModalRename() {
	removeMenu();
	document.getElementById("modalFormRename").removeAttribute("hidden");
	document.getElementById("elementRename").value = "";
}

$(document).ready(function() {
	$('#modalFormRename').on('shown.bs.modal', function() {
		$('#elementRename').trigger('focus');
	});
});

$("#elementRename").on("keydown", function search(e) {
	if (e.keyCode == 13) {
		$('#modalRenameButton').click();
	}
});


function thisRename() {
	var file = document.getElementById("thisRename").getAttribute("value");
	var filePromptName = document.getElementById("elementRename").value;
	$.ajax({
		url: "/rename",
		type: 'POST',
		dataType: "text",
		data: file + "\n" + filePromptName,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			var newId = file.substring(0, file.lastIndexOf("/") + 1) + filePromptName;
			showMessage(response);
			var tmp = document.getElementById(file);
			var isDirectory = tmp.getElementsByTagName('div')[0].getAttribute("isDir");
			changeFromFilesList(file, newId);
			tmp.id = newId;
			tmp.getElementsByTagName('div')[0].lastChild.textContent = filePromptName;
			var decendants = tmp.getElementsByTagName('*');
			for (i in decendants) {
				if (decendants[i].id)
					decendants[i].id = decendants[i].id.replace(file, newId);
			}
			if (document.getElementById("editorContent").getAttribute("file") == file) document.getElementById("editorContent").setAttribute("file", newId);
			if (isDirectory === "true") {
				changeFromDirsList(file, newId);
				tmp.getElementsByTagName("div")[0].setAttribute("isDir", "true");
				tmp.getElementsByTagName("div")[0].onclick = function onclick(event) { developDirectory(this); };
			} else {
				tmp.getElementsByTagName("div")[0].onclick = function onclick(event) { display(this); };
				tmp.getElementsByTagName("div")[0].setAttribute("isDir", "false");
			}
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});

}

function initModalDelete() {
	removeMenu();
	var file = document.getElementById("thisDelete").getAttribute("value");
	document.getElementById("modalFormDelete").removeAttribute("hidden");
	document.getElementById("modalFormDeleteTitle").innerText = getLocalizedText("prompt.delete").replace("%1", localCache.get("clickedFile"));
}

function thisDelete() {
	var file = document.getElementById("thisDelete").getAttribute("value");
	$.ajax({
		url: "/delete",
		type: 'POST',
		dataType: "text",
		data: file,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			showMessage(response);
			document.getElementById(file).parentElement.removeChild(document.getElementById(file));
			removeFromFilesList(file);
			if ($.trim(document.getElementById("editorContent").getAttribute("file")) == $.trim(file)) {
				resetEditors();
				document.getElementById("editorContent").setAttribute("file", "");
			}

		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
}


function initModalNewProject() {
	removeMenu();
	document.getElementById("modalFormCreate").removeAttribute("hidden");
	document.getElementById("modalFormCreateLabel").innerText = getLocalizedText("prompt.newproject");
	//	document.getElementById("modalFormCreateTitle").innerText = document.getElementById("NewProjectButton").innerText;
	document.getElementById("modalFormCreateTitle").innerText = getLocalizedText("menu.createProject");
	document.getElementById("modalFormCreateValidate").onclick = function() { thisNewProject() };
	document.getElementById("elementCreateValue").value = "";

}

function thisNewProject() {
	var directoryPromptName = document.getElementById("elementCreateValue").value;
	if (directoryPromptName == null || directoryPromptName == "") {
		showMessage("Project need a name.")
	} else if (directoryPromptName.includes("/")) {
		showMessage("Forbidden character");
	} else {
		$.ajax({
			url: "/createDirectory",
			type: 'POST',
			dataType: "text",
			data: directoryPromptName,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				showMessage(response)
				createNewProject(directoryPromptName);
			}, error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText)
			}
		});
	}
}

function createNewProject(name) {

	var ul = document.getElementById("fileTreeContainer").getElementsByTagName('ul')[0];
	addToDirsList("/" + name);
	var li = document.createElement('li');
	li.classList.add("filename");
	var subdiv = document.createElement('div');
	li.id = name;
	subdiv.onclick = function onclick(event) { developDirectory(this); };
	subdiv.setAttribute("isDir", "true");
	subdiv.innerHTML = '<img src="/images/project.png" height="20" width="20" />	' + li.id.substring(li.id);
	li.appendChild(subdiv)
	ul.appendChild(li);
	permissions.push(new Object());
	setCurrentProject(name);
	permissions[permissions.length - 1].username = currentUsername;
	permissions[permissions.length - 1].project = currentProject;
	permissions[permissions.length - 1].permission = "admin";
	//	reloadFilesAfterTest(name + "/paclif.opts");
	reloadGlobalFiles();
}

function getProperties() {
	document.getElementById("projectProperties").removeAttribute("hidden");
	var projectName = document.getElementById("thisNewDirectory").getAttribute("value");
	if (projectName.includes("/")) {
		projectName = projectName.substring(0, projectName.indexOf("/"));
	}
	setCurrentProject(projectName);
	if (getPermissionProject().includes("admin")) {
		document.getElementById("addCollaborator").removeAttribute("hidden");
	} else {
		document.getElementById("addCollaborator").setAttribute("hidden", "true");
	}
}

function getPermissionProject() {
	for (i in permissions) {
		if (permissions[i].project == currentProject) {
			return permissions[i].permission;
		}
	}
	return "";
}

function moveFile(file, dest) {
	console.log("Moving " + file + " to " + dest);
	$.ajax({
		url: "/movefile",
		type: 'POST',
		dataType: "text",
		data: file + "\n" + dest,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			reloadGlobalFiles();
			showMessage(response);

			document.getElementById(file).remove();
			if (document.getElementById(dest).getElementsByTagName("ul")[0])
				createNode(dest + file.substring(file.lastIndexOf("/")), document.getElementById(dest).getElementsByTagName("ul")[0]);

		}, error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
}


function thisSaveFile() {
	saveAceText();
}

function thisDownloadFile() {
	if (document.getElementById("editorContent").getAttribute("file") &&
		document.getElementById("editorContent").getAttribute("file") != "") {
		var editorToSave = "";
		if (getActiveEditor() == "scenarioYAML") {
			yamlToXml();
			editorToSave = ace.edit("scenarioXML");
		} else if (getActiveEditor() == "scenarioXML") {
			editorToSave = ace.edit("scenarioXML");
		} else if (getActiveEditor() == "testPlanYAML") {
			yamlToProperties();
			editorToSave = ace.edit("testPlanProperties");
		} else if (getActiveEditor() == "testPlanProperties") {
			editorToSave = ace.edit("testPlanProperties");
		} else {
			editorToSave = ace.edit("TextEditor");
		}
		var content = editorToSave.getSession().getValue();
		var file = document.getElementById("editorContent").getAttribute("file");
		file = getFileName(file);
		var element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
		element.setAttribute('download', file);

		element.style.display = 'none';
		document.body.appendChild(element);

		element.click();

		document.body.removeChild(element);
	}
}

function changeTargetedDir(value, filename) {
	document.getElementById("thisNewFile").setAttribute("value", value);
	document.getElementById("thisNewDirectory").setAttribute("value", value);
	document.getElementById("thisRename").setAttribute("value", filename);
	document.getElementById("thisDelete").setAttribute("value", filename);
	if (filename.includes("/")) {
		setCurrentProject(filename.substring(0, filename.indexOf("/")));
		document.getElementById("thisExport").classList.add("disabledMenu");
	} else {
		setCurrentProject(filename);
		enableMenu(document.getElementById("thisExport"));
	}
}

function showMessage(s) {
	$('<div id = message>' + s + '</div>').insertBefore('#editorContainers').delay(timeFading_ms).fadeOut();
}


function addToFilesList(file) {
	filesList.push(file);
}
function addToDirsList(dir) {
	directories.push(dir);
}

function changeFromFilesList(oldName, newName) {
	for (f in filesList) {
		if (filesList[f].includes(oldName)) {
			filesList[f] = filesList[f].replace(oldName, newName); //need to replace all subfile if a directory is removed
		}
	}
}

function changeFromDirsList(oldName, newName) {
	for (f in directories) {
		if (directories[f].includes(oldName)) {
			directories[f] = directories[f].replace(oldName, newName); //need to replace all subfile if a directory is removed
		}
	}
}


function removeFromFilesList(file) {

	for (f in filesList) {
		if (filesList[f].includes("/" + file + "/")) filesList.splice(f, 1); //need to remove all subfile if a directory is removed
		if (filesList[f].endsWith(file)) filesList.splice(f, 1);
	}
}

/*------------------------------------------------------------------	
----------------------CLIF Test execution---------------------------
-------------------------------------------------------------------*/


function changeButtonDeployingTest() {
	modalTestResetVal = document.getElementById("modalTestDeployed").innerHTML;
	var buttonDeployTest = document.getElementById("deployTestButton");
	buttonDeployTest.setAttribute("disabled", "true");
	buttonDeployTest.innerHTML = getLocalizedText("test.deploying") + '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"/>';
}

function changeButtonTestDeployed() {
	var buttonDeployTest = document.getElementById("deployTestButton");
	buttonDeployTest.removeAttribute("disabled");
	buttonDeployTest.innerHTML = getLocalizedText("test.access");
	buttonDeployTest.onclick = function() { showModalTestDeployed() };
	deployed = true;
}


function changeModalTestInit() {
	document.getElementById("modalFormInitTest").setAttribute("hidden", true);
	document.getElementById("modalTestStart").removeAttribute("disabled");
	$('#modalTestDeployedTitle').text(getLocalizedText("test.init.success"));

}
/**
This function check the deployed servers on the registry
 */
function checkRegistryServers() {
	let file = document.getElementById("editorContent").getAttribute("file");
	if (file.substring(file.lastIndexOf(".")) != ".ctp") {
		showMessage(getLocalizedText("test.error.ctp"));
	}
	else {
		document.getElementById("testModalButtonBar").setAttribute("value", file);
		$.ajax({
			url: "/checkServers",
			type: 'POST',
			dataType: "text",
			data: file,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				console.log(response);
				var parsedResponse = JSON.parse(response);
				clearModalCheckServer();
				completeModalCheckServer(parsedResponse);
				showModalCheckServer();
			},
			error: function(jqXhr, textStatus, errorThrown) {
				showMessage(jqXhr.responseText)
			}
		});
	}
}

/**
Complete the informations on the modal window modalCheckServer
By default, local host is known by the registry
 */
function completeModalCheckServer(parsedResponse) {
	var ulMissingServer = document.getElementById("modalCheckServerMissingUl");
	var ulExistServer = document.getElementById("modalCheckServerExistUl");
	for (var i = 0; i < parsedResponse["registryServers"].length; i++) {
		var li = document.createElement("LI");
		li.innerHTML = parsedResponse["registryServers"][i];
		ulExistServer.append(li);
	}
	for (var i = 0; i < parsedResponse["missingServers"].length; i++) {
		if (parsedResponse["missingServers"][i] != "local host") {
			var li = document.createElement("LI");
			li.innerHTML = parsedResponse["missingServers"][i];
			ulMissingServer.append(li);
		}
	}
	if (!parsedResponse["registryServers"].includes("local host")) {
		var li = document.createElement("LI");
		li.innerHTML = "local host";
		ulExistServer.append(li);
	}
	var legend = document.getElementById("checkServerLegend");
	legend.innerHTML = getLocalizedText("server.info").replace("%1", parsedResponse["registry"]);
}

/**
Clear the fields of the modal window modalTestCheckServer
 */
function clearModalCheckServer() {
	var ulMissingServer = document.getElementById("modalCheckServerMissingUl");
	var ulExistServer = document.getElementById("modalCheckServerExistUl");
	while (ulMissingServer.firstChild) {
		ulMissingServer.removeChild(ulMissingServer.firstChild);
	}
	while (ulExistServer.firstChild) {
		ulExistServer.removeChild(ulExistServer.firstChild);
	}
}
/**
Display the modal window modalTestCheckServer
 */
function showModalCheckServer() {
	$('#modalTestCheckServer').modal('show');
	$('#modalTestCheckServer').removeAttr('hidden');
}


function showModalTestDeployed() {
	$('#modalTestDeployed').modal('show');
	$('#modalTestDeployed').removeAttr('hidden');
}

function thisDeployTest() {
	console.log("deploying test");
	let file = document.getElementById("editorContent").getAttribute("file");
	if (file.substring(file.lastIndexOf(".")) != ".ctp") {
		showMessage(getLocalizedText("test.error.ctp"));
	}
	else {
		changeButtonDeployingTest();
		document.getElementById("testModalButtonBar").setAttribute("value", file);
		$.ajax({
			url: "/deployTest",
			type: 'POST',
			dataType: "text",
			data: file,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			complete: function(response) {
				if (response.readyState == 0
					|| (response.responseText && response.responseText.includes("error"))
					|| (response.responseText && response.responseText.includes("Error"))
					|| (response.responseText && response.responseText.includes("failed"))
					|| (response.responseText && response.responseText.includes("Failed"))) { // deploy failure

					console.log(response);
					$('#modalTestDeployedFailed').modal('show');
					$('#modalTestDeployedFailed').removeAttr('hidden');
					$('#modalTestDeployedFailedTitle').text(getLocalizedText("test.deploy.fail").replace("%1", getFileName(file)));
					$('#modalTestDeployedFailedContent').text(response.responseText);

					var buttonDeployTest = document.getElementById("deployTestButton");
					buttonDeployTest.removeAttribute("disabled");
					buttonDeployTest.innerHTML = getLocalizedText("test.deploy");
				} else { //deploy success 
					console.log("deploy success");
					deployedTest.push(file);
					showModalTestDeployed();
					changeButtonTestDeployed();
					document.getElementById("quickstatCheckLabel").innerHTML = getLocalizedText("test.quickstat");
					$('#modalTestDeployedTitle').text(getLocalizedText("test.deploy.success").replace("%1", getFileName(file)));
					document.getElementById("testIdInput").setAttribute("placeholder", getFileNameWithoutExtension(file));
				}
			}
		});
	}
}

function thisInitTest() {
	console.log("initializing test");
	let file = document.getElementById("editorContent").getAttribute("file");
	//	if (!deployedTest.includes(file)) {
	//		showMessage("Error : " + file + " is not deployed"); //TODO localize	
	//		return -1;
	//	}
	let id = document.getElementById("testIdInput").value;
	if (!id || $.trim(id).length == 0) {
		id = document.getElementById("testIdInput").getAttribute("placeholder");
	}
	document.getElementById("modalTestInit").innerHTML = getLocalizedText("test.init") + '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"/>';
	$.ajax({
		url: "/initTest",
		type: 'POST',
		dataType: "text",
		data: id,
		async: false,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			changeModalTestInit();
		},
		error: function(jqXhr, textStatus, errorThrown) {
			document.getElementById("modalTestInit").innerHTML = getLocalizedText("test.init");
			showMessage(jqXhr.responseText)
		}
	});
}

var testMetrics;

function thisStartTest() {
	console.log("starting test");
	document.getElementById("modalTestStart").setAttribute("disabled", true);
	document.getElementById("modalTestPause").removeAttribute("disabled");
	file = document.getElementById("testModalButtonBar").getAttribute("value");
	$.ajax({
		url: "/startTest",
		type: 'GET',
		async: true,
		success: function(response) {
			document.getElementById("modalTestDeployedTitle").innerHTML = getLocalizedText("modal.started")
			let modalTestTimerContainer = document.createElement("div");
			modalTestTimerContainer.innerHTML = getLocalizedText("metric.timeElapsed");
			let modalTestTimer = document.createElement("span");
			modalTestTimer.id = "modalTestTimer";
			modalTestTimerContainer.appendChild(modalTestTimer);
			modalTestTimerContainer.id = "modalTestTimerContainer";
			document.getElementById("testModalResult").appendChild(modalTestTimerContainer);


			let modalTestMetrics = document.createElement("div");
			modalTestMetrics.id = "modalTestMetrics";
			document.getElementById("testModalResult").appendChild(modalTestMetrics);

			createTableResultTest(getLocalizedText("metric.responsetime.mean"), "responsetime.mean");
			createTableResultTest(getLocalizedText("metric.responsetime.min"), "responsetime.min");
			createTableResultTest(getLocalizedText("metric.responsetime.max"), "responsetime.max");
			createTableResultTest(getLocalizedText("metric.requests.throughput"), "requests.throughput");
			createTableResultTest(getLocalizedText("metric.errors.rate"), "errors.rate");
			createTableResultTest(getLocalizedText("metric.errors.throughput"), "errors.throughput");
			createTableResultTest(getLocalizedText("metric.errors.number"), "errors.number");
			createTableResultTest(getLocalizedText("metric.requests.number"), "requests.number");

			testTime = 0;
			testMetrics = setInterval(displayMetrics, 1000);
		},
		error: function(jqXhr, textStatus, errorThrown) {
			document.getElementById("modalTestInit").innerHTML = getLocalizedText("test.init");
			showMessage(getLocalizedText("error.execution.starting"));
		}
	});
}
/**
 * This function creates a table that will contain the retrieved metrics
 * @param {String} localizedText - the fist column displayed text retrieved from the dictionary
 * @param {String} id - the second column id, used to assign the metrics
 */
function createTableResultTest(localizedText, id) {
	//create row
	var rowDiv = document.createElement("div");
	rowDiv.id = "rowDivLabel";
	rowDiv.classList.add("row");

	//first col
	var columnDiv1 = document.createElement("div");
	columnDiv1.classList.add("col");
	columnDiv1.style.textAlign = "right";
	var label = document.createElement("label");
	var textAsNode = document.createTextNode(localizedText);
	label.appendChild(textAsNode);
	columnDiv1.appendChild(label);
	rowDiv.appendChild(columnDiv1);

	//second col
	var columnDiv2 = document.createElement("div");
	columnDiv2.classList.add("col");
	columnDiv2.style.textAlign = "left";
	var span = document.createElement("span");
	span.id = id;
	columnDiv2.appendChild(span);
	rowDiv.appendChild(columnDiv2);

	//add to the parent
	var parent = document.getElementById("testModalResult");
	parent.appendChild(rowDiv);
}

function thisPauseTest() {
	console.log("pausing test");
	document.getElementById("modalTestStart").innerHTML = getLocalizedText("test.resume");
	document.getElementById("modalTestStart").onclick = function() { thisResumeTest() };
	document.getElementById("modalTestStart").removeAttribute("disabled");
	document.getElementById("modalTestPause").setAttribute("disabled", true);
	//	file = document.getElementById("testModalButtonBar").getAttribute("value");
	$.ajax({
		url: "/suspendTest",
		type: 'GET',
		success: function(response) {
			console.log("Pause Success");
			clearInterval(testMetrics);
			showMessage(response);
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(getLocalizedText("error.execution.suspending"));
			console.log("Pause failed");
		}
	});
}


function thisResumeTest() {
	console.log("resuming test");
	document.getElementById("modalTestStart").setAttribute("disabled", true);
	document.getElementById("modalTestPause").removeAttribute("disabled");
	//	file = document.getElementById("testModalButtonBar").getAttribute("value");
	$.ajax({
		url: "/resumeTest",
		type: 'GET',
		success: function(response) {
			testMetrics = setInterval(displayMetrics, 1000);
			console.log("Resume Success");
			showMessage(response);
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(getLocalizedText("error.execution.resuming"));
		}
	});
}

function thisStopTest() {
	console.log("stopping test");
	clearInterval(testMetrics);
	document.getElementById("modalTestStop").innerHTML = getLocalizedText("modal.stopping") + '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"/>';

	$.ajax({
		url: "/stopTest",
		type: 'GET',
		async: false,
		success: function(response) {
			CloseTest();
		},
		error: function(jqXhr, textStatus, errorThrown) {
			CloseTest();
		}
	});
}

function CloseTest() {

	document.getElementById("modalTestDeployed").setAttribute("hidden", true);
	$('#modalTestDeployed').modal('hide');
	document.getElementById("modalTestDeployed").innerHTML = modalTestResetVal;
	var buttonDeployTest = document.getElementById("deployTestButton");
	buttonDeployTest.innerHTML = getLocalizedText("test.deploy");
	buttonDeployTest.onclick = function() { checkRegistryServers() };
	deployed = false;
}

function thisCollectTest() {
	console.log("Collecting test results");
	file = document.getElementById("testModalButtonBar").getAttribute("value");
	let collectButton = document.getElementById("modalTestPause");
	collectButton.innerHTML = getLocalizedText("modal.collect") + '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"/>';
	var generateQuickstat = "false";
	if (document.getElementById("quickstatCheck").checked == true) {
		generateQuickstat = "generateQuickstat";
	}
	console.log("generateQuickstat");

	$.ajax({
		url: "/collectTest",
		type: 'POST',
		dataType: "text",
		data: generateQuickstat,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			showMessage(getLocalizedText("test.collect.success") + file.substring(0, file.lastIndexOf("/")));
			collectButton.innerHTML = getLocalizedText("modal.collect");
			collectButton.setAttribute("disabled", true);
			reloadFilesAfterTest(file, true);
			if (generateQuickstat == "generateQuickstat") {
				var filename = response.substring(0, response.indexOf("\n"));
				var fileContent = response.substring(response.indexOf("\n"));
				document.getElementById("modalCsvTitle").innerHTML = filename;
				displayCsvCollect(fileContent);
			}
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(getLocalizedText("error.execution.collecting"));
		}
	});
}

var metrics;

function thisGetMetrics() {
	let returnValue;
	$.ajax({
		url: "/getMetrics",
		type: 'GET',
		success: function(response) {
			if (response == null || $.trim(response) === "") { // test finished
				testFinished();
			} else {
				metrics = response;
			}
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
	return metrics;
}


function displayMetrics() {
	let metrics = thisGetMetrics();
	if (metrics == null) return null;
	testTime += 1;
	document.getElementById('modalTestTimer').textContent = testTime + " s";
	let metricsTable = metrics;
	let totalRequest = 0;
	let totalError = 0;
	for (let i = 0; i < metricsTable.length; i++) {
		totalRequest += Number(metricsTable[i][5]);
		totalError += Number(metricsTable[i][7]);
		metricsTable[i][5] = metricsTable[i][1] * metricsTable[i][6]; //calculating non cumulative number of request
		metricsTable[i][7] = metricsTable[i][1] * metricsTable[i][8]; //calculating non cumulative number of error
		metricsTable[i].splice(10, 1); // remmoving std
		metricsTable[i].splice(1, 1); // remmoving timeframe
	}
	let resultTable = new Array(8).fill(0);
	resultTable[1] = 999999999999999999;
	let weightedMeanResponseTime = 0;
	let weightedErrorRate = 0;
	for (let i = 0; i < metricsTable.length; i++) {
		//resultTable[0] = mean response time
		resultTable[1] = Math.min(resultTable[1], metricsTable[i][2]); //minimum response time
		resultTable[2] = Math.max(resultTable[2], metricsTable[i][3]); //maximum response time
		resultTable[3] = Number(resultTable[3]) + Number(metricsTable[i][5]) * Number(metricsTable[i][1]);//number of requests
		resultTable[4] = Number(resultTable[4]) + Number(metricsTable[i][5]); //request troughput
		resultTable[5] = Number(resultTable[5]) + Number(metricsTable[i][7]) * Number(metricsTable[i][1]);//number of error
		resultTable[6] = Number(resultTable[6]) + Number(metricsTable[i][7]); //error troughput
		weightedMeanResponseTime += Number(metricsTable[i][1]) * Number(metricsTable[i][5]);
		weightedErrorRate += Number(metricsTable[i][8]) * Number(metricsTable[i][7]);
		//resultTable[7] = //error rate
	}
	if (resultTable[4] != 0) {
		resultTable[0] = Math.floor(weightedMeanResponseTime / resultTable[4]);
	} else {
		resultTable[0] = 0;
	}
	if (resultTable[6] != 0) {
		resultTable[7] = Math.floor(weightedErrorRate / resultTable[6]);
	} else {
		resultTable[7] = 0;
	}

	document.getElementById("responsetime.mean").innerHTML = resultTable[0];
	document.getElementById("responsetime.min").innerHTML = resultTable[1];
	document.getElementById("responsetime.max").innerHTML = resultTable[2];
	document.getElementById("requests.throughput").innerHTML = resultTable[4] + " /s";
	document.getElementById("errors.throughput").innerHTML = resultTable[6] + " /s";
	document.getElementById("errors.rate").innerHTML = resultTable[7] + " %";
	document.getElementById("requests.number").innerHTML = totalRequest;
	document.getElementById("errors.number").innerHTML = totalError;
}

function testFinished() {

	clearInterval(testMetrics);
	$('#modalTestDeployedTitle').text(getLocalizedText("test.end.success").replace("%1", getFileName(file)));
	if (!document.getElementById("testCompleted")) {
		let div = document.createElement("div");
		div.id = "testCompleted"
		div.innerHTML = getLocalizedText("test.completed");
		document.getElementById("testModalResult").appendChild(div);
	}
	document.getElementById("modalTestStart").innerHTML = getLocalizedText("modal.restart");
	document.getElementById("modalTestStart").removeAttribute("onclick");
	document.getElementById("modalTestStart").onclick = function() { restartTest() };
	document.getElementById("modalTestStart").removeAttribute("disabled");
	document.getElementById("modalTestPause").innerHTML = getLocalizedText("modal.collect");
	document.getElementById("modalTestPause").removeAttribute("disabled");
	document.getElementById("modalTestPause").removeAttribute("onclick");
	document.getElementById("modalTestPause").onclick = function() { thisCollectTest() };
	document.getElementById("modalTestStop").innerHTML = getLocalizedText("modal.exit");


}

function restartTest() {
	let fileDeployed = document.getElementById("testModalButtonBar").getAttribute("value");
	document.getElementById("modalTestDeployed").innerHTML = modalTestResetVal;
	document.getElementById("testModalButtonBar").setAttribute("value", fileDeployed);
	$('#modalTestDeployedTitle').text(getLocalizedText("modal.restarting"));
	document.getElementById("testIdInput").setAttribute("placeholder", getFileNameWithoutExtension(file));
	document.getElementById("quickstatCheckLabel").innerHTML = getLocalizedText("test.quickstat");

}

function reloadGlobalFiles() {
	$.ajax({
		url: "/reloadFiles",
		type: 'GET',
		success: function(response) {
			filesList = response;
		}
	})
	$.ajax({
		url: "/reloadDirs",
		type: 'GET',
		success: function(response) {
			directories = response;
		}
	});
}
function reloadFilesAfterTest(file, report) {
	reloadGlobalFiles();
	let fileDir = file.substring(0, file.lastIndexOf("/"));
	if (document.getElementById(fileDir + "/report") == null && report) {
		if (document.getElementById(fileDir) == null) {
			createNewProject(fileDir);
		} else {
			ul = document.getElementById(fileDir).getElementsByTagName("ul")[0];
			if (ul != null) {
				console.log(ul);
				var li = document.createElement('li');
				var subdiv = document.createElement('div');
				li.id = fileDir + "/report";
				subdiv.setAttribute("isDir", "true");
				subdiv.onclick = function onclick(event) { developDirectory(this); };
				subdiv.innerHTML = '<img data-type="dir" src="/images/directory.png" height="20" width="20" /> report';
				ul.appendChild(li);
				li.appendChild(subdiv);
			}
		}
	} else {
		developDirectory(document.getElementById(fileDir + "/report").firstChild);
		developDirectory(document.getElementById(fileDir + "/report").firstChild);
	}
}

/*------------------------------------------------------------------	
--------------------------Right click menu--------------------------
-------------------------------------------------------------------*/
/**
This function creates the menu to manage project files.
Its content is adapted depending on where the user has clicked
@param {number} x - x position of the mouse
@param {number} y - y position of the mouse
@param {String} clickType - used to enable and disable some part of the context menu 
 */
function createMenu(x, y, clickType) {
	resetAceMenuPosition();
	menuPosition(x, y);
	menu.visibility = "visible";
	menu.opacity = "1";
	defaultMenu();
	var newFileMenu = document.getElementById("thisNewFileMenu");
	var newDirectoryMenu = document.getElementById("thisNewDirectoryMenu");
	var fileUploadMenu = document.getElementById("thisFileUploadMenu");
	var fileDeleteMenu = document.getElementById("thisDeleteMenu");
	var fileRenameMenu = document.getElementById("thisRenameMenu");
	var buttonPermission = document.getElementById("validateChangePermission");
	if (getPermissionProject().includes("admin")) {
		enableMenu(newFileMenu);
		enableMenu(newDirectoryMenu);
		enableMenu(fileUploadMenu);
		enableMenu(fileDeleteMenu);
		enableMenu(fileRenameMenu);
		buttonPermission.removeAttribute("hidden");
	} else if (getPermissionProject().includes("editor") &&
		!(document.getElementById("thisDelete").getAttribute("value").includes("/"))) { // Not a project
		enableMenu(newFileMenu);
		enableMenu(newDirectoryMenu);
		enableMenu(fileUploadMenu);
		enableMenu(fileDeleteMenu);
		enableMenu(fileRenameMenu);
		buttonPermission.setAttribute("hidden", "true");
	} else if (getPermissionProject().includes("editor")) {
		enableMenu(newFileMenu);
		enableMenu(newDirectoryMenu);
		enableMenu(fileUploadMenu);
		buttonPermission.setAttribute("hidden", "true");
	} else {
		disableMenu(newFileMenu);
		disableMenu(newDirectoryMenu);
		disableMenu(fileUploadMenu);
		disableMenu(fileDeleteMenu);
		disableMenu(fileRenameMenu);
		buttonPermission.setAttribute("hidden", "true");
	}
	if (clickType == "dir") {
		rightClickDir();
	}
	else if (clickType == "file") {
		rightClickFile();
	}
	else if (clickType == "xisFile") {
		rightClickXisFile();
	}
	else if (clickType == "other") {
		rightClickOther();
	}
}

/**
This function determines where the file management menu will be diisplayed
@param {number} x - x position of the mouse
@param {number} y - y position of the mouse
 */
function menuPosition(x, y) {
	var leftPanelHeight = $('#leftPanel').height();
	var menuHeight = $('#menu').height();

	var menuWidth = $('#menu').width();
	var childWidth = $('#dropdownFile').width();
	var editorWidth = $('#editorContent').width();
	var handlerWidth = $('#handler').width();
	var leftPanelWidth = $('#leftPanel').width();

	if (x + menuWidth > editorWidth + handlerWidth + leftPanelWidth) {
		menu.left = (editorWidth + handlerWidth + leftPanelWidth - menuWidth) + "px";
		document.getElementById("dropdownFile").style.right = '100%';
	}
	else {
		if (x + childWidth + menuWidth > editorWidth + handlerWidth + leftPanelWidth) {
			document.getElementById("dropdownFile").style.right = '100%';
		}
		menu.left = x + "px";
	}

	if (y + menuHeight > 51 + leftPanelHeight) {
		menu.top = (leftPanelHeight + 55 - menuHeight - 20) + "px";
	}
	else {
		menu.top = y + "px";
	}
}

/**
	This function handles the creation of the contextual menu for the ace editor.
	It adapts its position so that the menu doesn't appear outside the screen.
 */
function createContextMenuAceEditor(x, y) {

	contextMenuAceEditor.visibility = "visible";
	contextMenuAceEditor.opacity = "1";
	resetAceContextMenu();
	//get window size
	var editorWidth = $('#editorContent').width();
	var handlerWidth = $('#handler').width();
	var leftPanelWidth = $('#leftPanel').width();

	var editorHeight = $('#editorContent').height();
	var switchEditorHeight = $("#switchEditorBar").height();
	//menu size
	var menuWidth = $('#contextMenuAceEditor').width();
	var menuHeight = $('#contextMenuAceEditor').height();
	var childWidth = $('#newChild').width();
	var childHeight = $('#newChild').height();

	//context menu 
	if (x > editorWidth + handlerWidth + leftPanelWidth - menuWidth) {
		contextMenuAceEditor.left = (editorWidth + handlerWidth + leftPanelWidth - menuWidth) + "px";
		var allChild = document.getElementsByClassName('child');
		for (var i = 0; i < allChild.length; i++) {
			allChild[i].style.left = '-105%';
		}
	}
	else {
		if (x + childWidth > editorWidth + handlerWidth + leftPanelWidth - menuWidth) {
			var allChild = document.getElementsByClassName('child');
			for (var i = 0; i < allChild.length; i++) {
				allChild[i].style.left = '-105%';
			}
		}
		else if (x + childWidth * 2 > editorWidth + handlerWidth + leftPanelWidth - menuWidth) {
			var statementChild = document.getElementById("statementChild");
			statementChild.style.left = '-105%';
		}
		contextMenuAceEditor.left = x + "px";
	}

	if (y + menuHeight > editorHeight + switchEditorHeight + 55) {
		contextMenuAceEditor.top = (editorHeight + switchEditorHeight + 55 - menuHeight - 20) + "px";
		var statementChild = document.getElementById("newChild");
		statementChild.style.top = '-350%';
	}
	else {
		if (y + childHeight > editorHeight + switchEditorHeight + 55) {
			var statementChild = document.getElementById("newChild");
			statementChild.style.top = '-350%';
		}
		contextMenuAceEditor.top = y + "px";
	}
}

function resetAceContextMenu() {
	var allChild = document.getElementsByClassName('child');
	for (var i = 0; i < allChild.length; i++) {
		allChild[i].style.left = '100%';
	}
	var statementChild = document.getElementById("newChild");
	statementChild.style.top = '0%';
}

function resetAceMenuPosition() {
	var dropdownContent = document.getElementById("dropdownFile");
	dropdownContent.style.right = '-100%';
}

function enableMenu(element) {
	element.classList.remove("disabledMenu");
}

function disableMenu(element) {
	if (secutriyEnabled) {
		element.classList.add("disabledMenu");
	}
}
/**
This function is used to disable a item of a context menu without taking the security into account
 */
function disableMenuWithoutSecurity(element) {
	element.classList.add("disabledMenu");
}

function removeMenu() {
	menu.opacity = "0";
	menu.visibility = "hidden";
}


/**
This function activates the default menus of the file tree context menu
 */
function defaultMenu() {
	//dropdown menu
	enableMenu(document.getElementById("dropdownImport"));
	enableMenu(document.getElementById("thisImportSubmenu"));
	enableMenu(document.getElementById("thisFileUploadMenu"));
	enableMenu(document.getElementById("thisImportProjectMenu"));

	//new menu
	enableMenu(document.getElementById("dropdownNew"));
	enableMenu(document.getElementById("thisNewFileSubmenuButton"));
	enableMenu(document.getElementById("thisNewFileMenu"));
	enableMenu(document.getElementById("thisNewCtpSubmenu"));
	enableMenu(document.getElementById("thisNewXisSubmenu"));
	enableMenu(document.getElementById("thisNewCtpFromXisSubmenu"));
	enableMenu(document.getElementById("thisNewDirectoryMenu"));
	enableMenu(document.getElementById("thisNewProjectMenu"));

	//other
	enableMenu(document.getElementById("thisDeleteMenu"));
	enableMenu(document.getElementById("thisRenameMenu"));
	enableMenu(document.getElementById("thisDuplicateMenu"));

}



/**
This function is called when the user has clicked on a directory.
It desactivates the generation of ctp file and the duplicate option
It activates the importation of files
 */
function rightClickDir() {
	//new menu
	disableMenuWithoutSecurity(document.getElementById("thisNewCtpFromXisSubmenu"));
	//general menu
	disableMenuWithoutSecurity(document.getElementById("thisDuplicateMenu"));
}

/**
This function is called when the user has clicked on a file.
It desactivates the file creation and the upload option
 */
function rightClickFile() {
	//new menu
	disableMenuWithoutSecurity(document.getElementById("thisNewFileMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewCtpSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewXisSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewCtpFromXisSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewDirectoryMenu"));
	//import menu
	disableMenuWithoutSecurity(document.getElementById("thisFileUploadMenu"));
}

/**
This function is called when the user has clicked on an xis file.
It desactivates the file creation, except the creation of a ctp file from an xis file, and the upload option
 */
function rightClickXisFile() {
	//new menu
	disableMenuWithoutSecurity(document.getElementById("thisNewFileMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewCtpSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewXisSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewDirectoryMenu"));

	//import menu
	disableMenuWithoutSecurity(document.getElementById("thisFileUploadMenu"));

}

/**
This function is called when the user has clicked out off the file tree.
It only enables to create or import a project
 */
function rightClickOther() {
	//dropdown menu
	enableMenu(document.getElementById("thisImportSubmenu"));
	enableMenu(document.getElementById("dropdownImport"));
	enableMenu(document.getElementById("thisImportProjectMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisFileUploadMenu"));

	//new menu
	enableMenu(document.getElementById("dropdownNew"));
	enableMenu(document.getElementById("thisNewFileSubmenuButton"));
	disableMenuWithoutSecurity(document.getElementById("thisNewFileMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewCtpSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewXisSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewCtpFromXisSubmenu"));
	disableMenuWithoutSecurity(document.getElementById("thisNewDirectoryMenu"));
	enableMenu(document.getElementById("thisNewProjectMenu"));

	//other
	disableMenuWithoutSecurity(document.getElementById("thisDeleteMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisRenameMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisExportMenu"));
	disableMenuWithoutSecurity(document.getElementById("thisDuplicateMenu"));


}
/*------------------------------------------------------------------	
--------------------Managing Project Rights-------------------------
-------------------------------------------------------------------*/
currentProject = "";
function setCurrentProject(project) {
	if (project != currentProject) {
		currentProject = project;
		document.getElementById("currentProject").textContent = project;
		displayCollaborators();
	}
}

function thisaddCollaborator() {
	coolaboratorName = document.getElementById("collaboratorNameInput").value;
	colaboratorPermission = document.getElementById("collaboratorPermissionInput").value;
	if (coolaboratorName == null || coolaboratorName == "") {
		txt = "coolaborator need a name.";
	} else {
		$.ajax({
			url: "/addCollaborator",
			type: 'POST',
			dataType: "text",
			data: currentProject + "/" + colaboratorPermission + "/" + coolaboratorName,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				showMessage(response);
				displayCollaborators();
			},
			error: function(jqXhr, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}

}

function displayCollaborators() {
	$.ajax({
		url: "/getCollaborators",
		type: 'POST',
		dataType: "text",
		data: currentProject,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			collabs = JSON.parse(response.replace(/\\/g, "").replace(/"{/g, "{").replace(/}"/g, "}"));
			projectPermission = collabs;
			collabsTable = document.getElementById("tableBody");
			collabsTable.innerHTML = "";
			if (!getPermissionProject().includes("admin")) {
				collabs.forEach(user => {
					collabsTable.innerHTML += "<tr><td>" + user.username + "</td><td>" + user.permission + "</td></tr>"
				});
				document.getElementById("changePermissions").setAttribute("hidden", "true");
			} else {
				collabs.forEach(user => {
					var stopFromRemovingSelf = (user.username == currentUsername) ? 'disabled' : "";
					var tester = (user.permission.includes("tester")) ? 'selected="selected"' : "";
					var editor = (user.permission.includes("editor")) ? 'selected="selected"' : "";
					var viewer = (user.permission.includes("viewer")) ? 'selected="selected"' : "";
					var admin = (user.permission.includes("admin")) ? 'selected="selected"' : "";
					collabsTable.innerHTML +=
						'<tr><td> ' + user.username + '</td><td><select class="form-control changePermissionUser" ' + stopFromRemovingSelf + '>' +
						'<option value="viewer"' + viewer + '>' + getLocalizedText("permission.viewer") + '</option>' +
						'<option value="tester"' + tester + '>' + getLocalizedText("permission.tester") + '</option>' +
						'<option value="editor"' + editor + '>' + getLocalizedText("permission.editor") + '</option>' +
						'<option value="admin"' + admin + '>' + getLocalizedText("permission.admin") + '</option>' +
						'</select></td><td>	<img alt="remove user" src="/images/remove.png" height="20" width="20" onclick="thisRemoveUser(this.parentNode.parentNode)" />	</td></tr>';
				});
				document.getElementById("changePermissions").removeAttribute("hidden");
			}
		}
	});

}

function changePermissions() {
	var collaboratorTable = document.getElementById("collaboratorsTable");
	for (r in collaboratorTable.rows) {
		var listUser = [];
		var listPermission = [];
		if (r > 0) {
			username = collaboratorTable.rows[r].childNodes[0].innerText;
			userPermission = collaboratorTable.rows[r].childNodes[1].firstElementChild.value;
			if (projectPermission[r - 1].username != username || projectPermission[r - 1].permission != userPermission) {
				thisChangePermission(username, currentProject, userPermission);
			}
		}
	}
}

function thisChangePermission(username, currentProject, userPermission) {
	$.ajax({
		url: "/editCollaborator",
		type: 'POST',
		dataType: "text",
		data: currentProject + "/" + userPermission + "/" + username,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			showMessage(response);
			displayCollaborators();
		},
		error: function(jqXhr, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
}

function thisRemoveUser(parentNode) {
	username = parentNode.firstElementChild.innerText;
	$.ajax({
		url: "/removeCollaborator",
		type: 'POST',
		dataType: "text",
		data: currentProject + "/" + username,
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			showMessage(response);
			displayCollaborators();
		},
		error: function(jqXhr, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
}


/*------------------------------------------------------------------	
------------------------------Drag and drop-------------------------
-------------------------------------------------------------------*/
let draggedFile;

function drag(event) {
	draggedFile = event.target;
}

function drop(event) {
	if (!draggedFile) return null;
	event.preventDefault();
	let target = event.target;
	if (target.localName == "img")
		target = target.parentElement;
	if ((target.attributes[0].nodeName == "isdir" && target.attributes[0].nodeValue == "true")
		|| (target.attributes[1].nodeName == "isdir" && target.attributes[1].nodeValue == "true")
	) {
		moveFile(draggedFile.id, target.parentElement.id);
		draggedFile = null;
	}
}

function ondragover(event) {
	event.preventDefault();
}

function allowDrop(event) {
	event.preventDefault();
}
/*------------------------------------------------------------------	
------------------------------Miscellaneous-------------------------
-------------------------------------------------------------------*/

function getFileName(path) {
	return path.substring(path.lastIndexOf("/") + 1);
}

function getFileNameWithoutExtension(path) {
	return path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
}

function getFileProject(path) {
	return path.substring(0, path.indexOf("/"));
}

