/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 *Create an event listener to catch when the user's mouse is on the folding arrow
 */
if (document.addEventListener) {
	document.addEventListener('mouseover', function(event) {
		if (isDescendant(document.getElementById("scenarioXML"), event.target)) {
			var editor = ace.edit(getActiveEditor());
			var closedFolds = document.getElementsByClassName("ace_closed");
			var target = event.target;
			var booleanClosedFold = false;
			var targetedFolds;

			for (var i = 0; i < closedFolds.length && booleanClosedFold == false; i++) {
				if (closedFolds[i] == target) {
					//obtain the target line number
					var targetInnerHtml = target.parentNode.innerHTML;
					var temp = targetInnerHtml.split("<");	//temp is formed by a line number followed by an xml tag
					var targetFirstLine = parseInt(temp[0], 10);
					targetedFolds = closedFolds[i];

					//obtain the parents line numbers
					var completeText = editor.getValue();
					var arraySplitText = completeText.split("\n");

					var endLine = retrieveLineNumbers(target, targetFirstLine, arraySplitText.length);

					//retrieve the folded text
					var retrievedText = "";
					for (var j = targetFirstLine - 1; j < endLine; j++) {
						retrievedText += arraySplitText[j] + "\n";
					}


					var popupTxt = document.getElementById("popupFoldingText");
					var popupBox = document.getElementById("popupFolding");

					var resumedText = switchRetrievedText(retrievedText);

					var comment = handleComment(retrievedText);
					
					resumedText = comment + resumedText;

					//display the popup
					if (resumedText != null && resumedText != undefined && resumedText != "") {
						popupFoldingStyle(popupTxt, popupBox, resumedText, target);
						popupTxt.classList.toggle("show");
					}
					booleanClosedFold = true;
				}
			}
			//close the popup
			if (target != targetedFolds && target != document.getElementById("popupFoldingText") && target != document.getElementById("popupFolding")) {
				var popupTxt = document.getElementById("popupFoldingText");
				popupTxt.classList.toggle("show", false);
			}
		}
	});
	//close the popup when the user clicks on the closing arrow
	document.addEventListener('click', function(event) {
		if (isDescendant(document.getElementById("scenarioXML"), event.target)) {
			var target = event.target;
			var targetClassList = target.classList;
			if (targetClassList.contains("ace_closed")) {
				var popupTxt = document.getElementById("popupFoldingText");
				popupTxt.classList.toggle("show", false);
			}
		}
	}, true);
}

/**
 * This function determines the last line of a folded text portion on the ace editor
 * @param {Object} target - the object on which the event occurs
 * @param {int} targetFirstLine - the first line of the folded text
 * @param {int} textLineNumber - the number of line on that the file contains
 * @return {int} The last line that is folded
 */
function retrieveLineNumbers(target, targetFirstLine, textLineNumber) {
	//the usual class of the line numbers is "ace_gutter-cell". Whene a line is selected, 
	//it is changed to ace_gutter-cell.ace_gutter-active-line
	var targetParentClassName = target.parentNode.className;
	if (targetParentClassName.includes(" ")) {
		var targetParentClassArray = targetParentClassName.split(" ");
		for (var i = 0; i < targetParentClassArray.length; i++) {
			if (targetParentClassArray[i].includes("cell")) {
				targetParentClassName = targetParentClassArray[i];
			}
		}
	}
	var xmlEditor = document.getElementById("scenarioXML");
	var parentsArray = xmlEditor.getElementsByClassName(targetParentClassName);

	var parentsLineNumber = []
	for (var j = 0; j < parentsArray.length; j++) {
		//the obtained innerHtml has the following format : number<span ...>
		//only the  line number matters for us
		var temp = parentsArray[j].innerHTML.split("<");
		parentsLineNumber.push(parseInt(temp[0], 10));
	}

	//obtain the final line to retrieve
	//Exemple: If the text id folded between line 5 and 10, the elements of the parent class include lines 5 and 11
	var counter = 0;
	while (parentsLineNumber[counter] <= targetFirstLine) {
		counter++;
	}
	var endLine = parentsLineNumber[counter] - 1;
	if (isNaN(endLine)) {
		endLine = textLineNumber;
	}
	return endLine;
}

/**
 * This function handles the style of the displayed popup. It ajusts the popup heigth and adds the text to the popup
 * @param {HTML Object} popupTxt - a reference to the popup div that will contain the resumed text
 * @param {HTML Object} popupBox - a reference to the popup
 * @param {String} resumedText - The text to add to the popup
 * @param {Object} target - the object on which the event occurs. Used to know where to display the popup
 */
function popupFoldingStyle(popupTxt, popupBox, resumedText, target) {
	//determine the textarea size

	var maxLength = 0;
	var arrayResumedText = resumedText.split("\n");

	if (arrayResumedText.length > 10) {
		popupTxt.rows = 10;
	}
	else {
		popupTxt.rows = arrayResumedText.length;
	}
	for (var i = 0; i < arrayResumedText.length; i++) {
		if (arrayResumedText[i].length > maxLength) {
			maxLength = arrayResumedText[i].length;
		}
	}
	if (maxLength > 78) {
		popupTxt.cols = 80;
	}
	else {
		popupTxt.cols = maxLength + 2;
	}

	//popup position
	var targetPosition = target.getBoundingClientRect();
	popupBox.style.left = targetPosition.right + "px";
	popupBox.style.top = targetPosition.top - targetPosition.height - 51 + "px";

	//popup Text
	popupTxt.value = resumedText;
}

/**
 * This function is used to retrieve comments inside the text that we have to summarize
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The comments that must be kept 
 */
function handleComment(retrievedText) {
	var arrayRetrievedText = retrievedText.split("<");
	var comment = "";
	arrayRetrievedText.shift();
	for (var i = 0; i < arrayRetrievedText.length; i++) {
		arrayRetrievedText[i] = "<" + arrayRetrievedText[i];
	}
	//We fix the indentation level at which the comments can be read
	var validIndentLevel = 0;
	var currentIndentLevel = 0;
	if (arrayRetrievedText[0].includes("<scenario>") ||
		arrayRetrievedText[0].includes("<plugins") ||
		arrayRetrievedText[0].includes("<behaviors") ||
		arrayRetrievedText[0].includes("<if") ||
		arrayRetrievedText[0].includes("<while") ||
		arrayRetrievedText[0].includes("<nchoice") ||
		arrayRetrievedText[0].includes("<preemptive") ||
		arrayRetrievedText[0].includes("<control") ||
		arrayRetrievedText[0].includes("<sample") ||
		arrayRetrievedText[0].includes("<timer") ||
		arrayRetrievedText[0].includes("<condition") ||
		arrayRetrievedText[0].includes("<params") ||
		arrayRetrievedText[0].includes("<loadprofile")) {
		validIndentLevel++;
	}
	
	for (var j = 0; j < arrayRetrievedText.length; j++) {
		if (arrayRetrievedText[j][1] == "!") {
			if (currentIndentLevel == validIndentLevel){
				var indexChevron =  arrayRetrievedText[j].lastIndexOf(">");
				
				comment += arrayRetrievedText[j].substring(4, indexChevron - 2) + "\n";
			}
		}
		else if (arrayRetrievedText[j].includes("</")){
			currentIndentLevel--;
		}
		else if (arrayRetrievedText[j].includes("<")) {
			currentIndentLevel++;
		}
	}
	return comment;
}

/**
 * This function allows to know which function will be called to create the text summarizing the folded xml code 
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function switchRetrievedText(retrievedText) {
	var retrievedTextArray = retrievedText.split(">");
	var resumedText = "";
	for (var i = 0; i < retrievedTextArray.length - 1; i++) {
		retrievedTextArray[i] += ">";
	}

	if (retrievedTextArray[0].includes("<scenario")) {
		resumedText = foldedScenario(retrievedText);
	}
	else if (retrievedTextArray[0].includes("<behaviors")) {
		resumedText = foldedBehaviors(retrievedText);
	}
	if (retrievedTextArray[0].includes("<plugins")) {
		resumedText = foldedPlugins(retrievedText);
	}
	else if (retrievedTextArray[0].includes("<loadprofile")) {
		resumedText = foldedLoadprofiles(retrievedText);
	}
	else if (retrievedTextArray[0].includes("<control")) {
		resumedText = foldedPrimitive(retrievedText, "Control");
	}
	else if (retrievedTextArray[0].includes("<sample")) {
		resumedText = foldedPrimitive(retrievedText, "Sample");
	}
	else if (retrievedTextArray[0].includes("<timer")) {
		resumedText = foldedPrimitive(retrievedText, "Timer");
	}
	else if (retrievedTextArray[0].includes("<condition")) {
		resumedText = foldedPrimitive(retrievedText, "Condition");
	}
	else if (retrievedTextArray[0].includes("<params")) {
		resumedText = foldedParams(retrievedText);
	}
	else if (retrievedTextArray[0].includes("<nchoice")) {
		resumedText = foldedNchoice(retrievedText);
	}
	else if (retrievedTextArray[0].includes("<if")) {
		resumedText = foldedStatement(retrievedText, "If");
	}
	else if (retrievedTextArray[0].includes("<while")) {
		resumedText = foldedStatement(retrievedText, "While");
	}
	else if (retrievedTextArray[0].includes("<preemptive")) {
		resumedText = foldedStatement(retrievedText, "Preemptive");
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first xml tag is <scenario>
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedScenario(retrievedText) {
	var textAsObject = stringToObj(retrievedText);
	var textPlugins = textAsObject["behaviors"]["plugins"]["use"];
	var textBehavior = textAsObject["behaviors"]["behavior"];

	var resumedText = getLocalizedText("folding.scenario.behaviors");
	if (textBehavior.length > 1) {
		for (var j = 0; j < textBehavior.length; j++) {
			resumedText += "\t" + textBehavior[j]["id"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textBehavior["id"] + "\n";
	}

	resumedText += getLocalizedText("folding.scenario.plugins");
	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["name"] + "\n";
	}

	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first xml tag is <Behaviors>
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedBehaviors(retrievedText) {
	var textAsObject = stringToObj(retrievedText);
	var textPlugins = textAsObject["plugins"]["use"];
	var textBehavior = textAsObject["behavior"];

	var resumedText = getLocalizedText("folding.scenario.behaviors");
	if (textBehavior.length > 1) {
		for (var j = 0; j < textBehavior.length; j++) {
			resumedText += "\t" + textBehavior[j]["id"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textBehavior["id"] + "\n";
	}

	resumedText += getLocalizedText("folding.scenario.plugins");
	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["name"] + "\n";
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first xml tag is <plugins>
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedPlugins(retrievedText) {
	var textAsObject = stringToObj(retrievedText);
	var textPlugins = textAsObject["use"];

	var resumedText = getLocalizedText("folding.plugins");

	if (textPlugins.length > 1) {
		for (var k = 0; k < textPlugins.length; k++) {
			resumedText += "\t" + textPlugins[k]["id"] + " : " + textPlugins[k]["name"] + "\n";
		}
	}
	else {
		resumedText += "\t" + textPlugins["id"] + " : " + textPlugins["name"] + "\n";
	}
	return resumedText;
}

/**
 * This function summarizes the retrieved text if the first xml tag is a primitive tag
 * @param {String} retrievedText - The text that will be summarized
 * @param {String} primitive - The type of primitive (control, sample, timer)
 * @return {String} The summarized text
 */
function foldedPrimitive(retrievedText, primitive) {
	var textAsObject = stringToObj(retrievedText);
	var resumedText = primitive + " " + textAsObject["use"] + "." +textAsObject["name"] + "\n";
	if (textAsObject.hasOwnProperty("params")) {
		var textControl = textAsObject["params"];

		if (textControl["param"].length > 1) {
			for (var k = 0; k < textControl["param"].length; k++) {
				resumedText += "\t" + textControl["param"][k]["name"] + " : " + textControl["param"][k]["value"] + "\n";
			}
		}
		else {
			resumedText += "\t" + textControl["param"]["name"] + " : " + textControl["param"]["value"] + "\n";
		}
		return resumedText;
	}
}

/**
 * This function summarizes the retrieved text if the first xml tag is <params>
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedParams(retrievedText) {
	var textAsObject = stringToObj(retrievedText);
	if (textAsObject.hasOwnProperty("param")) {

		var resumedText = "Params" + "\n" + getLocalizedText("folding.control");

		if (textAsObject["param"].length > 1) {
			for (var k = 0; k < textAsObject["param"].length; k++) {
				resumedText += "\t" + textAsObject["param"][k]["name"] + " : " + textAsObject["param"][k]["value"] + "\n";
			}
		}
		else {
			resumedText += "\t" + textAsObject["param"]["name"] + " : " + textAsObject["param"]["value"] + "\n";
		}
		return resumedText;
	}
}

/**
 * This function summarizes the retrieved text if the first xml tag is <nchoice>
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedNchoice(retrievedText) {
	var textAsObject = stringToObj(retrievedText);
	if (textAsObject.hasOwnProperty("choice")) {
		var numberOfChoices = textAsObject["choice"].length;
		var totalWeight = 0;
		if (numberOfChoices > 1) {
			for (var i = 0; i < numberOfChoices; i++) {
				totalWeight += parseInt(textAsObject["choice"][i]["proba"], 10);
			}
		}
		else {
			totalWeight += parseInt(textAsObject["choice"]["proba"], 10);
		}
		var resumedText = getLocalizedText("folding.nchoice").replace("%1", numberOfChoices).replace("%2", totalWeight);

		return resumedText;
	}
}

/**
 * This function summarizes the retrieved text if the first xml tag is a statement
 * @param {String} retrievedText - The text that will be summarized
 * @param {String} statement - The type of statement (if, while, preemptive)
 * @return {String} The summarized text
 */
function foldedStatement(retrievedText, statement) {
	var textAsObject = stringToObj(retrievedText);
	if (textAsObject.hasOwnProperty("condition")) {
		var resumedText = statement + " " + textAsObject["condition"]["use"] + "." + textAsObject["condition"]["name"] + "\n";
		if (textAsObject["condition"].hasOwnProperty("params")) {
			var param = textAsObject["condition"]["params"]["param"];
			if (param.length > 1) {
				for (var k = 0; k < param.length; k++) {
					resumedText += "\t" + param[k]["name"] + " : " + param[k]["value"] + "\n";
				}
			}
			else {
				resumedText += "\t" + param["name"] + " : " + param["value"] + "\n";
			}
		}

		return resumedText;
	}
}

/**
 * This function summarizes the retrieved text if the first xml tag is <loadprofile>
 * @param {String} retrievedText - The text that will be summarized
 * @return {String} The summarized text
 */
function foldedLoadprofiles(retrievedText) {
	var textAsObject = stringToObj(retrievedText);
	var resumedText = getLocalizedText("folding.loadprofile");

	if (textAsObject.hasOwnProperty("group")) {
		if (textAsObject["group"].length > 1) {
			for (var i = 0; i < textAsObject["group"].length; i++) {
				resumedText += "\t" + textAsObject["group"][i]["behavior"];
			}
		}
		else {
			resumedText += "\t" + textAsObject["group"]["behavior"] + "\n";
		}
	}

	return resumedText;
}