/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */


/** 
 * This function removes the properties menu
 */
function removePropertiesContextMenu() {
	contextMenuPropertiesEditor.opacity = "0";
	contextMenuPropertiesEditor.visibility = "hidden";
}

/**
	This function handles the creation of the contextual menu for the ace editor.
	It adapts its position so that the menu doesn't appear outside the screen.
 */
function createPropertiesMenu(x, y) {
	contextMenuPropertiesEditor.visibility = "visible";
	contextMenuPropertiesEditor.opacity = "1";
	resetPropertiesMenu();
	//get window size
	var editorWidth = $('#editorContent').width();
	var handlerWidth = $('#handler').width();
	var leftPanelWidth = $('#leftPanel').width();

	var childHeight = $('#probePropertiesChild').height();
	var editorHeight = $('#editorContent').height();
	var switchEditorHeight = $("#switchEditorBar").height();
	//menu size
	var menuWidth = $('#contextMenuPropertiesEditor').width();
	var menuHeight = $('#contextMenuPropertiesEditor').height();

	//context menu 
	if (x > editorWidth + handlerWidth + leftPanelWidth - menuWidth) {
		contextMenuPropertiesEditor.left = (editorWidth + handlerWidth + leftPanelWidth - menuWidth) + "px";
		var allChild = document.getElementsByClassName('child');
		for (var i = 0; i < allChild.length; i++) {
			allChild[i].style.left = '-105%';
		}
	}
	else {
		contextMenuPropertiesEditor.left = x + "px";
	}

	if (y + menuHeight > editorHeight + switchEditorHeight + 55) {
		contextMenuPropertiesEditor.top = (editorHeight + switchEditorHeight + 55 - menuHeight - 20) + "px";
		var probeChild = document.getElementById("probePropertiesChild");
		probeChild.style.top = '-300%';
	}
	else {
		if (y + childHeight > editorHeight + switchEditorHeight + 55) {
			var probeChild = document.getElementById("probePropertiesChild");
			probeChild.style.top = '-300%';
		}
		contextMenuPropertiesEditor.top = y + "px";
	}
}

/**
 * This function is called each time the properties context menu is displayed
 * It resets the position of the properties context menu
 */
function resetPropertiesMenu() {
	var allChild = document.getElementsByClassName('child');
	for (var i = 0; i < allChild.length; i++) {
		allChild[i].style.left = '100%';
	}
	var statementChild = document.getElementById("probePropertiesChild");
	statementChild.style.top = '0%';
}

/**
 * This function adds the probes to the properties context menu.
 * It is executed when the window is refreshed
 * @param {JSON} probeInfoCache - a JSON String that contains the data about the probes
 */
function completePropertiesContextMenu(probeInfoCache) {
	var ul = document.getElementById("probePropertiesChild");
	for (let i = 0; i < probeInfoCache["probes"].length; i++) {
		let probeName = probeInfoCache["probes"][i]["name"];
		let probePath = probeInfoCache["probes"][i]["path"];
		let probeHelp = probeInfoCache["probes"][i]["help"];
		var li = document.createElement('li');
		li.classList.add("parent");

		var aTag = document.createElement("a");
		aTag.classList.add("contextualMenu");
		aTag.onclick = function() { handleInsertProbe(probeName, probePath, probeHelp) };

		var div = document.createElement("div");
		div.innerHTML = probeName;

		aTag.appendChild(div);
		li.appendChild(aTag);
		ul.appendChild(li);
	}
}

/**
 * This function prepares the probes-related text that will be inserted in the properties editor
 * @param {String} probeName - the name of the probe
 * @param {String} probePath - the class path of the probe
 * @param {String} probeHelp - indications about the probe arguments
 */
function handleInsertProbe(probeName, probePath, probeHelp) {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var bladeNumber = findBladeNumber(editorText);
	var firstLine = "blade." + bladeNumber + ".id=" + probeName + bladeNumber;
	var secondLine = "blade." + bladeNumber + ".probe=" + probePath;
	var thirdLine = "blade." + bladeNumber + ".server=local host";
	var fourthLine = "blade." + bladeNumber + ".argument=" + probeHelp;
	var fifthLine = "blade." + bladeNumber + ".comment=";
	var textToInsert = "\n" + firstLine + "\n" + secondLine + "\n" + thirdLine + "\n" + fourthLine + "\n" + fifthLine + "\n";
	findPositionAndInsert(editor, editorText, textToInsert)
}

/**
 * This function prepares the inector-related text that will be inserted in the properties editor
 */
function handleInsertInjector() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var bladeNumber = findBladeNumber(editorText);
	
	//retrieve the filename and change ctp to xis
	var fileName = document.getElementById("displayFileName").innerHTML;	
	fileName = fileName.substring(0, fileName.lastIndexOf(".ctp")) + ".xis";
	
	//create the text to insert
	var firstLine = "blade." + bladeNumber + ".id=injector" + bladeNumber;
	var secondLine = "blade." + bladeNumber + ".injector=IsacRunner";
	var thirdLine = "blade." + bladeNumber + ".server=local host";
	var fourthLine = "blade." + bladeNumber + ".argument=" + fileName;
	var fifthLine = "blade." + bladeNumber + ".comment=";
	var textToInsert = "\n" + firstLine + "\n" + secondLine + "\n" + thirdLine + "\n" + fourthLine + "\n" + fifthLine + "\n";
	findPositionAndInsert(editor, editorText, textToInsert)
}

/**
 * This function retrieves the cursor position and determines where the text will be inserted
 * @param {Object} editor - The ace editor object
 * @param {String} editorText - The text contained in the ace editor
 * @param {String} textToInsert - The text that will be inserted
 */
function findPositionAndInsert(editor, editorText, textToInsert) {
	var cursorPosition = editor.getCursorPosition();
	var Range = ace.require('ace/range').Range;
	var cursorLine = cursorPosition["row"];
	var textArray = editorText.split("\n");
	var lineToInsert = 0;

	var stopSearching = false;
	while (stopSearching == false) {
		if (cursorLine >= textArray.length) {
			stopSearching = true;
			textToInsert = "\n" + textToInsert;
			lineToInsert = cursorLine;
			editor.session.replace(new Range(lineToInsert, Number.MAX_VALUE, lineToInsert, Number.MAX_VALUE), textToInsert);

		}
		else if (textArray[cursorLine] == "") {
			stopSearching = true;
			lineToInsert = cursorLine;
			editor.session.replace(new Range(lineToInsert, 0, lineToInsert, 0), textToInsert);
		}
		else if (textArray[cursorLine].includes(".comment")) {
			stopSearching = true;
			lineToInsert = cursorLine;
			textToInsert = "\n" + textToInsert;
			editor.session.replace(new Range(lineToInsert, Number.MAX_VALUE, lineToInsert, Number.MAX_VALUE), textToInsert);
		}
		else {
			cursorLine++;
		}
	}
}

/**
 * This function browses a property file and find the first blade number that is not already used
 * @param {String} editorText : the text contained in the ace editor
 * @return {number} The first unused blade number
 */
function findBladeNumber(editorText) {
	var i = 0;
	var bladeNumberFound = false;
	var bladeNumber = "";
	while (bladeNumberFound == false) {
		if (editorText.includes("blade." + i + ".")) {
			i++;

		}
		else {
			bladeNumber = i;
			bladeNumberFound = true;
		}
	}
	return bladeNumber;
}
