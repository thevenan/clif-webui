/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**This function retreive the text selected by the user on the ace editor
 *@returns {string} the selected text
 */
function getSelectionText() {
	var selectedText = "";
	var editor = ace.edit(getActiveEditor());
	selectedText = editor.session.getTextRange(editor.getSelectionRange());
	return selectedText;
}

/**This function retreive the text selected by the user on the ace editor.
 * It also deletes the selected text
 *@returns {string} the selected text
 */
function cutSelectionText() {
	var selectedText = "";
	var editor = ace.edit(getActiveEditor());
	var range = editor.selection.getRange();
	selectedText = editor.session.getTextRange(editor.getSelectionRange());
	editor.session.replace(range, "");
	return selectedText;
}

/**This function copy a selected text and add it to the clipboard. 
 * It uses the clipboard API
 */
function copyAceMenu() {
	var selected = getSelectionText();
	if (selected.length > 0) {
		navigator.clipboard.writeText(selected).then(function() {
			/* success */
		}, function() {
			/* failure */
		});
	}
}

/**This function cut a selected text and add it to the clipboard. 
 * It uses the clipboard API
 */
function cutAceMenu() {
	var selected = cutSelectionText();
	if (selected.length > 0) {
		navigator.clipboard.writeText(selected).then(function() {
			/* success */
		}, function() {
			/* failure */
		});
	}
}

/**
 This function comments or uncomment a selected text or row
 */
function commentAceMenu() {
	var editor = ace.edit(getActiveEditor());
	selectedText = editor.session.getTextRange(editor.getSelectionRange());
	var Range = ace.require('ace/range').Range;
	//no selection. 
	if (selectedText.length == 0) {
		commentSingleRow(editor);
	}
	else {
		//retrieve the text contained on the selected lines
		var beginLine = editor.getSelectionRange().start.row;
		var endLine = editor.getSelectionRange().end.row;
		var lineArray = editor.session.getLines(beginLine, endLine);
		//add the line break 
		var selectedText = "";
		for (var i = 0; i < lineArray.length; i++) {
			selectedText += lineArray[i] + "\n";
		}
		//Count the number of comments
		var leftComment = selectedText.indexOf("<!--");
		var leftCommentCount = 0;
		var rightComment = selectedText.indexOf("-->");
		var rightCommentCount = 0;
		while (rightComment != -1) {
			rightCommentCount++;
			rightComment = selectedText.indexOf("-->", rightComment + 1);
		}
		while (leftComment != -1) {
			leftCommentCount++;
			leftComment = selectedText.indexOf("<!--", leftComment + 1);
		}
		//not the same number of comment
		if (rightCommentCount != leftCommentCount) {
			selectedText = "";
			for (var i = 0; i < lineArray.length; i++) {
				selectedText += "<!--" + lineArray[i] + "-->" + "\n";
			}
		}

		//no comment in the selection
		if (rightCommentCount == 0 && leftCommentCount == 0) {
			selectedText.replace(/</g, "<!--");
			selectedText.replace(/>/g, "-->");
		}
		//only one comment
		else if (rightCommentCount == leftCommentCount == 1) {
			//One multiline comment
			if (selectedText.indexOf("<") == selectedText.indexOf("<!--")
				&& selectedText.lastIndexOf(">") == selectedText.lastIndexOf("-->") + 2) {
				selectedText = selectedText.replace(/<!--/g, "<");
				selectedText = selectedText.replace(/-->/g, ">");
			}
			//
			else {
				selectedText = selectedText.replace(/</g, "<!--");
				selectedText = selectedText.replace(/>/g, "-->");
			}
		}
		//more than one comment
		else {
			var leftChevron = selectedText.indexOf("<!--");
			var leftChevronCount = 0;
			var rightChevron = selectedText.indexOf("-->");
			var rightChevronCount = 0;
			while (rightChevron != -1) {
				leftChevronCount++;
				rightChevron = selectedText.indexOf("<!--", rightChevron + 1);
			}
			while (leftChevron != -1) {
				rightChevronCount++;
				leftChevron = selectedText.indexOf("<!--", leftChevron + 1);
			}
			//every selected chevron is a comment
			if (leftCommentCount == rightCommentCount == leftChevronCount == rightChevronCount) {
				selectedText = selectedText.replace(/<!--/g, "<");
				selectedText = selectedText.replace(/-->/g, ">");
			}
			else {
				selectedText = selectedText.replace(/</g, "<!--");
				selectedText = selectedText.replace(/>/g, "-->");
			}
		}
		editor.session.replace(new Range(beginLine, 0, endLine, Number.MAX_VALUE), selectedText);
	}
}
/**
 * Comment or uncomment the line where the cursor is placed.
 * @param {Object} editor - The current editor 
 */
function commentSingleRow(editor) {
	var Range = ace.require('ace/range').Range;
	var currentRowNumber = editor.getSelectionRange().start.row;
	var currentRowText = editor.session.getLine(currentRowNumber);
	//Count the number of comments
	var leftComment = currentRowText.indexOf("<!--");
	var leftCommentCount = 0;
	var rightComment = currentRowText.indexOf("-->");
	var rightCommentCount = 0;
	while (rightComment != -1) {
		rightCommentCount++;
		rightComment = currentRowText.indexOf("-->", rightComment + 1);
	}
	while (leftComment != -1) {
		leftCommentCount++;
		leftComment = currentRowText.indexOf("<!--", leftComment + 1);
	}

	if (rightCommentCount != leftCommentCount) {
		editor.session.replace(new Range(currentRowNumber, 0, currentRowNumber, Number.MAX_VALUE), "<!--" + currentRowText + "-->");
	}
	else if (currentRowText.includes("<!--") && currentRowText.includes("-->")) {
		currentRowText = currentRowText.replace(/<!--/g, "<");
		currentRowText = currentRowText.replace(/-->/g, ">");
		editor.session.replace(new Range(currentRowNumber, 0, currentRowNumber, Number.MAX_VALUE), currentRowText);
	}
	else {
		currentRowText = currentRowText.replace(/</g, "<!--");
		currentRowText = currentRowText.replace(/>/g, "-->");
		editor.session.replace(new Range(currentRowNumber, 0, currentRowNumber, Number.MAX_VALUE), currentRowText);
	}
}