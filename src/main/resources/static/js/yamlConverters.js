

function xmlToYaml() {
	var editorXml = ace.edit("scenarioXML");
	var rawXml = formatXml(editorXml.getValue().replace(/\\/g, '\\\\')).replace(/&/g, 'U+FE60');
	if ($.trim(rawXml).length == 0) {
		return true;
	}
	var xmlcontent = "";
	rawXml = XMLCommentToTag(rawXml);
	rawXml.split("\n").forEach(line => {
		if (line.match(/\'.*\".*\".*\'/g)) {
			var tmp = line.split("'");
			for (i in tmp) {
				if (i == 0) {
					xmlcontent += tmp[i] + "'";
				} else if (i == tmp.length - 1) {
					xmlcontent += tmp[i];
				} else {
					xmlcontent += tmp[i].replace(/"/g, "U+0022") + "'";
				}
			}
		} else {
			xmlcontent += line;
		}
	});
	xmlcontent = parseXml(xmlcontent, "");
	if (xmlcontent.nodeType == 9) // document node
		xmlcontent = xmlcontent.documentElement;
	var xmlObj = xmlToObject(xmlcontent);
	var json = objectToJson(xmlObj, xmlcontent.nodeName, "\t");
	var xmlJson = "{\n" + ("" ? json.replace(/\t/g, "") : json.replace(/\t|\n/g, "")) + "\n}";
	xmlJson = xmlJson.replace('undefined', '').replace(/#/g, '').replace(/@/g, '');
	var obj;
	try {
		obj = JSON.parse(xmlJson);
	} catch (err) {
		alert("Synthax error: XML malformed");
		return false;
	}
	if (obj.parsererror) {
		alert(obj.parsererror.text);
		return false;
	}
	else {
		obj = replaceParams(obj);
		obj = JSON.parse(JSON.stringify(obj).replace(/param":/g, "params\":").replace(/choice":/g, "nchoice\":"));
//		obj = replaceParamChoiceTagsJson(obj);
		obj = replaceSpecialCharYaml(obj);
		var yaml = json2yaml(JSON.parse(JSON.stringify(obj)));
		yaml = yaml.replace(/U\+FE60/g, "&").replace(/U\+0022/g, "\"");
		yaml = transformYamlTagToComment(yaml);
		yaml = topBotXmlCommentToYaml(yaml);
		var editorYaml = ace.edit("scenarioYAML");
		editorYaml.setValue(yaml);
		reloadEditor(editorYaml);
		editorYaml.getSession().foldToLevel(3);
		return true;
	}
}

function replaceParamChoiceTagsJson(obj){
	var jsonString = JSON.stringify(obj);
	var indexParam = jsonString.indexOf("\"param\":");
	var indexChoice = jsonString.indexOf("\"choice\":");
	while(indexParam != -1){
		jsonString = jsonString.substring(0, indexParam) + "\"params\":" + jsonString.substring(0, indexParam + 8);
		indexParam = jsonString.indexOf("\"param\":", indexParam + 1);
	}
	while(indexChoice != -1){
		jsonString = jsonString.substring(0, indexChoice) + "\"nchoice\":" + jsonString.substring(0, indexChoice + 9);
		indexChoice = jsonString.indexOf("\"choice\":", indexParam + 1);
	}
	var obj = JSON.parse(jsonString);
	return obj;
}

/**
*Converts a YAML text to a XML text.
*The text is converted to a JSON format first, and converted from JSON to XML.
*As some XML tags and YAML sections don't have the same name, it's necessary to change some texts")
 */
function yamlToXml() {
	var editorYaml = ace.edit("scenarioYAML");
	var yamlcontent = editorYaml.getValue();
	yamlcontent = YAMLCommentToTag(yamlcontent);
	try {
		var yamlJson = jsyaml.load(yamlcontent);
	} catch (err) {
		alert(err);
		return false
	}
	var obj = replaceParam(yamlJson);
	var xml = JSONtoXML(JSON.parse(JSON.stringify(obj)));
	xml = formatXml(xml);
	xml = '<!DOCTYPE scenario SYSTEM "classpath:org/ow2/clif/scenario/isac/dtd/scenario.dtd">\n' + xml;
	var editorXml = ace.edit("scenarioXML");
	xml = transformXmlTagToComment(xml);
	editorXml.setValue(xml);
	reloadEditor(editorXml);
	editorXml.getSession().foldToLevel(2);
	return true;
}

function propertiesToYaml() {
	var editorProperties = ace.edit("testPlanProperties");
	var propertiesContent = editorProperties.getValue().split("\n");
	var testPlanLines = [];
	var servers = [];
	var comments = [];
	for (var i in propertiesContent) {
		var line = propertiesContent[i];
		var lineStructure = new Object();
		if ($.trim(line).length == 0) { // line is empty		
		} else if ($.trim(line[0]) == "#") {	// line is a comment
			comments.push(line);
		} else {
			lineStructure.group = line.substring(line.indexOf(".") + 1, line.indexOf(".", line.indexOf(".") + 1));
			lineStructure.type = line.substring(line.indexOf(".", line.indexOf(".") + 1) + 1, line.indexOf("="));
			lineStructure.value = line.substring(line.indexOf("=") + 1);
			if (lineStructure.type == "server") {
				if (servers.length != 0) {
					var isNew = true;
					servers.forEach(server => {
						if (server.name == lineStructure.value) {	// server already in servers
							isNew = false;
							server.blade.push(lineStructure.group);
						}
					});

				} else {
					var isNew = true;
				}
				if (isNew) {
					var server = new Object();
					server.name = lineStructure.value;
					server.blade = [];
					server.blade.push(lineStructure.group)
					servers.push(server);
				}
			} else {
				testPlanLines.push(lineStructure);
			}

		}
	}
	// sorting line by server then id
	var testPlanObject = { testPlan: {} }
	var i = 0;
	testPlanObject.testPlan['server'] = [];
	servers.forEach(server => {
		var injectors = [];
		var probes = [];
		testPlanObject.testPlan.server.push({ name: server.name });
		server.blade.forEach(blade => {
			var bladeObject = getBladeObject(blade, testPlanLines);
			if (bladeObject.isInjector) {
				delete bladeObject.isInjector;
				injectors.push(bladeObject);
			} else {
				delete bladeObject.isInjector;
				probes.push(bladeObject);
			}
		});
		if (injectors.length == 1) {
			testPlanObject.testPlan.server[i].injectors = injectors[0];
		} else if (injectors.length > 1) {
			testPlanObject.testPlan.server[i].injectors = injectors;
		}

		if (probes.length == 1) {
			testPlanObject.testPlan.server[i].probes = probes[0];
		} else if (probes.length > 1) {
			testPlanObject.testPlan.server[i].probes = probes;
		}
		i++;
	});



	var obj = replaceSpecialCharYaml(testPlanObject);
	var yaml = json2yaml(JSON.parse(JSON.stringify(obj)));
	comments.forEach(com => {
		yaml = com + "\n" + yaml;
	});
	var editorYaml = ace.edit("testPlanYAML");
	editorYaml.setValue(yaml);
	reloadEditor(editorYaml);
	return true;
}

function yamlToProperties() {
	var editorYaml = ace.edit("testPlanYAML");
	var yamlcontent = editorYaml.getValue();
	var comments = [];
	var yamlLinesContent = yamlcontent.split("\n");
	yamlLinesContent.forEach(line => {
		if ($.trim(line)[0] == "#") {
			comments.push(line);
		}
	});
	var yamlObject;
	try {
		var yamlObject = jsyaml.load(yamlcontent);
	} catch (err) {
		alert(err);
		return false
	}
	var propertiesText = "";
	var blade = 0;
	yamlObject.testPlan.server.forEach(serv => {
		if (serv.injectors) {
			serv.injectors.forEach(injector => {
				propertiesText += "blade." + blade + ".id=" + injector.id + "\n";
				propertiesText += "blade." + blade + ".injector=" + injector.class + "\n";
				propertiesText += "blade." + blade + ".argument=" + injector.argument + "\n";
				propertiesText += ("blade." + blade + ".comment=" + injector.comment + "\n").replace("null", "");
				propertiesText += "blade." + blade + ".server=" + serv.name + "\n\n";
				blade++;
			});
		}
		if (serv.probes) {
			serv.probes.forEach(probes => {
				propertiesText += "blade." + blade + ".id=" + probes.id + "\n";
				propertiesText += "blade." + blade + ".probe=" + probes.class + "\n";
				propertiesText += "blade." + blade + ".argument=" + probes.argument + "\n";
				propertiesText += ("blade." + blade + ".comment=" + probes.comment + "\n").replace("null", "");
				propertiesText += "blade." + blade + ".server=" + serv.name + "\n\n";
				blade++;
			});
		}
	});
	comments.forEach(com => {
		propertiesText = com + "\n" + propertiesText;
	});
	var editorProperties = ace.edit("testPlanProperties");
	editorProperties.setValue(propertiesText);
	reloadEditor(editorProperties);
	return true;
}

function getBladeObject(blade, testPlanLines) {
	var type = "";
	var id = "";
	var classType = "";
	var argument = "";
	var comment = "";
	testPlanLines.forEach(obj => {
		if (obj.group == blade) {
			if (obj.type == "probe") {
				type = "probe";
				classType = obj.value;
			} else if (obj.type == "injector") {
				type = "injector"
				classType = obj.value;
			} else if (obj.type == "argument") {
				argument = obj.value;
			} else if (obj.type == "comment") {
				comment = obj.value;
			} else if (obj.type == "id") {
				id = obj.value;
			}
		}
	});

	return {
		isInjector: (type == "injector"),
		id: id,
		class: classType,
		argument: argument,
		comment: comment
	};
}

/**
*This function browses through the parameters of an a parsed Yaml text
 */
function replaceParams(obj) {
	//On parcours tous les parametres de obj, un objet YAML ou JSON extrait
	for (prop in obj) {
		//si obj existe. Sinon retourner XXX?
		if (obj) {
			if (prop == 'params') {
				var param = obj[prop]['param'];
				delete obj[prop];
				obj['param'] = param;
			}
			if (prop == 'nchoice') {
				var choice = obj[prop]['choice'];
				delete obj[prop];
				obj['choice'] = choice;
				replaceParams(obj['choice']);
			}
			if (typeof obj[prop] != 'string') {
				obj[prop] = replaceParams(obj[prop]);
			}
		}
	}
	return obj;
}


function parseXml(xml) {
	var dom = null;
	if (window.DOMParser) {
		dom = (new DOMParser()).parseFromString(xml, "application/xml");
		try { } catch (e) {
			dom = null;
		}
	} else if (window.ActiveXObject) {
		try {
			dom = new ActiveXObject('Microsoft.XMLDOM');
			dom.async = false;
			if (!dom.loadXML(xml)) // parse error ..

				window.alert(dom.parseError.reason + dom.parseError.srcText);
		} catch (e) {
			dom = null;
		}
	} else
		alert("cannot parse xml string!");
	return dom;
}

function isBlock(nodeName) {
	return nodeName == "preemptive" || nodeName == "while"
		|| nodeName == "behavior" //|| nodeName=="then" || nodeName=="else";
}

function xmlToObject(xml) {
	var o = {};
	if (xml.attributes.length != 0) {
		for (let i = 0; i < xml.attributes.length; i++) {
			o[xml.attributes[i].name] = xml.attributes[i].value;
		}
	}
	if (xml.childElementCount != 0) {
		var doVal;
		for (let i = 0; i < xml.childElementCount; i++) {
			let childName = xml.children[i].nodeName;
			if (childName == "else" || childName == "then") {
				if (xml.children[i].childElementCount != 0) {
					if (xml.children[i].childElementCount == 1) {
						let tmp = {};
						tmp[xml.children[i].children[0].nodeName] = xmlToObject(xml.children[i].children[0]);
						o[childName] = tmp;
					} else {
						o[childName] = new Array();
						for (let j = 0; j < xml.children[i].childElementCount; j++) {
							let tmp = {};
							tmp[xml.children[i].children[j].nodeName] = xmlToObject(xml.children[i].children[j]);
							o[childName][j] = tmp;
						}
					}
				}
				else {
					o[childName] = "";
				}
			} else if (isBlock(xml.nodeName) && childName != "condition" || (xml.nodeName == "choice" && childName != "proba")) {
				if (doVal) {
					let tmp = new Object();
					tmp[childName] = xmlToObject(xml.children[i]);
					doVal[doVal.length] = tmp;
				} else {
					doVal = new Array();
					doVal[0] = new Object();
					doVal[0][childName] = xmlToObject(xml.children[i]);
				}
			} else if (o[childName]) {
				if (o[childName] instanceof Array) {
					o[childName][o[childName].length] = xmlToObject(xml.children[i]);
				} else {
					o[childName] = [o[childName], xmlToObject(xml.children[i])];
				}
			} else {
				o[childName] = xmlToObject(xml.children[i]);
			}
		}
	}
	if (doVal) {
		if (o.do) {
			if (o.do instanceof Array) {
				if (tmp instanceof Array) {
					o.do = o.do.concat(doVal);
				} else {
					o.do[o.do.length] = doVal;
				}
			} else {
				o["do"] = [o["do"], doVal];
			}
		} else {
			o.do = doVal;
		}
	}
	return o;
}

function objectToJson(o, name, ind) {
	var json = name ? ("\"" + name + "\"") : "";
	if (o instanceof Array) {
		for (var i = 0, n = o.length; i < n; i++)
			o[i] = objectToJson(o[i], "", ind + "\t");
		json += (name ? ":[" : "[") + (o.length > 1 ? ("\n" + ind + "\t" + o.join(",\n" + ind + "\t") + "\n" + ind) : o.join("")) + "]";
	} else if (o == null)
		json += (name && ":") + "null";
	else if (typeof (o) == "object") {
		var arr = [];
		for (var m in o)
			arr[arr.length] = objectToJson(o[m], m, ind + "\t");
		json += (name ? ":{" : "{") + (arr.length > 1 ? ("\n" + ind + "\t" + arr.join(",\n" + ind + "\t") + "\n" + ind) : arr.join("")) + "}";
	} else if (typeof (o) == "string")
		json += (name && ":") + "\"" + o.toString() + "\"";
	else
		json += (name && ":") + o.toString();
	return json;
}

(function json2yaml(self) {
	var spacing = "  ";

	function getType(obj) {
		var type = typeof obj;
		if (obj instanceof Array) {
			return 'array';
		} else if (type == 'string') {
			return 'string';
		} else if (type == 'boolean') {
			return 'boolean';
		} else if (type == 'number') {
			return 'number';
		} else if (type == 'undefined' || obj === null) {
			return 'null';
		} else {
			return 'hash';
		}
	}

	function convert(obj, ret) {
		var type = getType(obj);

		switch (type) {
			case 'array':
				convertArray(obj, ret);
				break;
			case 'hash':
				convertHash(obj, ret);
				break;
			case 'string':
				convertString(obj, ret);
				break;
			case 'null':
				ret.push('null');
				break;
			case 'number':
				ret.push(obj.toString());
				break;
			case 'boolean':
				ret.push(obj ? 'true' : 'false');
				break;
		}
	}

	function convertArray(obj, ret) {
		if (obj.length === 0) {
			ret.push('[]');
		}
		for (var i = 0; i < obj.length; i++) {

			var ele = obj[i];
			var recurse = [];
			convert(ele, recurse);

			for (var j = 0; j < recurse.length; j++) {
				ret.push((j == 0 ? "- " : spacing) + recurse[j]);
			}
		}
	}

	function convertHash(obj, ret) {
		for (var k in obj) {
			var recurse = [];
			if ((k == 'params' && (typeof obj['params'].length == "undefined"))
				|| (k == 'nchoice' && (typeof obj['nchoice'].length == "undefined"))
				|| (k == 'injectors' && (typeof obj['injectors'].length == "undefined"))
				|| (k == 'probes' && (typeof obj['probes'].length == "undefined"))) {
				var ele = obj[k];
				convert(ele, recurse);
				ret.push(k + ':')
				for (var j = 0; j < recurse.length; j++) {
					if (j == 0) ret.push(spacing + "- " + recurse[j]);
					else ret.push(spacing + spacing + recurse[j]);

				}
			} else {
				if (obj.hasOwnProperty(k)) {
					var ele = obj[k];
					convert(ele, recurse);
					var type = getType(ele);
					if (type == 'string' || type == 'null' || type == 'number' || type == 'boolean') {
						ret.push(k + ': ' + recurse[0]);
					} else {
						ret.push(k + ': ');
						for (var i = 0; i < recurse.length; i++) {
							ret.push(spacing + recurse[i]);
						}
					}
				}
			}
		}
	}

	function convertString(obj, ret) {
		ret.push(obj);
	}

	self.json2yaml = function(obj) {
		if (typeof obj == 'string') {
			obj = JSON.parse(obj);
		}

		var ret = [];
		convert(obj, ret);
		return ret.join("\n");
	};
})(this);

/**
*Converts a JSON text to a XML text.
*Recursive function. Call itself to browse every indent level.
*@param {object} obj - the input JSON text as an object.
*Indents levels corresponds to braces levels.
*Example : obj = {scenario: { behaviors: {behavior: {'id:B0'}}}
*@returns {string} xml - the converted text to xml
 */
function JSONtoXML(object) {
	var xml = ''; //xml text
	//name : braces name at first indent level of obj
	//ex : scenario
	const obj = JSON.parse(JSON.stringify(object));
	for (var name in obj) {
		// Xml tag : < name + args >
		var args = '';
		var text = '';
		var xmlTagWithoutContent = '';

		//key: tag name at second level of indent
		//ex: behaviors
		//value: the content of the braces at second level indent
		//ex : behavior: {'id:B0'}
		for (var [key, value] of Object.entries(obj[name])) {
			//manages empty tags. They will be added before or after the recursive call 
			//depending on where they must be added on the xml file			
			if (value == null) {
				value = "";
				if (key == "use" || key == "name" || key == "id") {
					args += ' ' + key + '="' + value + '"';
				}
				else {
					xmlTagWithoutContent += "<" + key + ">";
					xmlTagWithoutContent += "</" + key + ">";
				}
				//delete an indent level to browse through it only once because of the recursive call
				delete obj[name][key];
			}
			//this tag is a leaf, and will become an argument for parent tag
			else if (!(value instanceof Array) && !(isNaN(Object.keys(value)[0]) && isNaN(value))) {
				if (value === null) value = "";

				if (isNaN(value) && (value.includes('"'))) {
					args += ' ' + key + "='" + value + "'";
				} else {
					args += ' ' + key + '="' + value + '"';
				}
				delete obj[name][key];
			}
		}

		//add the xml tag to the output xml text
		if (args != '') { // tag has arguments
			xml += "<" + name + args + ">"
		}
		//tag has no argument
		if (!(obj[name] instanceof Array) && args == '') {
			xml += "<" + name + ">";
			if (name == 'behaviors') {
				xml += xmlTagWithoutContent;
				xmlTagWithoutContent = '';
			}
		}
		// recursive call to the function to apply the same reasonning to the other indentations 
		if (obj[name] instanceof Array) { // List 
			if (name == "then" || name == "else") {
				xml += "<" + name + ">";
				for (let i = 0; i < obj[name].length; i++) {
					xml += JSONtoXML(obj[name][i]);
				}
				delete obj[name];
			} else {
				for (var array in obj[name]) {
					var isLeaf = true;
					for (var [key, value] of Object.entries(obj[name][array])) {
						if (value == null) {
							value = "";
							if (isNaN(value) && (value.includes('"'))) {
								args += ' ' + key + "='" + value + "'";
							} else {
								args += ' ' + key + '="' + value + '"';
							}
							delete obj[name][array][key];

						} else if (isNaN(Object.keys(value)[0]) && isNaN(value)) {
							isLeaf = false;
						} else if (value instanceof Array) {
							isLeaf = false;
						}
						else {
							// tag with keyword becomes argument for parent tag
							if (value === null) value = "";
							if (!(value instanceof Array)) {
								if (isNaN(value) && (value.includes('"'))) {
									args += ' ' + key + "='" + value + "'";
								} else {
									args += ' ' + key + '="' + value + '"';
								}
								delete obj[name][array][key];
							}
						}
					}
					if (name != "do") xml += "<" + name + args + ">" + text;
					if (!isLeaf) xml += JSONtoXML(obj[name][array]);
					if (name != "do") xml += "</" + name + ">";
					args = '';
					text = '';
				}
			}
		} else if (typeof obj[name] == "object") { // Object
			xml += JSONtoXML(new Object(obj[name]));
		} else { // String
			xml += obj[name];
		}
		xml += xmlTagWithoutContent;
		xml += (obj[name] instanceof Array) ? '' : "</" + name + ">";
	}

	var xml = xml.replace(/<\/?[0-9]{1,}>/g, '');
	return xml
}

function formatXml(xml) {
	var reg = /(>)\s*(<)(\/*)/g;
	var wsexp = / *(.*) +\n/g;
	var contexp = /(<.+>)(.+\n)/g;
	xml = xml.replace(reg, '$1\n$2$3').replace(wsexp, '$1\n').replace(contexp, '$1\n$2');
	var pad = 0;
	var formatted = '';
	var lines = xml.split('\n');
	var indent = 0;
	var lastType = 'other';
	// 4 types of tags - single, closing, opening, other (text, doctype, comment) - 4*4 = 16 transitions 
	var transitions = {
		'single->single': 0,
		'single->closing': -1,
		'single->opening': 0,
		'single->other': 0,
		'closing->single': 0,
		'closing->closing': -1,
		'closing->opening': 0,
		'closing->other': 0,
		'opening->single': 1,
		'opening->closing': 0,
		'opening->opening': 1,
		'opening->other': 1,
		'other->single': 0,
		'other->closing': -1,
		'other->opening': 0,
		'other->other': 0
	};

	for (var i = 0; i < lines.length; i++) {
		var ln = lines[i];

		if (ln.match(/\s*<\?xml/)) {
			formatted += ln + "\n";
			continue;
		}

		var single = Boolean(ln.match(/<.+\/>/)); // is this line a single tag? ex. <br />
		var closing = Boolean(ln.match(/<\/.+>/)); // is this a closing tag? ex. </a>
		var opening = Boolean(ln.match(/<[^!].*>/)); // is this even a tag (that's not <!something>)
		var type = single ? 'single' : closing ? 'closing' : opening ? 'opening' : 'other';
		var fromTo = lastType + '->' + type;
		lastType = type;
		var padding = '';

		indent += transitions[fromTo];
		for (var j = 0; j < indent; j++) {
			padding += '\t';
		}
		if (fromTo == 'opening->closing')
			formatted = formatted.substr(0, formatted.length - 1) + ln + '\n'; // substr removes line break (\n) from prev loop
		else
			formatted += padding + ln + '\n';
	}

	return formatted;
}
/**
	This function is used to format the whole xml text
 */
function formatXmlV2(xml, tab = '\t', nl = '\n') {
	//first and last characters must be "<" or ">"
	while (xml[0] != "<") {
		xml = xml.slice(1);
	}
	while (xml[xml.length - 1] != ">") {
		xml = xml.slice(0, xml.length - 1);
	}
	let formatted = '', indent = '';
	//slice : delete the first "<" and last ">" so that the first and last element of node won't be empty
	const nodes = xml.slice(1, -1).split(/>\s*</);
	if (nodes[0][0] == '?') formatted += '<' + nodes.shift() + '>' + nl;
	for (let i = 0; i < nodes.length; i++) {
		const node = nodes[i];
		//handle comments
		if (node[0] == '/') {
			indent = indent.slice(tab.length); // decrease indent
		}
		formatted += indent + '<' + node + '>' + nl;
		if (node[0] != '/' && node[node.length - 1] != '/' && node.indexOf('</') == -1 && node[0] != '!') {
			indent += tab; // increase indent
		}
	}
	return formatted;
}


function replaceSpecialCharYaml(obj) {
	for (prop in obj) {
		if (obj) {
			if (typeof obj[prop] == 'string') {
				obj[prop].replace(/"/g, '\"');

				if (obj[prop].includes("'") && !obj[prop].includes('"')) {
					obj[prop] = '"' + obj[prop] + '"';
				} else
					if (obj[prop].includes(',') ||
						obj[prop].includes('{') ||
						obj[prop].includes('[') ||
						obj[prop].includes(']') ||
						obj[prop].includes('}') ||
						obj[prop].includes('*') ||
						obj[prop].includes('#') ||
						obj[prop].includes('?') ||
						obj[prop].includes('}') ||
						obj[prop].includes('|') ||
						obj[prop].includes('-') ||
						obj[prop].includes('<') ||
						obj[prop].includes('>') ||
						obj[prop].includes('=') ||
						obj[prop].includes('!') ||
						obj[prop].includes('%') ||
						obj[prop].includes('@') ||
						obj[prop].includes('\\') ||
						obj[prop].includes('!') ||
						obj[prop].includes(')') ||
						obj[prop].includes(':') ||
						obj[prop].includes('(')

					) {
						obj[prop] = "'" + obj[prop] + "'";
					}
			}
			else {
				obj[prop] = replaceSpecialCharYaml(obj[prop]);
			}
		}
	}
	return obj;
}

function replaceSpecialCharXml(obj) {
	for (prop in obj) {
		if (obj) {
			if (((typeof obj[prop] == 'string'))) {
				if ((obj[prop].match(/"/g) || []).length > 2) {
					obj[prop] = "'" + obj[prop] + "'";
				}
			}
			else {
				obj[prop] = replaceSpecialCharXml(obj[prop]);
			}
		}
	}
	return obj;
}
/**
*This function browses through the parameters of a Yaml text to change the name of some YAML sections and
adapt it to match XML tags
*@param obj - the YAML text
 */
function replaceParam(obj) {
	//We browse through every parsed word to change some names given to some sections of the YAML
	for (prop in obj) {
		if (obj) {
			//When prop =='params', we save the datas contained in obj[params]
			//if params is an array, we recover its datas and save it under the name 'param'
			if (prop == 'params') {
				var params = obj['params'];
				delete obj['params'];
				var tmp = new Object();
				if (Array.isArray(params)) {
					tmp['param'] = [];
					for (param in params) {
						tmp['param'].push(params[param]);
					}
				} else {
					tmp['param'] = params;
				}
				obj['params'] = tmp;
			} else if (prop == 'nchoice') {
				var nchoice = obj['nchoice'];
				delete obj['nchoice'];
				var tmp = new Object();
				if (Array.isArray(nchoice)) {
					tmp['choice'] = [];
					for (choice in nchoice) {
						tmp['choice'].push(nchoice[choice]);
					}
				} else {
					tmp['choice'] = nchoice;
				}
				obj['nchoice'] = tmp;
				replaceParam(obj['nchoice']);
			}
			else if (typeof obj[prop] != 'string') {
				obj[prop] = replaceParam(obj[prop]);
			}
		}
	}
	return obj;
}

/**
 *This function retrieves the xml of the ace editor and transforms it to a javascript object.
 *@param {String} xmlText - String that we want to convert to an object
 */
function stringToObj(xmlText) {
	if (xmlText == undefined || xmlText == null || xmlText == "") {
		var editorXml = ace.edit(getActiveEditor());
		var rawXml = formatXml(editorXml.getValue().replace(/\\/g, '\\\\')).replace(/&/g, 'U+FE60');
	}
	else {
		var rawXml = formatXml(xmlText.replace(/\\/g, '\\\\')).replace(/&/g, 'U+FE60');
	}
	if ($.trim(rawXml).length == 0) {
		return true;
	}
	var xmlcontent = "";
	rawXml.split("\n").forEach(line => {
		if (line.match(/\'.*\".*\".*\'/g)) {
			var tmp = line.split("'");
			for (i in tmp) {
				if (i == 0) {
					xmlcontent += tmp[i] + "'";
				} else if (i == tmp.length - 1) {
					xmlcontent += tmp[i];
				} else {
					xmlcontent += tmp[i].replace(/"/g, "U+0022") + "'";
				}
			}
		} else {
			xmlcontent += line;
		}
	});
	xmlcontent = parseXml(xmlcontent, "");
	if (xmlcontent.nodeType == 9) // document node
		xmlcontent = xmlcontent.documentElement;
	var xmlObj = xmlToObject(xmlcontent);

	return xmlObj;
}


