<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plugin SYSTEM "classpath:org/ow2/clif/scenario/isac/dtd/plugin.dtd">

<plugin name="Synchro">
  <object class="org.ow2.isac.plugin.synchro.SessionObject">
    <params>
      <param name="distributed" type="String"></param>
      <param name="domain" type="String"></param>
    </params>
    <help>Synchronization plug-in, for all virtual users of all behaviors of all deployed scenarios, either locally in current scenario, or, optionally, among several scenarios possibly distributed among several CLIF servers. The latter mode is enabled by selecting the "distributed option" on plug-in import and giving an arbitrary domain name. To support this distributed synchronization domain, the test plan must include a pseudo injector of class Synchro, with the chosen domain name as argument. You may deploy this Synchro pseudo-injector on any CLIF server in your test plan.

Synchronization principles: named locks with wait/notify primitives. The status of a lock may also be tested. Synchronization can also take into account the number of notify calls on one lock. Rendez-vous provide an alternative usage, where the number of notify calls necessary to release the lock is predefined: either with the set-rendez-vous control primitive of this plug-in, or by adding lock_name=count arguments to the Synchro pseudo-injector argument line in the test plan.

Some variables are provided:
${synchroId:} gives the list of released locks,
${synchroId:lock_name} gives the number of notifications for the specified lock.

CAUTION: each virtual user blocked on a wait, waitN or rendez-vous call, keeps its own execution thread. As a consequence, the pool of ISAC execution threads must be sized in a relevant manner, with at least one execution thread per synchronizing virtual user. An alternative to avoid this constraint on threads is to make an active wait loop with a small sleep period, using wasNotified or wasNotifiedN condition.</help>
  </object>
  <control name="notify" number="0">
    <params>
      <param name="lock" type="String"></param>
    </params>
    <help>Notifies the given lock, releasing all virtual users blocked waiting for this lock (possibly depending of the number of notify calls issued). This notification is remanent: further "wait" calls on this lock will not block the virtual user.</help>
  </control>
  <control name="wait" number="1">
    <params>
      <param name="lock" type="String"></param>
      <param name="timeout" type="String"></param>
    </params>
    <help>Waits until the given lock is notified, or the given time-out is elapsed (0 means infinite timeout, negative values are forbidden). If this lock has been notified before the "wait" call, the virtual user is not blocked (notifications are remanent).</help>
  </control>
  <control name="waitN" number="4">
    <params>
      <param name="lock" type="String"></param>
      <param name="timeout" type="String"></param>
      <param name="times" type="String"></param>
    </params>
    <help>Waits until the given lock has been notified at least N times, or the given time-out is elapsed (0 means infinite timeout, negative values are forbidden).</help>
  </control>
  <control name="set-rendez-vous" number="5">
    <params>
      <param name="lock" type="String"></param>
      <param name="times" type="String"></param>
    </params>
    <help>Sets the number of times the given lock will have to be notified so that it is considered as released.</help>
  </control>
  <control name="rendez-vous" number="6">
    <params>
      <param name="lock" type="String"></param>
      <param name="timeout" type="String"></param>
    </params>
    <help>Notifies the given lock and then waits until it has been notified at least the number of times specified when the rendez-vous was set, or the given time-out is elapsed (0 means infinite timeout, negative values are forbidden). If the rendez-vous is not set yet, then this call first waits for the rendez-vous to be set (still meeting the time out).</help>
  </control>
  <test name="wasNotified" number="2">
    <params>
      <param name="options" type="String"></param>
      <param name="lock" type="String"></param>
    </params>
    <help>True if the provided lock name has been released already, false otherwise. The "not" option just reverses the condition logic.</help>
  </test>
  <test name="wasNotifiedN" number="3">
    <params>
      <param name="options" type="String"></param>
      <param name="lock" type="String"></param>
      <param name="times" type="String"></param>
    </params>
    <help>True if the provided lock name has been released already N times, false otherwise. The "not" option just reverses the condition logic.</help>
  </test>
</plugin>

