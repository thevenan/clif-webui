/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005-2007,2012 France Telecom
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.csvprovider;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.ClifClassLoader;

/**
 * Implementation of a session object for plugin ~CSVProvider~
 * Data provider taking external data from a CSV file.
 * Lines are read from a file as sequences of fields separated by a given separator character.
 * Each field may be referenced either by its index with an array notation [<i>n</i>]
 * (first index is 0) or by a name as optionally set at specimen session object creation.
 * Use <code>next</code> control to go to next line.
 * The sequence of line for each session object depends on two switches:
 * <ul>
 * <li>shared or not: when shared is enabled, the sequence of lines is shared by all
 * session objects associated to the same behavior (in other words copies of the same specimen).
 * When disabled, all session objects have their own sequence of lines. 
 * <li>loop or not: when loop is enabled, the sequence of lines automatically loops
 * to the first line when the last line is reached. When disabled, the <code>reset</code> control
 * must be used to loop; otherwise, an exception is thrown.
 * </ul>
 *  
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class SessionObject
	implements SessionObjectAction, TestAction, ControlAction, DataProvider
{
	static final String PLUGIN_BIGFILE = "bigfile";
	static final int CONTROL_NEXT = 2;
	static final int CONTROL_RESET = 3;
	static final int CONTROL_SKIP = 4;
	static final String CONTROL_SKIP_N = "n";
	static final int TEST_ENDOFFILE = 0;
	static final int TEST_NOTENDOFFILE = 1;
	static final String PLUGIN_FIELDS = "fields";
	static final String PLUGIN_LOOP = "loop";
	static final String PLUGIN_SHARED = "shared";
	static final String PLUGIN_MACINTOSH_LINE_SEPARATOR = "macintosh_line_separator";
	static final String PLUGIN_SEPARATOR = "separator";
	static final String PLUGIN_FILENAME = "filename";
	static final String PLUGIN_COMMENT = "comment";
	static final String ENABLE_CBX = "enable";
	static final String LINES_GET = "#";

	public static final char DEFAULT_SEPARATOR = ',';
	private static final Pattern pattern = Pattern.compile("\\[(\\d+)\\]");
	private String filename;
	private boolean macintoshLineSeparator;
	private char separator;
	private List<String> commentPrefixes;
	private List<String[]> allValues;
	private String[] currentValues;
	private String[] fieldNames;
	private int position = 0;
	private Boolean shared;
	private boolean loop;
	private boolean bigFile;
	private Reader input;
	private SessionObject parent;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		String tmp;

		if (params == null)
		{
			fieldNames = null;
		}
		else
		{
			separator =
				(tmp = params.get(PLUGIN_SEPARATOR)) != null && tmp.length() >= 1
				? tmp.charAt(0)
				: DEFAULT_SEPARATOR;
			try
			{
				fieldNames =
					(tmp = params.get(PLUGIN_FIELDS)) != null
					? split(new StringReader(tmp), null, separator, false)
					: null;
			}
			catch (IOException ex)
			{
				throw new IsacRuntimeException(
					"Incorrect field names in CsvProvider: " + params.get(PLUGIN_FIELDS),
					ex);
			}
			macintoshLineSeparator = ParameterParser.getCheckBox(
				params.get(PLUGIN_MACINTOSH_LINE_SEPARATOR)).contains(ENABLE_CBX);
			shared = ParameterParser.getCheckBox(
				params.get(PLUGIN_SHARED)).contains(ENABLE_CBX);
			loop = ParameterParser.getCheckBox(
				params.get(PLUGIN_LOOP)).contains(ENABLE_CBX);
			bigFile = ParameterParser.getCheckBox(
				params.get(PLUGIN_BIGFILE)).contains(ENABLE_CBX);
			commentPrefixes = ParameterParser.getNField(params.get(PLUGIN_COMMENT));
			try
			{
				filename = params.get(PLUGIN_FILENAME);
				if (shared || ! bigFile)
				{
					input = new InputStreamReader(ClifClassLoader.getClassLoader().getResourceAsStream(filename));
				}
				if (! bigFile)
				{
					allValues = new ArrayList<String[]>();
					String[] line;
					while ((line = split(input, commentPrefixes, separator, macintoshLineSeparator)) != null)
					{
						allValues.add(line);
					}
					input.close();
				}
				position = -1;
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException(
					"CsvProvider can't parse file " + params.get(PLUGIN_FILENAME),
					ex);
			}
		}
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		allValues = so.allValues;
		filename = so.filename;
		fieldNames = so.fieldNames;
		macintoshLineSeparator = so.macintoshLineSeparator;
		commentPrefixes = so.commentPrefixes;
		separator = so.separator;
		shared = so.shared;
		loop = so.loop;
		bigFile = so.bigFile;
		if (shared)
		{
			parent = so;
			input = so.input;
		}
		reset();
	}


	/**
	 * Reads one line of text from the provided Reader object, and splits it into an array
	 * of fields according to the specified separator character. If this line starts with
	 * one of the specified comment prefixes, it is skipped and the next line is parsed,
	 * and so on until a non commented line is found, or the end of file is reached.  
	 * @param enumeration
	 * @param commentPrefixes list of strings to be considered as comment prefixes when found
	 * at the beginning of the line
	 * @param separator character separating the line fields
	 * @param macintoshLineSeparator if true, the considered end-of-line character is just CR,
	 * instead of LF when true. 
	 * @return the array of strings containing the fields
	 */
	static private String[] split(
		Reader enumeration,
		List<String> commentPrefixes,
		char separator,
		boolean macintoshLineSeparator)
	throws IOException
	{
		final List<String> l = new LinkedList<String>();
		final StringBuilder s = new StringBuilder();
		int c;
		int state = 0;
		boolean again = true;

		/* CSV parser automaton */
		while (again)
		{
			c = enumeration.read();
			switch (state)
			{
				case 0: // normal character reading
					switch (c)
					{
						case -1:
							l.add(new String(s));
							again = false;
							break;
						case '\r':
							if (macintoshLineSeparator)
							{
								l.add(new String(s));
								again = false;
							}
							break;
						case '\n':
							if (!macintoshLineSeparator)
							{
								l.add(new String(s));
								again = false;
							}
							break;
						case '"':
							state = 2;
							break;
						default:
							if (isComment(s.toString() + (char)c, l, commentPrefixes))
							{
								s.setLength(0);
								state = 1;
							}
							else if (c == separator)
							{
								l.add(new String(s));
								s.setLength(0);
							}
							else
							{
								s.append((char)c);
							}
					}
					break;
				case 1: // discard comment line
					switch (c)
					{
						case -1:
							again = false;
							break;
						case '\r':
							if (macintoshLineSeparator)
							{
								state = 0;
							}
							break;
						case '\n':
							if (!macintoshLineSeparator)
							{
								state = 0;
							}
							break;
					}
					break;
				case 2: // reading a "-delimited sequence
					switch (c)
					{
						case -1:
							throw new IOException("Ending \" character is missing at end of file.");
						case '"':
							state = 3;
							break;
						default:
							s.append((char)c);
					}
					break;
				case 3: // found a " character in a "-delimited sequence
					switch (c)
					{
						case -1:
							l.add(new String(s));
							again = false;
							break;
						case '"': // "" is the escape sequence for "
							s.append('"');
							state = 2;
							break;
						case '\r':
							state = 0;
							if (macintoshLineSeparator)
							{
								l.add(new String(s));
								again = false;
							}
							break;
						case '\n':
							state = 0;
							if (!macintoshLineSeparator)
							{
								l.add(new String(s));
								again = false;
							}
							break;
						default:
							state = 0;
							if (c == separator)
							{
								l.add(new String(s));
								s.setLength(0);
							}
							else
							{
								s.append((char)c);
							}
						}
				}
		}
		return
			(l.size() == 1 && l.get(0).length() == 0)
			? null
			: l.toArray(new String[l.size()]);
	}


	private static boolean isComment(
		String field,
		List<String> l,
		List<String> commentPrefixes)
	{
		if (commentPrefixes != null && l.isEmpty())
		{
			for (String prefix : commentPrefixes)
			{
				if (field.startsWith(prefix))
				{
					return true;
				}
			}
		}
		return false;
	}

	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		position = -1;
		currentValues = null;
		if (bigFile && ! shared)
		{
			try
			{
				if (input != null)
				{
					input.close();
				}
				input = new InputStreamReader(ClifClassLoader.getClassLoader().getResourceAsStream(filename));
			}
			catch (Exception ex)
			{
				input = null;
				throw new IsacRuntimeException(
					"Can't (re)set CsvProvider session object with file \"" + filename + "\".",
					ex);
			}
		}
	}

	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String,String> params)
	{
		switch (number)
		{
			case TEST_ENDOFFILE:
				return position != -1 && currentValues == null;
			case TEST_NOTENDOFFILE:
				return position == -1 || currentValues != null;
			default:
				throw new Error("Unable to find this test in ~CSVProvider~ ISAC plugin: " + number);
		}
	}


	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_NEXT:
				if (shared)
				{
					synchronized(shared)
					{
						doNext();
					}
				}
				else
				{
					doNext();
				}
				break;
			case CONTROL_RESET:
				if (shared)
				{
					synchronized(shared)
					{
						parent.position = -1;
						if (bigFile)
						{
							try
							{
								parent.input.close();
								parent.input = new InputStreamReader(ClifClassLoader.getClassLoader().getResourceAsStream(filename));
								input = parent.input;
							}
							catch (Exception ex)
							{
								input = null;
								parent.input = null;
								throw new IsacRuntimeException(
									"Can't reset CsvProvider with file \"" + filename + "\".",
									ex);
							}
						}
						reset();
					}
				}
				else
				{
					reset();
				}
				break;
			case CONTROL_SKIP:
			{
				int skip = Integer.parseInt(params.get(CONTROL_SKIP_N)); 
				if (shared)
				{
					synchronized(shared)
					{
						while (skip-- > 0)
						{
							doNext();
						}
					}
				}
				else
				{
					while (skip-- > 0)
					{
						doNext();
					}
				}
				break;
			}
			default:
				throw new Error("Unable to find this control in ~CSVProvider~ ISAC plugin: " + number);
		}
	}

	private void doNext()
	{
		// try to get the next line of data
		if (shared)
		{
			position = parent.position;
			input = parent.input;
		}
		++position;
		if (bigFile)
		{
			try
			{
				currentValues = split(input, commentPrefixes, separator, macintoshLineSeparator);
			}
			catch (IOException ex)
			{
				throw new IsacRuntimeException(
					"CsvProvider can't read file \"" + filename + "\".",
					ex);
			}
		}
		else if (position < allValues.size())
		{
			currentValues = allValues.get(position);
		}
		else
		{
			currentValues = null;
		}
		// if there is no next line
		if (currentValues == null)
		{
			if (loop)
			{
				position = -1;
				if (shared)
				{
					parent.position = -1;
				}
				if (bigFile)
				{
					try
					{
						if (input != null)
						{
							input.close();
						}
						input = new InputStreamReader(ClifClassLoader.getClassLoader().getResourceAsStream(filename));
					}
					catch (Exception ex)
					{
						input = null;
						throw new IsacRuntimeException(
							"CsvProvider session object can't access file \"" + filename + "\".",
							ex);
					}
					if (shared)
					{
						parent.input = input;
					}
				}
				doNext();
			}
		}
		else if (shared)
		{
			parent.position = position;
		}
	}

	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		if (var.equals(LINES_GET))
		{
			if (bigFile)
			{
				throw new IsacRuntimeException(
					"CsvProvider for file \""
					+ filename
					+ "\": the number of lines is not available when configured to load lines at runtime.");
			}
			else
			{
				return String.valueOf(allValues.size());
			}
		}
		else if (position == -1)
		{
			throw new IsacRuntimeException(
				"Can't get variable "
				+ var
				+ " from plug-in CsvProvider for file \""
				+ filename
				+ "\": missing initial call to control \"next\".");
		}
		else if (currentValues == null)
		{
			throw new IsacRuntimeException("End of file \"" + filename + "\" reached in CsvProvider.");
		}
		else
		{
			Matcher m;
			int i, n;

			if (fieldNames != null)
				for (i = 0, n = fieldNames.length; i < n; i++)
					if (fieldNames[i].equals(var))
						return (i < currentValues.length) ? currentValues[i] : null;
			m = pattern.matcher(var);
			return
				m.matches()
				&& (i = Integer.parseInt(m.group(1))) < currentValues.length
				? currentValues[i]
				: null;
		}
	}
}
