package org.ow2.isac.plugin.stringhandler;

import java.util.Map;
import java.util.HashMap;

public abstract class Base64Test
{
	static final String TEST_STRING = "âàçéêèëêîïù";
	static final String ENCODED_STRING = "w6LDoMOnw6nDqsOow6vDqsOuw6/DuQ==";
	static final String URLSAFE_ENCODED_STRING = "w6LDoMOnw6nDqsOow6vDqsOuw6_DuQ==";
	static final String ENCODING = "UTF8";

	/**
	 * @param args no argument
	 */
	public static void main(String[] args)
	{
		System.out.println("source string: " + TEST_STRING);
		Map<String,String> params = new HashMap<String,String>();
		params.put(SessionObject.PLUGIN_DEFAULT, TEST_STRING);
		SessionObject specimen = new SessionObject(params);
		SessionObject so = (SessionObject)specimen.createNewSessionObject();

		params.clear();
		params.put(SessionObject.CONTROL_BASE64ENCODE_CHARSET, ENCODING);
		so.doControl(SessionObject.CONTROL_BASE64ENCODE, params);
		System.out.println("Base64 encoding: " + so.doGet(""));
		assert so.doGet("").equals(ENCODED_STRING) : "Base64 encoding result differs from expected";

		params.clear();
		params.put(SessionObject.CONTROL_BASE64DECODE_CHARSET, ENCODING);
		so.doControl(SessionObject.CONTROL_BASE64DECODE, params);
		System.out.println("Base64 decoding: " + so.doGet(""));
		assert so.doGet("").equals(TEST_STRING) : "Base64 decoding result differs from expected";

		params.clear();
		params.put(SessionObject.CONTROL_BASE64ENCODE_CHARSET, ENCODING);
		params.put(SessionObject.CONTROL_BASE64ENCODE_OPTIONS, "URL-safe");
		so.doControl(SessionObject.CONTROL_BASE64ENCODE, params);
		System.out.println("URL-safe base64 encoding: " + so.doGet(""));
		assert so.doGet("").equals(URLSAFE_ENCODED_STRING) : "URL-safe base64 encoding result differs from expected";

		params.clear();
		params.put(SessionObject.CONTROL_BASE64DECODE_CHARSET, ENCODING);
		params.put(SessionObject.CONTROL_BASE64DECODE_URLSAFE, "yes");
		so.doControl(SessionObject.CONTROL_BASE64DECODE, params);
		System.out.println("URL-safe base64 decoding: " + so.doGet(""));
		assert so.doGet("").equals(TEST_STRING) : "URL-safe base64 decoding result differs from expected";
		
		params.clear();
		params.put(SessionObject.CONTROL_BASE64ENCODE_CHARSET, ENCODING);
		params.put(SessionObject.CONTROL_BASE64ENCODE_OPTIONS, "URL-safe;Discard padding");
		so.doControl(SessionObject.CONTROL_BASE64ENCODE, params);
		System.out.println("URL-safe base64 encoding, no padding: " + so.doGet(""));
		assert so.doGet("").equals(
			URLSAFE_ENCODED_STRING.substring(0, URLSAFE_ENCODED_STRING.indexOf('=')))
			: "URL-safe base64 encoding without padding result differs from expected";

		params.clear();
		params.put(SessionObject.CONTROL_BASE64DECODE_CHARSET, ENCODING);
		params.put(SessionObject.CONTROL_BASE64DECODE_URLSAFE, "yes");
		so.doControl(SessionObject.CONTROL_BASE64DECODE, params);
		System.out.println("URL-safe base64 decoding, no padding: " + so.doGet(""));
		assert so.doGet("").equals(TEST_STRING) : "URL-safe base64 decoding without padding result differs from expected";
	}
}
