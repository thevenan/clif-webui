/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class util {

    public static boolean isJUnitTest() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<StackTraceElement> list = Arrays.asList(stackTrace);
        for (StackTraceElement element : list) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }

    public static String getUserLogged() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    public static boolean canEditFile(String permission) {
        return permission.contains("admin") || permission.contains("editor");
    }

    public static String getProjectName(String filename) {
        if (!filename.contains("/"))
            return filename;
        else
            return filename.substring(0, filename.indexOf("/"));

    }

    public static String getFileName(String filename) {
        if (!filename.contains("/"))
            if (!filename.contains("."))
                return filename;
            else
                return filename.substring(0,filename.lastIndexOf("."));
        else
            if (!filename.contains("."))
                return filename.substring(filename.indexOf("/") + 1);
            else
                return filename.substring(filename.indexOf("/") + 1, filename.lastIndexOf("."));

    }

    public static String[] getArgsFile(String file) {
        String[] tmp = { file.substring(0, file.indexOf("\n")), file.substring(file.indexOf("\n") + 1) };
        return tmp;
    }

    public static String[] getArgsPermissions(String attributes) {
        String[] tmp = { attributes.substring(0, attributes.indexOf("/")),
                attributes.substring(attributes.indexOf("/") + 1, attributes.indexOf("/", attributes.indexOf("/") + 1)),
                attributes.substring(attributes.indexOf("/", attributes.indexOf("/") + 1) + 1) };
        return tmp;
    }

    public static String returnMessage(String properties, String[] args) {
        ResourceBundle messages = ResourceBundle.getBundle("lang/res", LocaleContextHolder.getLocale());
        String message = messages.getString(properties);
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                message = message.replace("%" + (i + 1), args[i]);
            }
        }
        return message;
    }

    public static void createPaclifOpts(String target) throws IOException {
        InputStream is = new ClassPathResource("paclif.opts").getInputStream();
        byte[] buffer = new byte[is.available()];
        is.read(buffer);	 
        File targetFile = new File(target);
        OutputStream os = new FileOutputStream(targetFile);
        os.write(buffer);
        os.close();
        is.close();        
	}
}


