package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.communication"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.communication")
public class ConfigProactiveCommunication {
	
	private String protocol = null;
	private String additional_protocols =null;

	public String getAdditional_protocols() {
		return additional_protocols;
	}

	public void setAdditional_protocols(String additional_protocols) {
		this.additional_protocols = additional_protocols;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
}
