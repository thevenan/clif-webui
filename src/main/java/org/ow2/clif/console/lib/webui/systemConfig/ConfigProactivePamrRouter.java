package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pamr.router"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pamr.router")
public class ConfigProactivePamrRouter {
	private String address = null;
	private String port = null;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	
}
