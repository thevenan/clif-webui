package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.net"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.net")
public class ConfigProactiveNet {
	
	private String disableIPv6 = null;
	private String nolocal = null;
	private String netmask = null;
	private String noprivate = null;

	
	public String getDisableIPv6() {
		return disableIPv6;
	}


	public void setDisableIPv6(String disableIPv6) {
		this.disableIPv6 = disableIPv6;
	}


	public String getNolocal() {
		return nolocal;
	}


	public void setNolocal(String nolocal) {
		this.nolocal = nolocal;
	}


	public String getNetmask() {
		return netmask;
	}


	public void setNetmask(String netmask) {
		this.netmask = netmask;
	}


	public String getNoprivate() {
		return noprivate;
	}


	public void setNoprivate(String noprivate) {
		this.noprivate = noprivate;
	}


	public void configProactive() {
		
	}

}
