/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin, Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.ArrayUtils;
import org.ow2.clif.console.lib.webui.util;
import org.ow2.clif.console.lib.webui.auth.dao.DataAccessImpl;
import org.ow2.clif.console.lib.webui.storage.Exceptions.StorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.xml.sax.SAXException;

/**
*
* @author Tim Martin
* @author Antoine Thevenet
*/

@Service
public class StorageHandler {

    
    private final DataAccessImpl dataAccessImpl;
    private final boolean securityEnabled;

    private final FileSystemStorage storageService;
    Logger logger = (Logger) LoggerFactory.getLogger(StorageHandler.class);


	@Autowired
	public StorageHandler(StorageProperties properties, DataAccessImpl dataAccessImpl) {
        this.dataAccessImpl = dataAccessImpl;
        this.securityEnabled = properties.securityEnabled().contains("true");
        this.storageService = new FileSystemStorage(properties.getLocation());
	}

    
    public boolean canEditFile(String projectname){
		if(!this.securityEnabled) return true;
		String username = util.getUserLogged();
		if(projectname.contains("/")){
			return util.canEditFile(dataAccessImpl.getPermission(username, projectname));
		}else{
			return dataAccessImpl.getPermission(username, projectname).contains("admin");
		}
    }    	

    public String saveFile(String fileName, String fileContent) throws FileNotFoundException {
        if(!this.canEditFile(util.getProjectName(fileName))){
            logger.info("User "+util.getUserLogged()+"Unauthorized to save "+util.getProjectName(fileName));
            return util.returnMessage("error.unauthorized",null);
        }
		String args[] = {fileName.substring(fileName.lastIndexOf("/")+1)};
		logger.info("Saving file "+fileName);
        storageService.saveFile(fileName, fileContent);
        return util.returnMessage("action.save",args);
    }
    
    public String importFile(String fileName, String fileContent) {		
		if(!canEditFile(util.getProjectName(fileName))){
			logger.info("User "+util.getUserLogged()+"Unauthorized to upload "+util.getProjectName(fileName));
			return util.returnMessage("error.unauthorized",null);
		}    	
		storageService.store(fileName,fileContent);
        logger.info("Uploading file "+fileName);
		String args[] = {fileName.substring(fileName.lastIndexOf("/")+1)};
     	return util.returnMessage("action.fileimport",args);
	}

    public ArrayList<String> importProject(byte[] data) throws IOException {
        ArrayList<String> newProjects = storageService.importUnzipProject(data);
        for(String p : newProjects){
            p = p.replaceAll("/","");
            if(!util.isJUnitTest() && securityEnabled){ // Creating project in DB
                dataAccessImpl.createProject(util.getUserLogged(), p);            
            }			
        }
        return newProjects;
	}

	public String createFile(String file) {		
        if(!canEditFile(util.getProjectName(file)) && (file!=util.getProjectName(file))){
            logger.info("User "+util.getUserLogged()+"Unauthorized to create in "+util.getProjectName(file));
            return util.returnMessage("error.unauthorized",null);
        }        
        logger.info("Creating file "+file);
        storageService.createFile(file);
        String args[] = {file.substring(file.lastIndexOf("/")+1)};
        return util.returnMessage("action.create",args);
    }
	
    public String createDirectory(String path){
        if(path.contains("/")){ //not creating a project
            if(!canEditFile(util.getProjectName(path)) && (path!=util.getProjectName(path))){
                logger.info("User "+util.getUserLogged()+"Unauthorized to create in "+util.getProjectName(path));
                return util.returnMessage("error.unauthorized",null);
            }     
        }
        logger.info("Creating directory "+path);
        storageService.createDirectory(path);
        if(!path.contains("/") && !util.isJUnitTest() && securityEnabled){ // Creating project in DB
            dataAccessImpl.createProject(util.getUserLogged(), path);            
        }			
    
        String args[] = {path.substring(path.lastIndexOf("/")+1)};
        return util.returnMessage("action.create",args);
    }

	public String rename(String path, String newName) {
        if(!canEditFile(util.getProjectName(path))){
            logger.info("User "+util.getUserLogged()+"Unauthorized to rename "+util.getProjectName(path));
            return util.returnMessage("error.unauthorized",null);
        }
        logger.info("Renaming "+path+" to "+newName);

        storageService.rename(newName,path);
        if(!path.contains("/") && !util.isJUnitTest() && securityEnabled){  // Renaming project in DB
            dataAccessImpl.renameProject(newName,path);				
        }	
        String args[] = {path.substring(path.lastIndexOf("/")+1),newName};
        return util.returnMessage("action.rename",args);
    }
    
	
    public String move(String file, String path) {
        if(!canEditFile(util.getProjectName(path))){
            logger.info("User "+util.getUserLogged()+"Unauthorized to rename "+util.getProjectName(path));
            return util.returnMessage("error.unauthorized",null);
        }
        logger.info("Moving "+file+" to "+path);
        storageService.move(file,path);
        
        String args[] = {path.substring(path.lastIndexOf("/")+1),path};
        return util.returnMessage("action.move",args);
	}

	public String delete(String file) {
		
		if(!canEditFile(util.getProjectName(file))){
			logger.info("Unauthorized to delet file "+file);
			return util.returnMessage("error.unauthorized",null);
		}
		storageService.delete(file);
        if(!file.contains("/") && !util.isJUnitTest() && securityEnabled){ // Deleting project in DB
            dataAccessImpl.deleteProject(file);				
        }	
        logger.info("Deleted file "+file);
		String args[] = {file.substring(file.lastIndexOf("/")+1)};
        return util.returnMessage("action.delete",args);
         
	}


	public String loadContent(String filename) {
        logger.info("Displaying file "+filename);
    	return storageService.loadContent(filename);
	}


	public Model listUploadedFiles(Model model) throws IOException {
		String username = util.getUserLogged();
		if(username==""){
			return null;
		}
		logger.info("Connected as : "+username);
		model.addAttribute("username",username);
        logger.info("Getting uploaded files...");
        model.addAttribute("files", storageService.loadAll().map(path -> path.toString().toString()).collect(Collectors.toList()));		
        if(securityEnabled)
            model.addAttribute("projects", dataAccessImpl.getAccessibleProjects(username));		
        else
            model.addAttribute("projects", storageService.loadProjects());		
		model.addAttribute("directories", storageService.loadDirectory().map(path -> path.toString().toString()).collect(Collectors.toList()));	
        logger.info("Getting user permissions...");
		model.addAttribute("permissions", dataAccessImpl.getAccessUser(util.getUserLogged()));		
        model.addAttribute("secutriyEnabled",securityEnabled);
        return model;
	}


	public Object[] getAccessiblesFiles() {
        String username = util.getUserLogged();
		if(username=="" && securityEnabled){
			return null;
		}
        return storageService.loadAll().map(path -> path.toString().toString()).toArray();
	}


	public Object[] getAccessiblesDirs() {
        String username = util.getUserLogged();
		if(username=="" && securityEnabled){
			return null;
		}
        return storageService.loadDirectory().map(path -> path.toString().toString()).toArray();
	}


    public InputStream getZipProject(String projectName) throws IOException {
        return storageService.exportZipProject(projectName);
    }
    
    public String createFileWithContent(String file) {		
        if(!canEditFile(util.getProjectName(file)) && (file!=util.getProjectName(file))){
            logger.info("User "+util.getUserLogged()+"Unauthorized to create in "+util.getProjectName(file));
            return util.returnMessage("error.unauthorized",null);
        }        
        logger.info("Creating file "+file);
        storageService.createFileWithContent(file);
        String args[] = {file.substring(file.lastIndexOf("/")+1)};
        return storageService.loadContent(file);
    }
    
    public String duplicateFile(String duplicatedFileName, String originalFileName) throws IOException {		
        if(!canEditFile(util.getProjectName(duplicatedFileName)) && (duplicatedFileName!=util.getProjectName(duplicatedFileName))){
            logger.info("User "+util.getUserLogged()+"Unauthorized to create in "+util.getProjectName(duplicatedFileName));
            return util.returnMessage("error.unauthorized",null);
        }        
        logger.info("Creating file "+duplicatedFileName);
        storageService.duplicateFile(duplicatedFileName, originalFileName);
        return storageService.loadContent(duplicatedFileName);
    }
    
    /**
     * This function is called when the web page is opened in order to build the cache
     * @param resourceToAccess
     * @return
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
	public String buildCache() throws IOException, ParserConfigurationException, SAXException{
		return storageService.initCache();
	}
	
	/**
     * This function is called when the web page is opened in order to build the cache
     * @param resourceToAccess
     * @return
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
	public String buildCacheProbe() throws IOException, ParserConfigurationException, SAXException{
		return storageService.initCacheProbe();
	}

	/*public void addZippedFolder(String path) {
        ImportZipFolder.put(util.getUserLogged(),path);
        logger.info("creating folder space key : "+util.getUserLogged()+" value : "+path);
	}
    */
}
