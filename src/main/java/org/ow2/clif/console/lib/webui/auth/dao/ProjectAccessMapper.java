/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.ow2.clif.console.lib.webui.auth.model.Permission;
import org.ow2.clif.console.lib.webui.auth.model.ProjectAccess;

import org.springframework.jdbc.core.RowMapper;

public class ProjectAccessMapper implements RowMapper<ProjectAccess> {

    @Override
    public ProjectAccess mapRow(ResultSet rs, int rowNum) throws SQLException {
        String tmp = rs.getString("permission");
        Permission permission;
        if(tmp.contains("admin")){
            permission = Permission.admin;
        }else if(tmp.contains("editor")){
            permission = Permission.editor;
        }else if(tmp.contains("tester")){
            permission = Permission.tester;
        }else{
            permission = Permission.viewer;
        }
        ProjectAccess projAcc = new ProjectAccess(rs.getString("username"),rs.getString("projectName"),permission);
        return projAcc;
    }
}