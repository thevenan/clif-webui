/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.controller;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.ow2.clif.console.lib.webui.clifTest.TestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ClifTestController {
	
	@Autowired
	private TestHandler testHandler;
	
	
	@RequestMapping(value = "/checkServers", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> checkTest(@RequestBody String file, RedirectAttributes redirectAttributes) {
		try {
			String response = testHandler.checkServers(file);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/deployTest", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleTestDeploy(@RequestBody String file, RedirectAttributes redirectAttributes) {
		try {
			String response = testHandler.deployTest(file);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/initTest", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleTestInit(@RequestBody String testRunId, RedirectAttributes redirectAttributes) {
		try {
			String response = testHandler.initTest(testRunId);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/startTest", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> handleTestStart(RedirectAttributes redirectAttributes) {
		try {
			String response = testHandler.startTest();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/suspendTest", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> handleSuspendStart(RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = testHandler.suspendTest();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/stopTest", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> handleStopTest(RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = testHandler.stopTest();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/resumeTest", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> handleResumeTest(RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = testHandler.resumeTest();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/getMetrics", method = RequestMethod.GET)
	@ResponseBody
	public String[][] handleGetMetrics(RedirectAttributes redirectAttributes) throws IOException {
		return testHandler.getMetrics();
	}

	@RequestMapping(value = "/collectTest", method = RequestMethod.POST)
	@ResponseBody
	public String handleCollectTest(@RequestBody String generateQuickstat, RedirectAttributes redirectAttributes)
			throws IOException {
		return testHandler.collectTest(generateQuickstat);
	}

	/**
	 * This class checks if a given index already exists in the ElasticSearch
	 * database
	 * 
	 * @param fileLocation       a string in JSON format which contains the partial
	 *                           urls of the files whose data we want to send to
	 *                           ElasticSearch
	 * @param redirectAttributes
	 * @return a JSON string indicating if a given index already exists in
	 *         ElasticSearch
	 */
	@RequestMapping(value = "/checkIndexElasticSearch", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleSendData(@RequestBody String fileLocation,
			RedirectAttributes redirectAttributes) {
		try {
			// parse the json argument to an arrayList of String
			JSONArray jsonFilePath = new JSONArray(fileLocation);
			ArrayList<String> partialFilePathArray = new ArrayList<String>();
			for (int i = 0; i < jsonFilePath.length(); i++) {
				partialFilePathArray.add(jsonFilePath.getString(i));
			}
			ArrayList<String> indexArray = new ArrayList<String>();
			ArrayList<Long> originArray = new ArrayList<Long>();
			ArrayList<String> filePathArray = new ArrayList<String>();
			ArrayList<String> bladeIdArray = new ArrayList<String>();
			ArrayList<String> fileSizeArray = new ArrayList<String>();
			boolean indexExist = false;
			String returnJsonArray = "{\"response\": [";

			for (int i = 0; i < partialFilePathArray.size(); i++) {
				// retrieve the data from the prepareTestData function
				ArrayList<Object> bulkParameters = testHandler.prepareTestData(partialFilePathArray.get(i));
				indexArray.add(String.valueOf(bulkParameters.get(0)));
				filePathArray.add(String.valueOf(bulkParameters.get(1)));
				originArray.add(Long.parseLong(String.valueOf(bulkParameters.get(2))));
				bladeIdArray.add(String.valueOf(bulkParameters.get(3)));
				fileSizeArray.add(String.valueOf(bulkParameters.get(4)));
				indexExist = testHandler.checkIndexExist(indexArray.get(i));
				// we build the json response
				returnJsonArray += "{\"index\":\"" + indexArray.get(i) + "\",";
				returnJsonArray += "\"filepath\":\"" + partialFilePathArray.get(i) + "\",";
				returnJsonArray += "\"fileSize\":\"" + fileSizeArray.get(i) + "\",";
				if (indexExist == true) {
					returnJsonArray += "\"indexExist\":" + "\"true\"" + "},";
				} else {
					returnJsonArray += "\"indexExist\":" + "\"false\"" + "},";
				}
			}
			// close the {} of the json response
			returnJsonArray = returnJsonArray.substring(0, returnJsonArray.length() - 1);
			returnJsonArray += "]}";
			return new ResponseEntity<>(returnJsonArray, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This class sends data to ElasticSearch
	 * 
	 * @param fileLocation       a string in JSON format which contains the partial
	 *                           urls of the files whose data we want to send to
	 *                           ElasticSearch
	 * @param redirectAttributes
	 * @return a JSON string giving a summary of the operations performed with
	 *         ElasticSearch
	 */
	@RequestMapping(value = "/replaceElasticData", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleReplaceElasticData(@RequestBody String fileLocation,
			RedirectAttributes redirectAttributes) {
		try {
			Logger logger = (Logger) LoggerFactory.getLogger(ClifTestController.class);
			// parse the json argument to an arrayList of String
			JSONArray jsonFilePath = new JSONArray(fileLocation);
			ArrayList<String> partialFilePathArray = new ArrayList<String>();
			for (int i = 0; i < jsonFilePath.length(); i++) {
				partialFilePathArray.add(jsonFilePath.getString(i));
			}
			ArrayList<String> indexArray = new ArrayList<String>();
			ArrayList<Long> originArray = new ArrayList<Long>();
			ArrayList<String> filePathArray = new ArrayList<String>();
			ArrayList<String> bladeIdArray = new ArrayList<String>();
			ArrayList<String> fileSizeArray = new ArrayList<String>();
			String returnJsonArray = "{\"response\": [";

			// we gather the parameters to build the bulk
			for (int i = 0; i < partialFilePathArray.size(); i++) {
				ArrayList<Object> bulkParameters = testHandler.prepareTestData(partialFilePathArray.get(i));
				indexArray.add(String.valueOf(bulkParameters.get(0)));
				filePathArray.add(String.valueOf(bulkParameters.get(1)));
				originArray.add(Long.parseLong(String.valueOf(bulkParameters.get(2))));
				bladeIdArray.add(String.valueOf(bulkParameters.get(3)));
				fileSizeArray.add(String.valueOf(bulkParameters.get(4)));

				// we build the json response
				returnJsonArray += "{\"index\":\"" + indexArray.get(i) + "\",";
				returnJsonArray += "\"fileSize\":\"" + fileSizeArray.get(i) + "\",";
				returnJsonArray += "\"filepath\":\"" + partialFilePathArray.get(i) + "\"},";
			}
			// We send all data directly
			for (int i = 0; i < indexArray.size(); i++) {
				String className = testHandler.getClassName(filePathArray.get(i));
				String[] labels = testHandler.obtainLabels(bladeIdArray.get(i), className);
				testHandler.handleBulk(indexArray.get(i), originArray.get(i), filePathArray.get(i), labels, className);
				logger.info("Sent " + partialFilePathArray.get(i) + " to ElasticSearch");
			}
			returnJsonArray = returnJsonArray.substring(0, returnJsonArray.length() - 1);
			returnJsonArray += "]}";

			return new ResponseEntity<>(returnJsonArray, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
