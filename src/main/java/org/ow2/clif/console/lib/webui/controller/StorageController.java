/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin, Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.webui.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Enumeration;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.apache.tomcat.util.json.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.ow2.clif.console.lib.webui.util;
import org.ow2.clif.console.lib.webui.storage.Exceptions.*;
import org.ow2.clif.console.lib.webui.storage.StorageHandler;

/**
 *
 * @author Tim Martin
 * @author Antoine Thevenet
 */

@Controller
public class StorageController {

	private final StorageHandler storageHandler;
	Logger logger = (Logger) LoggerFactory.getLogger(StorageController.class);

	@Autowired
	public StorageController(StorageHandler storageHandler) {
		this.storageHandler = storageHandler;
	}

	@RequestMapping("/")
	public String mapEditor() {
		return "redirect:/login";
	}

	@GetMapping("/clifwebui")
	public String listUploadedFiles(Model model) throws IOException {
		model = storageHandler.listUploadedFiles(model);
		if (model == null) {
			return "login";
		}
		return "clifwebui";
	}

	@RequestMapping(value = "/reloadFiles", method = RequestMethod.GET)
	@ResponseBody
	public Object[] reloadFiles() throws IOException {
		return storageHandler.getAccessiblesFiles();
	}

	@RequestMapping(value = "/reloadDirs", method = RequestMethod.GET)
	@ResponseBody
	public Object[] reloadDirs() throws IOException {
		return storageHandler.getAccessiblesDirs();
	}

	@RequestMapping(value = "/getFileContent", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> displayFile(@RequestBody String filename) {
		try {
			String response = storageHandler.loadContent(filename);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/saveFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveFile(@RequestBody String file) {
		try {
			String[] fileContent = util.getArgsFile(file);
			String response = storageHandler.saveFile(fileContent[0], fileContent[1]);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/importFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleFileUpload(@RequestBody String file) {
		try {
			String[] fileContent = util.getArgsFile(file);
			String response = storageHandler.importFile(fileContent[0], fileContent[1]);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/importProject", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ArrayList<String>> handleProjectImport(@RequestBody byte[] data) {
		try {
			ArrayList<String> response = storageHandler.importProject(data);
			logger.info("Response: " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info(e.toString());
			ArrayList<String> response = new ArrayList<>();
			response.add(e.getLocalizedMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/exportProject/{projectName}", method = RequestMethod.GET)
	public void handleExportImport(@PathVariable("projectName") String projectName, HttpServletResponse response) {
		try {
			InputStream is = storageHandler.getZipProject(projectName);
			response.setContentType("application/zip");
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			logger.info("Error writing file to output stream for ", projectName, ex);
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

	@RequestMapping(value = "/createFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleFileCreate(@RequestBody String file, RedirectAttributes redirectAttributes)
			throws IOException {
		try {
			String response = storageHandler.createFile(file);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/createFileWithContent", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleFileCreateWithContent(@RequestBody String fileWithContent,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = storageHandler.createFileWithContent(fileWithContent);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/duplicateFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleDuplicateFile(@RequestBody String file, RedirectAttributes redirectAttributes)
			throws IOException {
		try {
			String duplicatedFileName = file.substring(0, file.indexOf("\n"));
			String originalFileName = file.substring(file.indexOf("\n") + 1);
			String response = storageHandler.duplicateFile(duplicatedFileName, originalFileName);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/createDirectory", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleDirectoryCreate(@RequestBody String path, RedirectAttributes redirectAttributes)
			throws IOException {
		try {
			String response = storageHandler.createDirectory(path);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleRename(@RequestBody String nameThenNewName,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String path = nameThenNewName.substring(0, nameThenNewName.indexOf("\n"));
			String newName = nameThenNewName.substring(nameThenNewName.indexOf("\n") + 1);
			String response = storageHandler.rename(path, newName);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/movefile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleMove(@RequestBody String nameThenNewName, RedirectAttributes redirectAttributes)
			throws IOException {
		// try{
		String file = nameThenNewName.substring(0, nameThenNewName.indexOf("\n"));
		String destPath = nameThenNewName.substring(nameThenNewName.indexOf("\n") + 1);
		String response = storageHandler.move(file, destPath);
		return new ResponseEntity<>(response, HttpStatus.OK);
		/*
		 * }catch(Exception e){ return new ResponseEntity<>(e.getMessage(),
		 * HttpStatus.INTERNAL_SERVER_ERROR); }
		 */
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleDelete(@RequestBody String file) {
		try {
			String response = storageHandler.delete(file);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@RequestMapping(value = "/getLocalizedTexts", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> sendLocalizedTexts() {
		HashMap<String, String> locales = new HashMap<String, String>();
		ResourceBundle messages = ResourceBundle.getBundle("lang/res", LocaleContextHolder.getLocale());
		Enumeration<String> keys = messages.getKeys();
		while (keys.hasMoreElements()) {
			String param = keys.nextElement();
			locales.put(param, messages.getString(param));
		}
		return locales;
	}

	@RequestMapping(value = "/buildCache", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> buildCache(@RequestBody String resourceToAccess,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = storageHandler.buildCache();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/buildCacheProbe", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleCacheProbe(@RequestBody String resourceToAccess,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = storageHandler.buildCacheProbe();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
