/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.clifTest;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import java.lang.ProcessBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.supervisor.api.ClifException;

import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.util.ThrowableHelper;
import org.ow2.clif.console.lib.webui.storage.StorageProperties;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactive;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactiveCommunication;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactiveCommunicationBenchmark;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactiveCommunicationProtocols;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactiveNet;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePamr;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePamrAgent;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePamrRouter;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePamrssh;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePnp;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePnps;
import org.ow2.clif.console.lib.webui.systemConfig.ConfigProactivePnpsExtended;
import org.ow2.clif.console.lib.webui.systemConfig.SetProactive;
import org.ow2.clif.console.lib.webui.util;
import org.ow2.clif.console.lib.webui.elasticSearch.MappingElasticSearch;
import org.ow2.clif.supervisor.api.BladeState;

@Service
public class TestHandler {

	private final Path rootLocation;

	static volatile boolean deployOK = true;

	private Map<String, Process> runningProcess;

	private ClifRegistry registry;

	private final String SuccessInit = "Test plan %1 initialized successfully.";

	private String[] bladeIds;
	private String[] serverName;
	private String planTestFileName;
	private String resultFolderName;
	private ClifAppFacade clifApp;

	public String[] obtainBladesId() {
		return bladeIds;
	}

	public void setBladesId(String file) {
		bladeIds = getInjectorsId(file);
	}

	public String getPlanTestFileName() {
		return planTestFileName;
	}

	public void setPlanTestFileName(String planTestFileName) {
		this.planTestFileName = planTestFileName;
	}

	public String getResultFolderName() {
		return resultFolderName;
	}

	public void setResultFolderName(String resultFolderName) {
		this.resultFolderName = resultFolderName;
	}

	public String[] getServerName() {
		return serverName;
	}

	public void setServerName(String[] serverName) {
		this.serverName = serverName;
	}

	public ClifAppFacade getClifApp() {
		return clifApp;
	}

	public void setClifApp(ClifAppFacade clifApp) {
		this.clifApp = clifApp;
	}
	
	//We load system properties
	@Autowired
	private ConfigProactive configProactive;
	@Autowired
	private ConfigProactiveCommunication configProactiveCommunication;
	@Autowired
	private ConfigProactiveCommunicationBenchmark configProactiveCommunicationBenchmark;
	@Autowired
	private ConfigProactiveCommunicationProtocols configProactiveCommunicationProtocols;
	@Autowired
	private ConfigProactiveNet configProactiveNet;
	@Autowired
	private ConfigProactivePamr configProactivePamr;
	@Autowired
	private ConfigProactivePamrAgent configProactivePamrAgent;
	@Autowired
	private ConfigProactivePamrRouter configProactivePamrRouter;
	@Autowired
	private ConfigProactivePamrssh configProactivePamrssh;
	@Autowired
	private ConfigProactivePnp configProactivePnp;
	@Autowired
	private ConfigProactivePnps configProactivePnps;
	@Autowired
	private ConfigProactivePnpsExtended configProactivePnpsExtended;
	
	/**
	 * Before launching CLIF registry, we launch each system property linked to CLIF proactive
	 * @throws Exception
	 */
	@PostConstruct
	public void setSystemProperties() throws Exception {
		SetProactive setProactive = new SetProactive();
		setProactive.setProactive(configProactive);
		setProactive.setProactiveCommunication(configProactiveCommunication);
		setProactive.setProactiveCommunicationExtended(configProactiveCommunicationBenchmark,
				configProactiveCommunicationProtocols);
		setProactive.setProactiveNet(configProactiveNet);
		setProactive.setProactivePamr(configProactivePamr);
		setProactive.setProactivePamrAgent(configProactivePamrAgent);
		setProactive.setProactivePamrRouter(configProactivePamrRouter);
		setProactive.setProactivePamrssh(configProactivePamrssh);
		setProactive.setProactivePnp(configProactivePnp);
		setProactive.setProactivePnps(configProactivePnps);
		setProactive.setProactivePnpsExtended(configProactivePnpsExtended);
		System.setProperty("fractal.provider", "org.objectweb.proactive.core.component.Fractive");
		try {
			logger.info("Creating a CLIF Registry...");
			this.registry = ClifRegistry.getInstance(true);
			logger.info("Created registry " + this.registry);
		} catch (Exception ex) {
			logger.info("Failed. Trying to connect to an existing CLIF Registry...");
			this.registry = ClifRegistry.getInstance(false);
			logger.info("Connected to " + this.registry);
		}

	}

	@Autowired
	public TestHandler(StorageProperties properties) throws Exception {
		this.rootLocation = Paths.get(properties.getLocation());
		this.runningProcess = new HashMap<>();
	}

	Logger logger = (Logger) LoggerFactory.getLogger(TestHandler.class);

	private ClifAppFacade getClifAppFacade(String file) {
		logger.debug("Searching test with name: " + file);
		return new ClifAppFacade(this.registry.lookupClifApp(util.getFileName(file)), util.getFileName(file));
	}

	private synchronized String[] getBladesId(String file) {
		try {
			InputStream is = new FileInputStream(new File(rootLocation.toString() + "/" + file));
			Map<String, ClifDeployDefinition> testPlanProject = TestPlanReader.readFromProp(is);
			Set<String> keys = testPlanProject.keySet();
			return keys.toArray(new String[keys.size()]);
		} catch (Exception e) {
			throw new ClifHandlerException("error.testplan.deployed", e);
		}
	}

	private synchronized String[] getInjectorsId(String file) {
		try {
			InputStream is = new FileInputStream(new File(rootLocation.toString() + "/" + file));
			Map<String, ClifDeployDefinition> testPlanProject = TestPlanReader.readFromProp(is);
			Set<String> injectors = new HashSet();
			Set<String> serverName = new HashSet();

			for (String i : testPlanProject.keySet()) {
				serverName.add((testPlanProject.get(i).getServerName()));
				System.out.println(testPlanProject.get(i).getServerName());
			}
			setServerName(serverName.toArray(new String[serverName.size()]));

			for (String i : testPlanProject.keySet()) {
				if (!testPlanProject.get(i).isProbe())
					injectors.add(i);
			}
			return injectors.toArray(new String[injectors.size()]);
		} catch (Exception e) {
			throw new ClifHandlerException("error.testplan.deployed", e);
		}
	}

	public String readOutput(Process p) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			logger.info(line);
			sb.append(line + "\n");
			if (line != null && line.contains("is deployed")) {
				br.close();
				return sb.toString();
			}
			if (line != null && line.contains("operation failed")) {
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
					logger.info(line);
				}
				br.close();
				return "Error: " + sb.toString();
			}
		}
		br.close();
		return sb.toString();
	}

	/**
	 * This function checks the server that are registered in the registry. It
	 * compares them to the servers mentioned in the ctp file
	 * 
	 * @param file
	 * @return
	 */
	public String checkServers(String file) {
		setBladesId(file);
		setPlanTestFileName(file);

		// retrieve the servers existing in the registry and mentioned in the ctp file
		String[] registryServers = registry.getServers();
		String[] ctpServers = getServerName();
		// Complete the JSON response
		String responseRegistry = "\"registryServers\":[";
		String responseMissing = "\"missingServers\":[";
		String registryInfo = "\"registry\":" + "\"" + registry.toString() + "\"";
		// complete the JSON about registry servers
		for (int i = 0; i < registryServers.length; i++) {
			responseRegistry += "\"" + registryServers[i] + "\"" + ",";
		}
		if (registryServers.length >= 1) {
			responseRegistry = responseRegistry.substring(0, responseRegistry.length() - 1);
		}
		responseRegistry += "]";

		// complete the JSON about the servers mentioned in the ctp file
		for (int i = 0; i < ctpServers.length; i++) {
			boolean serverExist = false;
			for (int j = 0; j < registryServers.length; j++) {
				if (ctpServers[i].equals(registryServers[j])) {
					serverExist = true;
				}
			}
			if (serverExist == false) {
				responseMissing += "\"" + ctpServers[i] + "\"" + ",";
			}
		}
		if (responseMissing.charAt(responseMissing.length() - 1) == ',') {
			responseMissing = responseMissing.substring(0, responseMissing.length() - 1);
		}
		responseMissing += "]";
		String response = "{" + responseRegistry + "," + responseMissing + "," + registryInfo + "}";
		return response;

	}

	/**
	 * This function launches the clif command clifcmd deploy. It also set up some
	 * variables that will be used by the other commands
	 * 
	 * @param file : the .ctp file name
	 * @return
	 * @throws IOException
	 * @throws ClifException
	 */
	public String deployTest(String file) throws IOException, ClifException {
		ProcessBuilder pb = new ProcessBuilder("clifcmd", "deploy", util.getFileName(file),
				this.rootLocation + "/" + file);
		logger.info("Deploying " + file + " with " + util.getFileName(file));
		pb.directory(new File(this.rootLocation + "/" + file.substring(0, file.lastIndexOf("/"))));
		Process process = pb.start();
		runningProcess.put(file, process);

		// set up some variables to avoid using post methods

		setBladesId(file);
		setPlanTestFileName(file);

		return readOutput(process);
	}

	public String initTest(String testRunId) {
		String testPlanName = getPlanTestFileName();
		setResultFolderName(testRunId);
		logger.info("Initializing test plan " + testRunId);
		try {
			ClifAppFacade clifApp = getClifAppFacade(testPlanName);
			if (clifApp == null) {
				throw new ClifHandlerException("error.testplan.deployed");
			}
			setClifApp(clifApp);
			logger.info("Test plan found");
			clifApp.init(testRunId);
			int res = clifApp.waitForState(null, BladeState.INITIALIZED);
			if (res == 0) {
				logger.info("Initialized");
				return SuccessInit.replace("%1", testPlanName);
			}
		} catch (Exception ex) {
			logger.info("Initialization failed:\n" + ThrowableHelper.getStackTrace(ex));
			if (ex instanceof ClifException && ex.getCause() == null) {
				throw new ClifHandlerException("error.init.failed", ex);
			}
		}
		throw new ClifHandlerException("error.init.failed");
	}

	public String startTest() throws IOException {
		String[] bladeIds = obtainBladesId();
		String testPlanName = getPlanTestFileName();
		try {
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null) {
				throw new ClifHandlerException("error.testplan.deployed");
			}
			logger.info("Starting " + testPlanName + " test plan...");
			int res = clifApp.start(bladeIds);
			if (res == 0) {
				logger.debug("Blades states before waiting : " + clifApp.getGlobalState(bladeIds).toString());
				res = clifApp.waitForState(bladeIds, BladeState.RUNNING);
				logger.debug("Blades states after waiting : " + clifApp.getGlobalState(bladeIds).toString());
				if (res == 0) {
					logger.info("Test started");
					return "Test started";
				}
			}
			throw new ClifHandlerException("error.init.blades");
		} catch (Exception ex) {
			throw new ClifHandlerException("error.execution.starting", ex);
		}
	}

	public String suspendTest() throws IOException {
		String[] bladeIds = obtainBladesId();
		logger.info("Suspending test...");
		String testPlanName = getPlanTestFileName();
		ClifAppFacade clifApp = getClifApp();

		logger.info("Blades states before suspend: " + clifApp.getGlobalState(bladeIds).toString());

		try {
			// logger.info("Searching for deployed test...");
			int res = clifApp.suspend(bladeIds);
			// logger.info("Blades states after suspend:
			// "+this.clifApp.getGlobalState(bladeIds).toString());
			logger.info("Result : " + res);
			if (res == 0) {
				// logger.info("Blades states before waiting:
				// "+this.clifApp.getGlobalState(bladeIds).toString());
				res = clifApp.waitForState(bladeIds, BladeState.SUSPENDED);
				// logger.info("Blades states after waiting:
				// "+this.clifApp.getGlobalState(bladeIds).toString());

			}
			if (res < 0) {
				throw new ClifHandlerException("error.blades.notrunning");
			}
			logger.info("Test Suspended");
			return "Test Suspended";
		} catch (Exception ex) {
			throw new ClifHandlerException("error.execution.suspending", ex);
		}
	}

	public String stopTest() {
		logger.info("Stopping test...");
		String[] bladeIds = obtainBladesId();
		String testPlanName = getPlanTestFileName();
		try {
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null) {
				throw new ClifHandlerException("error.testplan.deployed");
			}
			if (clifApp.getGlobalState(bladeIds) != BladeState.COMPLETED) {
				logger.debug("Blades states before stopping test : " + clifApp.getGlobalState(bladeIds).toString());
				int res = clifApp.stop(bladeIds);
				logger.debug("Blades states after stopping test : " + clifApp.getGlobalState(bladeIds).toString());

				if (res == 0) {
					res = clifApp.waitForState(bladeIds, BladeState.STOPPED);
					if (res == 0) {
						Process p = this.runningProcess.get(testPlanName);
						if (p != null)
							p.destroy();
						this.runningProcess.remove(testPlanName);
						logger.info("Test stopped");
						return "Test stopped";
					}
				}
				throw new ClifHandlerException("error.blade.unstoppable");
			} else {
				Process p = this.runningProcess.get(testPlanName);
				p.destroy();
				this.runningProcess.remove(testPlanName);
				logger.info("Test process stopped");
				return "Test stopped";
			}
		} catch (Exception ex) {
			throw new ClifHandlerException("error.execution.stopping", ex);
		}

	}

	public String resumeTest() {
		String testPlanName = getPlanTestFileName();
		String[] bladeIds = obtainBladesId();
		try {
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null) {
				throw new ClifHandlerException("error.testplan.deployed");
			}
			logger.info("Resuming " + testPlanName + " test plan...");
			logger.info("Blades states before resuming : " + clifApp.getGlobalState(bladeIds).toString());
			int res = clifApp.resume(bladeIds);
			if (res == 0) {
				res = clifApp.waitForState(bladeIds, BladeState.RUNNING);
				logger.info("Blades states after waiting : " + clifApp.getGlobalState(bladeIds).toString());
				if (res == 0) {
					logger.info("Test Resumed");
					return "Test Resumed";
				}
			}
			throw new ClifHandlerException("error.blade.unresumable");
		} catch (Exception ex) {
			throw new ClifHandlerException("error.suspend.blades", ex);
		}
	}

	public String[][] getMetrics() {
		String[] bladeIds = obtainBladesId();
		String[][] stats = new String[bladeIds.length][11];
		ClifAppFacade clifApp = getClifApp();
		if (clifApp == null) {
			throw new ClifHandlerException("error.testplan.deployed");
		}
		String state = clifApp.getGlobalState(bladeIds).toString();
		if (state == "completed") {
			logger.info("Test completed");
			return null;
		}

		for (int i = 0; i < bladeIds.length; i++) {
			stats[i][0] = "Blade " + bladeIds[i];
			long[] tmp = clifApp.getStats(bladeIds[i]);
			for (int j = 0; j < tmp.length; j++) {
				stats[i][j + 1] = String.valueOf(tmp[j]);
			}
		}
		return stats;
	}

	/**
	 * This function retrieves the various servers. It also generates the quickstats
	 * file
	 * 
	 * @param generateQuickstat
	 * @return
	 * @throws IOException
	 */
	public String collectTest(String generateQuickstat) throws IOException {
		String[] bladeIds = obtainBladesId();
		String testPlanName = getPlanTestFileName();
		String[] splitName = testPlanName.split("/");

		try {
			String quickstatContent = "";
			ClifAppFacade clifApp = getClifAppFacade(testPlanName);
			logger.info("Waiting for test termination...");
			clifApp.join(bladeIds);
			logger.info("Collecting data from test plan " + testPlanName + "...");
			int res = clifApp.collect(bladeIds, null);
			if (res < 0) {
				throw new ClifHandlerException("error.blade.uncollectable");
			}
			if (generateQuickstat.equals("generateQuickstat")) {
				// Generate the quickstat file
				// The process builder executes the quickstat command
				ProcessBuilder pb = new ProcessBuilder("clifcmd", "quickstats",
						this.rootLocation + "/" + splitName[0] + "/report");
				logger.info("Generating quickstat report");
				pb.directory(new File(this.rootLocation + "/" + splitName[0] + "/report"));
				Process process = pb.start();

				// We retrieve the quickstat command standard output
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				StringBuilder stringBuilder = new StringBuilder();
				int c = bufferedReader.read();

				while (c != -1) {
					stringBuilder.append(String.valueOf((char) c));
					c = bufferedReader.read();
				}
				quickstatContent = stringBuilder.toString();
				if (quickstatContent.indexOf("Quick statistics report done") != -1) {
					quickstatContent = quickstatContent.substring(0,
							quickstatContent.indexOf("Quick statistics report done"));
				}
//				//delete useless space returned by quickstats
				String[] splitedQuickstat = quickstatContent.split("\t");
				for (int i = 0; i < splitedQuickstat.length; i++) {
					while (splitedQuickstat[i].charAt(0) == ' ') {
						splitedQuickstat[i] = splitedQuickstat[i].substring(1);
					}
				}
				quickstatContent = "";
				for (int i = 0; i < splitedQuickstat.length; i++) {
					quickstatContent += splitedQuickstat[i] + "\t";
				}
				quickstatContent = quickstatContent.substring(0, quickstatContent.length() - 1);
				// We choose the file name

				String folderName = retrieveLastFolderName(this.rootLocation + "/" + splitName[0] + "/report");

				// We create a file and fill the quickstat content to it

				File file = new File(
						this.rootLocation.toString() + '/' + splitName[0] + "/report/" + folderName + ".csv");
				file.createNewFile();
				FileWriter myWriter = new FileWriter(
						this.rootLocation.toString() + '/' + splitName[0] + "/report/" + folderName + ".csv");
				myWriter.write(quickstatContent);
				myWriter.close();

				// We add the filename to the returned value
				quickstatContent = folderName + ".csv" + "\n" + quickstatContent;

			}
			logger.info("Collection done");
			return quickstatContent;
		} catch (Exception ex) {
			throw new ClifHandlerException("error.execution.collecting");
		}
	}

	/**
	 * This function is used to retrieve in which folder quickstat.csv will be
	 * stored
	 * 
	 * @param folderURL : The folder that we want to read
	 * @return the name of the last created folder
	 * @throws IOException
	 */
	public String retrieveLastFolderName(String folderURL) throws IOException {

		ArrayList<String> folderNames = new ArrayList<>();
		DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(folderURL));

		for (Path path : stream) {
			if (Files.isDirectory(path)) {
				String directoryName = path.getFileName().toString();
				if (!directoryName.startsWith(".")) {
					folderNames.add(path.getFileName().toString());
				}
			}
		}

		ArrayList<String> timeStamp = new ArrayList<>();
		for (int i = 0; i < folderNames.size(); i++) {
			if (folderNames.get(i).contains(getResultFolderName())) {
				timeStamp.add(folderNames.get(i));
			}

		}
		int size = timeStamp.size();

		for (int i = 0; i < size - 1; i++) {
			for (int j = i + 1; j < size; j++) {
				if (timeStamp.get(i).compareTo(timeStamp.get(j)) > 0) {
					String temp = timeStamp.get(i);
					timeStamp.set(i, timeStamp.get(j));
					timeStamp.set(j, temp);
				}
			}
		}
		return timeStamp.get(size - 1);
	}

	/**
	 * This function creates the ElasticSearch index and the date from the filePath.
	 * It also retrieves information about the file size.
	 * 
	 * @param partialFilePath : The partial file path of the file that contains the
	 *                        wanted data. Example :
	 *                        projet/report/testName_date/BladeId/event
	 * @return A list of the required parameters to build the bulk
	 * @throws ParseException
	 */
	public ArrayList<Object> prepareTestData(String partialFilePath) throws ParseException {
		String filePath = this.rootLocation + "/" + partialFilePath;

		// create a Date pattern
		String pattern = "yyyy-MM-dd_HH-mm-ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		// retrieve the date from folderName
		String[] pathItemArray = partialFilePath.split("/");
		String folderTimeStamp = "";
		String dateTime = "";
		// retrieve the folder that contains the timestamp
		for (int i = 0; i < pathItemArray.length; i++) {
			if (pathItemArray[i].length() >= 19 && pathItemArray[i].contains("_") && pathItemArray[i].contains("-")) {
				dateTime = pathItemArray[i].substring(pathItemArray[i].length() - 19, pathItemArray[i].length());
				folderTimeStamp = pathItemArray[i];
			}
		}
		// format date to match the pattern
		dateTime = dateTime.replaceAll("h", "-");
		dateTime = dateTime.replaceAll("m", "-");
		// convert time since 1970 in ms
		Date date = simpleDateFormat.parse(dateTime);
		long originTime = date.getTime();

		// index format : clif-event-bladeId-date
		String bladeId = pathItemArray[pathItemArray.length - 1];
		bladeId = bladeId.replace(' ', '_');
		String event = pathItemArray[pathItemArray.length - 2];
		String index = "clif-" + event + "-" + bladeId + "-" + folderTimeStamp;
		index = index.toLowerCase();
		index = index.replace(' ', '_');

		// Obtain file size
		File file = new File(filePath);
		String bytes = Long.toString(file.length());

		ArrayList<Object> bulkParameters = new ArrayList<Object>();
		bulkParameters.add(index);
		bulkParameters.add(filePath);
		bulkParameters.add(originTime);
		bulkParameters.add(bladeId);
		bulkParameters.add(bytes);
		return bulkParameters;

	}

	/**
	 * Retrieves the class name associated to the event by reading the file
	 * filePath.classname
	 * 
	 * @param filePath : the absolute url of the event file
	 * @return the class name associated to the event file
	 * @throws IOException
	 */
	public String getClassName(String filePath) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath + ".classname")));
		String className = "";
		while (reader.ready()) {
			className += reader.readLine();
		}
		reader.close();
		return className;
	}

	/**
	 * This function uses Java reflection to get the labels associated to an event.
	 * The method getEventFieldLabels is called to retrieve the labels.
	 * 
	 * @param event     : the file name. Example : action
	 * @param className :the class associated to this event. Example :
	 *                  org.ow2.clif.storage.api.ActionEvent
	 * @return the labels associated to the event
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public String[] obtainLabels(String event, String className)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// use the reflexion to invoke the static method getEventFieldLabels
		Class<?> cls = Class.forName(className);
		Method method = cls.getMethod("getEventFieldLabels", String.class);
		String[] labels = (String[]) method.invoke(null, event);
		return labels;
	}

	/**
	 * This function check if an index already exists in ElasticSearch.
	 * 
	 * @param index : the index name that we want to test
	 * @return If the index exists, returns true. Else, returns false.
	 * @throws IOException
	 */
	public boolean checkIndexExist(String index) throws IOException {
		RestHighLevelClient client = null;
		ClientConfiguration clientConfiguration = ClientConfiguration.builder().connectedTo("localhost:9200").build();
		client = RestClients.create(clientConfiguration).rest();

		GetIndexRequest requestIndex = new GetIndexRequest(index);
		boolean exists = client.indices().exists(requestIndex, RequestOptions.DEFAULT);
		return exists;
	}

	/**
	 * Creates a bulk with the data read from url and the labels from labels.
	 * 
	 * @param index      : The index name
	 * @param origin     : The origin time. Used to calculate the date parameter
	 * @param url        : The location of the file to be read
	 * @param labels     : The labels that match the current event
	 * @param classname: The event class name
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	public void handleBulk(String index, long origin, String url, String[] labels, String className)
			throws IOException, ClassNotFoundException {
		RestHighLevelClient client = null;
		ClientConfiguration clientConfiguration = ClientConfiguration.builder().connectedTo("localhost:9200").build();
		client = RestClients.create(clientConfiguration).rest();
		BulkRequest blk = new BulkRequest();
		MappingElasticSearch mappingElasticSearch = new MappingElasticSearch();

		// We read the file
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(url)));
		int id = 0;

		// we get the label data types
		String[] labelType = mappingElasticSearch.getLabelType(className, labels.length);

		// data
		while (reader.ready()) {
			String line = reader.readLine();
			XContentBuilder builder = XContentFactory.jsonBuilder();
			builder.startObject();

			if (line.charAt(0) != '#') {
				String[] values = line.split(",");
				// handle date
				long temp = origin + Long.valueOf(values[0]);
				values[0] = String.valueOf(temp);
				builder.timeField(labels[0], temp);

				// handle other fields
				builder = mappingElasticSearch.buildField(builder, labels, values, labelType);

				// complete the bulk
				builder.endObject();
				IndexRequest request = new IndexRequest(index).id(String.valueOf(id)).source(builder);
				blk.add(request);
				if (id % 1000 == 0 && id > 1) {
					client.bulk(blk, RequestOptions.DEFAULT);
					blk = new BulkRequest();
				}
				id++;
			}
		}

		reader.close();
		// send data
		client.bulk(blk, RequestOptions.DEFAULT);

		// mapping
		mappingElasticSearch.mappingEvent(index, labels, client, labelType);
//		logger.info("Sent " + url + " to ElasticSearch");
	}

}
