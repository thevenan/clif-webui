package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive."
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive")
public class ConfigProactive {
	
	private String useIPaddress = null;
	private String hostname = null;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getUseIPaddress() {
		return useIPaddress;
	}

	public void setUseIPaddress(String useIPaddress) {
		this.useIPaddress = useIPaddress;
	}
}
