/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.controller;

import org.ow2.clif.console.lib.webui.storage.StorageProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;

import org.ow2.clif.console.lib.webui.auth.dao.DataAccessImpl;
import org.ow2.clif.console.lib.webui.auth.model.UserRegistration;
import org.ow2.clif.console.lib.webui.auth.PermissionsHandler;

@Controller
public class PermissionController {
    
	private final DataAccessImpl dataAccessImpl;	
	private final boolean securityEnabled;
	private final PermissionsHandler permissionsHandler;

    @Autowired
	public PermissionController(StorageProperties properties, JdbcUserDetailsManager jdbcUserDetailsManager, DataAccessImpl dataAccessImpl, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.dataAccessImpl = dataAccessImpl;
		this.securityEnabled = properties.securityEnabled().contains("true");
		this.permissionsHandler = new PermissionsHandler(dataAccessImpl,jdbcUserDetailsManager,bCryptPasswordEncoder);
	}


    @RequestMapping("/login")
	public ModelAndView login() {
		if(!this.securityEnabled) return new ModelAndView("redirect:/clifwebui");
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register() {
		if(!this.securityEnabled) return new ModelAndView("redirect:/clifwebui");
		return new ModelAndView("register", "user", UserRegistration.class);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView processRegister(@ModelAttribute("user") UserRegistration userRegistrationObject) {
		return permissionsHandler.handleRegister(userRegistrationObject);
	}
	
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("errorMsg", "Your username and password are invalid.");

		if (logout != null)
			model.addAttribute("msg", "You have been logged out successfully.");

		return "login";
	}

	@RequestMapping(value = "/addCollaborator",  method = RequestMethod.POST)
	@ResponseBody
	public String processAddCollaborator(@RequestBody String attributes, RedirectAttributes redirectAttributes) throws IOException {
		return permissionsHandler.handleAddCollaborator(attributes,redirectAttributes);			
	}

    @RequestMapping(value = "/getCollaborators", method = RequestMethod.POST)
	@ResponseBody
	public List<String> sendCollaborators(@RequestBody String project){
		return dataAccessImpl.getCollaborators(project);
	}


	@RequestMapping(value = "/editCollaborator",  method = RequestMethod.POST)
	@ResponseBody
	public String processEditCollaborator(@RequestBody String attributes, RedirectAttributes redirectAttributes) throws IOException {
		return permissionsHandler.handleEditCollaborator(attributes,redirectAttributes);
	}

	@RequestMapping(value = "/removeCollaborator",  method = RequestMethod.POST)
	@ResponseBody
	public String processRemoveCollaborator(@RequestBody String attributes, RedirectAttributes redirectAttributes) throws IOException {
		return permissionsHandler.handleRemoveCollaborator(attributes,redirectAttributes);
	}
}
