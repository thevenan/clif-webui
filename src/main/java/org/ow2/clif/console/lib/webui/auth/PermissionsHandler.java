/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.auth;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.ow2.clif.console.lib.webui.util;
import org.ow2.clif.console.lib.webui.auth.dao.DataAccessImpl;
import org.ow2.clif.console.lib.webui.auth.model.UserRegistration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.security.core.userdetails.User;

public class PermissionsHandler {

    private JdbcUserDetailsManager jdbcUserDetailsManager;
    public final DataAccessImpl dataAccessImpl;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    Logger logger = (Logger) LoggerFactory.getLogger(PermissionsHandler.class);

    public PermissionsHandler(DataAccessImpl dataAccessImpl, JdbcUserDetailsManager jdbcUserDetailsManager, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.jdbcUserDetailsManager = jdbcUserDetailsManager;
        this.dataAccessImpl = dataAccessImpl;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public ModelAndView handleRegister(UserRegistration userRegistration) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        ModelAndView model = new ModelAndView();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        if (dataAccessImpl.emailExists(userRegistration.email)) {
			model.addObject("email_exists", "This email already exists.");
			model.setViewName("register");
			if(dataAccessImpl.userExists(userRegistration.username)){
				model.addObject("user_exists", "This user already exists.");			
			}
			return model;
		}
		if(dataAccessImpl.userExists(userRegistration.username)){
			model.addObject("user_exists", "This user already exists.");
			model.setViewName("register");
			return model;			
		}		
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<UserRegistration>> violations = validator.validate(userRegistration);
        for (ConstraintViolation<UserRegistration> violation : violations) {
            throw new ValidationException(violation.getMessage());
        }
		String encodedPassword = bCryptPasswordEncoder.encode(userRegistration.password);
        User user = new User(userRegistration.username, encodedPassword, authorities);
		jdbcUserDetailsManager.createUser(user);
		dataAccessImpl.addEmailToUser(userRegistration.username, userRegistration.email);
		return new ModelAndView("redirect:/login");
	}

	public String handleAddCollaborator(String attributes, RedirectAttributes redirectAttributes) {
        String[] arguments = util.getArgsPermissions(attributes);
		String projectName = arguments[0];
		String permission = arguments[1];
		String username = arguments[2];
        if(redirectAttributes!=null){
            String user = util.getUserLogged();
            if(!dataAccessImpl.getPermission(user, projectName).contains("admin"))
                return util.returnMessage("error.unauthorized",null);
        }
        String args[] = {username,""};
        try{
            if(!dataAccessImpl.addCollaborator(username, projectName, permission)){
                return util.returnMessage("error.useralreadyexists",args);
            }
        }catch(DataIntegrityViolationException e){
            return util.returnMessage("error.userdoesnotexist",args);
        }
        args[1]=permission;
        return util.returnMessage("action.addedcollab",args);
	}

	public String handleEditCollaborator(String attributes, RedirectAttributes redirectAttributes) {
        String[] arguments = util.getArgsPermissions(attributes);
		String projectName = arguments[0];
		String permission = arguments[1];
        String username = arguments[2];
        if(redirectAttributes!=null){
            String user = util.getUserLogged();
            if(user==null || username==user||!dataAccessImpl.getPermission(user, projectName).contains("admin"))
                return util.returnMessage("error.unauthorized",null);
        }
        logger.info("changing "+username+" to "+permission+" for "+projectName);
        dataAccessImpl.editCollaborator(username, projectName, permission);
        String args[] = {username,permission};
         return util.returnMessage("action.editcollab",args);
	}

	public String handleRemoveCollaborator(String attributes, RedirectAttributes redirectAttributes) {
        String projectName = attributes.substring(0,attributes.indexOf("/"));
        logger.info("ProjectName: "+projectName);
        String username = attributes.substring(attributes.indexOf("/")+1);
        logger.info("UserName: "+username);
        if(redirectAttributes!=null){
            String user = util.getUserLogged();
            if(username==user||!(dataAccessImpl.getPermission(user, projectName).contains("admin"))||dataAccessImpl.getPermission(username, projectName).contains("admin"))
                return util.returnMessage("error.unauthorized",null);
        }
         logger.info("Removing "+username+" from "+projectName);

        
        dataAccessImpl.removeCollaborator(username, projectName);
        String args[] = {username,projectName};
         return util.returnMessage("action.removedcollab",args);
	}
    
}
