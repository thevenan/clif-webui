package org.ow2.clif.console.lib.webui.elasticSearch;

import java.io.IOException;
import java.lang.reflect.Method;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

public class MappingElasticSearch {

	private String[] labelTypeActionEvent = { "date", "long", "text", "long", "boolean", "long", "text", "text" };
	private String[] labelTypeLifecycleEvent = { "date", "long", "text" };

	private String[] labelTypeAlarmEvent = { "date", "integer", "text" };

	/**
	 * Each class has a different number of labels, with different data types. This
	 * function associates an array containing data types with the event class name
	 * 
	 * @param classname : the event class name
	 * @param labelNumber
	 * @return An array containing data types
	 * @throws ClassNotFoundException
	 */
	public String[] getLabelType(String classname, int labelNumber) throws ClassNotFoundException {
		String[] labelType;
		switch (classname) {
		case "org.ow2.clif.storage.api.LifeCycleEvent":
			labelType = labelTypeLifecycleEvent;
			break;
		case "org.ow2.clif.storage.api.ActionEvent":
			labelType = labelTypeActionEvent;
			break;
		case "org.ow2.clif.storage.api.AlarmEvent":
			labelType = labelTypeAlarmEvent;
		default:
			labelType = isProbeEvent(classname, labelNumber);
		}
		return labelType;
	}

	/**
	 * This method check if the class is extended from the class
	 * org.ow2.clif.storage.api.ProbeEvent. If so, the types of the labels is long,
	 * except the first one which is date
	 * @param className  : the event class name
	 * @param labelNumber 
	 * @throws ClassNotFoundException
	 * 
	 */
	public String[] isProbeEvent(String className, int labelNumber) throws ClassNotFoundException {
		String[] labelTypeProbeEvent;
		Class<?> cls = Class.forName(className);
		String superclassName = cls.getSuperclass().getName();
		System.out.println(superclassName);
		if (superclassName == "org.ow2.clif.storage.api.ProbeEvent") {
			labelTypeProbeEvent = new String[labelNumber];
			labelTypeProbeEvent[0] = "date";
			for (int i = 1; i < labelNumber; i++) {
				labelTypeProbeEvent[i] = "long";
			}
		} else {
			labelTypeProbeEvent = null;
		}
		return labelTypeProbeEvent;
	}

	/**
	 * This function builds the Json object that will be sent to ElasticSearch. It
	 * parses the String entries to the correct type.
	 * 
	 * @param builder   : the object that will be sent to ElasticSearch
	 * @param labels    : an array containing the values labels
	 * @param values    : the data extracted from a line of the file we want to send
	 *                  to ElasticSearch
	 * @param labelType : the types associated to the labels
	 * @return the completed JSON object
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public XContentBuilder buildField(XContentBuilder builder, String[] labels, String[] values, String[] labelType)
			throws NumberFormatException, IOException {
		for (int i = 1; i < labels.length; i++) {
			switch (labelType[i]) {
			case "long":
				builder.field(labels[i], Long.valueOf(values[i]));
				break;
			case "boolean":
				builder.field(labels[i], Boolean.parseBoolean(values[i]));
				break;
			case "integer":
				builder.field(labels[i], Integer.parseInt(values[i]));
				break;
			default:
				builder.field(labels[i], values[i]);
			}

		}
		return builder;
	}

	/**
	 * This function applies a mapping to the data that were sent to ElasticSearch
	 * 
	 * @param index     : the index to which the mapping will be associated
	 * @param labels
	 * @param client    : contains the REST configuration
	 * @param labelType
	 * @throws IOException
	 */
	public void mappingEvent(String index, String[] labels, RestHighLevelClient client, String[] labelType)
			throws IOException {

		PutMappingRequest requestMapping = new PutMappingRequest(index);
		XContentBuilder propertiesBuilder = XContentFactory.jsonBuilder();

		propertiesBuilder.startObject();
		{
			propertiesBuilder.startObject("properties");
			{
				for (int i = 0; i < labels.length; i++) {
					propertiesBuilder.startObject(labels[i]);
					{
						propertiesBuilder.field("type", labelType[i]);
					}
					propertiesBuilder.endObject();
				}
				propertiesBuilder.endObject();
			}
		}
		propertiesBuilder.endObject();
		requestMapping.source(propertiesBuilder);
		client.indices().putMapping(requestMapping, RequestOptions.DEFAULT);
	}
}
