/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.auth.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import org.ow2.clif.console.lib.webui.auth.model.UserRegistration;

import java.util.ArrayList;
import java.util.List;


import org.ow2.clif.console.lib.webui.auth.model.ProjectAccess;


@Service
public class DataAccessImpl implements DataAccessInterface{

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Override
    public UserRegistration findByUserName(String username) {
        String sql = "SELECT * FROM USERS WHERE USERNAME = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserRowMapper());
    }

    @Override
    public boolean addEmailToUser(String username, String email) {
        String sql = "UPDATE USERS SET EMAIL = ? WHERE USERNAME = ?";
        return jdbcTemplate.update(sql,email,username)==1;
    }

    @Override
    public boolean createProject(String username, String projectName) {
        String sql = "INSERT INTO USERPROJECT VALUES (?,?,?)";
        return jdbcTemplate.update(sql,username,projectName,"admin")==1;
    }
    @Override
    public boolean emailExists(String email) {
        String sql = "SELECT count(*) FROM USERS WHERE EMAIL = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{email}, Integer.class)!=0;         
    }

    @Override
    public boolean userExists(String username) {
        String sql = "SELECT count(*) FROM USERS WHERE USERNAME = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{username}, Integer.class)!=0;         
    }

    @Override
    public List<String> getAccessibleProjects(String username) {
        String sql = "SELECT * FROM USERPROJECT WHERE USERNAME = ?";
        List<ProjectAccess> projectList = jdbcTemplate.query(sql, new Object[]{username}, new ProjectAccessMapper());
        List<String> projectNameList = new  ArrayList<String>();
        for(int i=0;i<projectList.size();i++){
            projectNameList.add(projectList.get(i).project);
        }
        return projectNameList;
    }

    @Override
    public boolean addCollaborator(String username, String projectName, String permission) throws DataIntegrityViolationException{

        String sql = "select  count(*)  from USERPROJECT where USERNAME = ? and PROJECTNAME = ?  "; //testing if user doesn't already exists in this project
        if(jdbcTemplate.queryForObject(sql, new Object[]{username, projectName}, Integer.class)!=0){
            return false;
        }

        sql = "select  count(*) from USERPROJECT where PROJECTNAME = ?";//testing if the project exists
        if(jdbcTemplate.queryForObject(sql, new Object[]{projectName}, Integer.class)==0)
            return false;

        sql = "INSERT INTO USERPROJECT VALUES (?,?,?)";
        return jdbcTemplate.update(sql,username,projectName,permission)==1;
    }

    @Override
    public boolean editCollaborator(String username, String projectName, String permission) {
        String sql = "UPDATE USERPROJECT SET PERMISSION=? WHERE USERNAME=? AND PROJECTNAME=?";
        return jdbcTemplate.update(sql,permission,username,projectName)==1;
    }

    @Override
    public boolean removeCollaborator(String username, String projectName) {
        String sql = "DELETE FROM USERPROJECT WHERE USERNAME=? AND PROJECTNAME=?";
        return jdbcTemplate.update(sql,username,projectName)==1;
    }

    @Override
    public List<String> getCollaborators(String projectName) {
        String sql = "select * from USERPROJECT where PROJECTNAME = ?";
        List<ProjectAccess> projectList = jdbcTemplate.query(sql, new Object[]{projectName}, new ProjectAccessMapper());
        List<String> jsonList = new  ArrayList<String>();
        for(int i=0;i<projectList.size();i++){
            jsonList.add(projectList.get(i).toJSON());
        }   
        return jsonList;
    }

    @Override
    public String getPermission(String username, String projectname) {
        String sql = "select permission from USERPROJECT where USERNAME = ? and PROJECTNAME = ?  ";
        return jdbcTemplate.queryForObject(sql, new Object[]{username, projectname}, String.class);
    }

    @Override
    public List<String> getAccessUser(String username) {
        String sql = "select * from USERPROJECT where USERNAME = ?";
        List<ProjectAccess> projectList = jdbcTemplate.query(sql, new Object[]{username}, new ProjectAccessMapper());
        List<String> jsonList = new  ArrayList<String>();
        for(int i=0;i<projectList.size();i++){
            jsonList.add(projectList.get(i).toJSON());
        }   
        return jsonList;
    }

    @Override
	public boolean renameProject(String newName, String oldName) {
        String sql = "UPDATE USERPROJECT SET PROJECTNAME = ? WHERE PROJECTNAME = ?";
        return jdbcTemplate.update(sql,newName,oldName)>0;
	}

    @Override
	public boolean deleteProject(String path) {
        String sql = "DELETE from USERPROJECT where PROJECTNAME = ?";
        return jdbcTemplate.update(sql,path)==1;
	}


    
}
