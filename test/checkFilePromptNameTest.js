var assistant = require('/home/thevenet/Documents/CLIF/clif-webui/src/main/resources/static/js/assistant')
var assert = require('assert')


describe('clifwebui', function() {

	describe('checkFilePromptName', function() {

		it('should return x.ctp when x.ctp is written', function() {

			assert.strictEqual(assistant.checkFilePromptName("ctp", "x.ctp"), "x.ctp")

		})


		it('should return x.ctp when x is written', function() {

			assert.deepStrictEqual(assistant.checkFilePromptName("ctp", "x"), "x.ctp")

		})
	})

})

