var assistant = require('/home/thevenet/Documents/CLIF/clif-webui/src/main/resources/static/js/assistant')
var assert = require('assert')
let Plugin = {
	Plugin: [
		{
			plugin: {
				object: {
					params: "",
					help: "This plug-in makes it possible to measure the duration of a behavior segment and get a pseudo-sample event with this duration value. Once the chronograph is started by the appropriate control, the current duration value can be recorded by the split sample, and can be used as variable ${my_chrono:}.",
					class: "org.ow2.isac.plugin.chrono.SessionObject"
				},
				test: [
					{
						params: {
							param: {
								name: "value",
								type: "String"
							}
						},
						help: "current chrono value is greater than the given value",
						name: "is_gt",
						number: "0"
					},
					{
						params: {
							param: {
								name: "value",
								type: "String"
							}
						},
						help: "current chrono value is greater than or equals the given value",
						name: "is_gte",
						number: "1"
					},
					{
						params: {
							param: {
								name: "value",
								type: "String"
							}
						},
						help: "current chrono value is less than the given value",
						name: "is_lt",
						number: "2"
					},
					{
						params: {
							param: {
								name: "value",
								type: "String"
							}
						},
						help: "current chrono value is greater than or equals the given value",
						name: "is_lte",
						number: "3"
					},
					{
						params: "",
						help: "chrono is started",
						name: "is_on",
						number: "4"
					},
					{
						params: "",
						help: "chrono is stopped",
						name: "is_off",
						number: "5"
					},
					{
						params: "",
						help: "chrono is suspended",
						name: "is_suspended",
						number: "6"
					}
				],
				control: [
					{
						params: "",
						help: "starts the chrono",
						name: "start",
						number: "7"
					},
					{
						params: "",
						help: "suspends the chrono",
						name: "suspend",
						number: "8"
					},
					{
						params: "",
						help: "resumes the chrono",
						name: "resume",
						number: "9"
					},
					{
						params: "",
						help: "stops the chrono without creating a pseudo-sample",
						name: "drop",
						number: "12"
					}
				],
				sample: [
					{
						params: {
							param: [
								{
									name: "successful",
									type: "String"
								},
								{
									name: "comment",
									type: "String"
								},
								{
									name: "result",
									type: "String"
								}
							]
						},
						help: "records current chrono value in a pseudo sample without stopping the chrono.",
						name: "split",
						number: "10"
					},
					{
						params: {
							param: [
								{
									name: "successful",
									type: "String"
								},
								{
									name: "comment",
									type: "String"
								},
								{
									name: "result",
									type: "String"
								}
							]
						},
						help: "records current chrono value in a pseudo sample and stops the chrono.",
						name: "stop",
						number: "11"
					}
				],
				name: "Chrono"
			}
		},


		{
			plugin: {
				object: {
					help: "Provides constants TRUE and FALSE to be used as conditions, as well as miscellaneous features: printing, alarm, logging and Java system property setting.\nAlso provides a \"date_ms\" variable that gives the current date as the number of milliseconds elapsed since 1st January 1970 GMT (syntax: ${common_plugin_id:date_ms}). A custom-formatted date can be obtained with variable \"date!myDateFormat\", where myDateFormat follows the Java java.text.SimpleDateFormat specification.",
					class: "org.ow2.isac.plugin.common.SessionObject"
				},
				test: [
					{
						help: "A condition which is always true.",
						name: "true",
						number: "0"
					},
					{
						help: "A condition which is always false.",
						name: "false",
						number: "1"
					}
				],
				control: [
					{
						help: "Prints a string on standard output",
						params: {
							param: {
								name: "string",
								type: "String"
							}
						},
						name: "printout",
						number: "0"
					},
					{
						help: "Prints a string on error output",
						params: {
							param: {
								name: "string",
								type: "String"
							}
						},
						name: "printerr",
						number: "1"
					},
					{
						params: {
							param: {
								name: "message",
								type: "String"
							}
						},
						help: "Generates an alarm with the given message",
						name: "alarm",
						number: "2"
					},
					{
						params: {
							param: [
								{
									name: "name",
									type: "String"
								},
								{
									name: "value",
									type: "String"
								}
							]
						},
						help: "Sets a Java system property.",
						name: "setProperty",
						number: "4"
					},
					{
						params: {
							param: {
								name: "timezone",
								type: "String"
							}
						},
						help: "Sets timezone for variable 'date'. The timezone parameter may be filled a number of ways: either an abbreviation such as \"PST\", a full name such as \"Europe/Paris\", or a custom ID such as \"GMT+1:00\". Leave this parameter empty to get the default timezone as configured on the host.",
						name: "setTimeZone",
						number: "5"
					}
				],
				sample: {
					params: {
						param: [
							{
								name: "duration",
								type: "String"
							},
							{
								name: "comment",
								type: "String"
							},
							{
								name: "result",
								type: "String"
							},
							{
								name: "successful",
								type: "String"
							},
							{
								name: "iteration",
								type: "String"
							}
						]
					},
					help: "Generates a pseudo action/request report for custom logging purpose.",
					name: "log",
					number: "3"
				},
				name: "Common"
			}
		},
		{
			plugin: {
				object: {
					params: {
						param: [
							{
								name: "filename",
								type: "String"
							},
							{
								name: "charset",
								type: "String"
							}
						]
					},
					help: "Reads the full content of a file. Then, this content can be accessed through a variable expression ${myFileReaderId:}.",
					class: "org.ow2.isac.plugin.filereader.SessionObject"
				},
				control: [
					{
						params: "",
						help: "Discards any loaded content, and considers this file reader as unset.",
						name: "clear",
						number: "3"
					},
					{
						params: {
							param: [
								{
									name: "filename",
									type: "String"
								},
								{
									name: "charset",
									type: "String"
								}
							]
						},
						help: "Reads the full content of a file. Then, this content can be accessed through a variable expression ${myFileReaderId:}. If the file content can't be loaded for any reason, any previously loaded content is discarded (in other words, this file reader is cleared).",
						name: "load",
						number: "0"
					}
				],
				test: [
					{
						params: "",
						help: "True if a file has been successfully loaded, false otherwise.",
						name: "isSet",
						number: "1"
					},
					{
						params: "",
						help: "True if no file has been successfully loaded, false otherwise.",
						name: "isNotSet",
						number: "2"
					}
				],
				name: "FileReader"
			}
		}

	]
}


describe('assistant', function() {

	describe('retrievePrimitive', function() {

		it('should retrieve empty', function() {


			assert.deepStrictEqual(assistant.retrievePrimitive("control", "Chrono", "drop", Plugin), '')

		})

		it('should retrieve params FileReader load', function() {


			let fileReaderLoad = {
				param: [
					{
						name: "filename",
						type: "String"
					},
					{
						name: "charset",
						type: "String"
					}
				]
			}
			assert.deepStrictEqual(assistant.retrievePrimitive("control", "FileReader", "load", Plugin), fileReaderLoad)

		})

		it('should retrieve params Common log', function() {


			let commonDuration = {

				param: [
					{
						name: "duration",
						type: "String"
					},
					{
						name: "comment",
						type: "String"
					},
					{
						name: "result",
						type: "String"
					},
					{
						name: "successful",
						type: "String"
					},
					{
						name: "iteration",
						type: "String"
					}
				]
			}
			assert.deepStrictEqual(assistant.retrievePrimitive("sample", "Common", "log", Plugin), commonDuration)

		})




	})

})
